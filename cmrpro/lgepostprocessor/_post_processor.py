__all__ = ["LGEModel",]

import os
import numpy as np
import meshio
import imageio
import pyvista as pv
from vtk import *
from typing import List,Tuple
from vtk.util.numpy_support import numpy_to_vtk,vtk_to_numpy
from scipy.interpolate import LinearNDInterpolator,NearestNDInterpolator
import scipy as sp
import matplotlib.pyplot as plt

class LGEModel:
    """ Uses a processedExam object (including lge and cine segmentations) and fitted LV to analyze scar
    can only be used after a LV mesh and lge segmentations were generated (see mesh fitter)
    :param processed_exam: ProcessedExamObject with cine and lge series including segmentations
    :param out_folder
    """

    processed_exam: 'processedExam'
    out_path: str
    mesh_data: vtkUnstructuredGrid
    mesh_points: np.ndarray
    mesh_coordinates: List[np.ndarray]
    lge_seg: List[np.ndarray]
    lge_seg_worldcoords: np.ndarray
    lge_seg_meshcoords: np.ndarray
    affine = np.ndarray

    def __init__(self, processed_exam, out_path: str):
        self.processed_exam=processed_exam
        self.out_path=out_path
        self.affine=[]
        for s in self.processed_exam.series:
            if s['view']=='SAX':
                self.affine=s['affineRes']
        #read mesh in world coordinates from out_path (the LV mesh is a previous output from meshfitter) corresponding the the time frame of the lge image
        reader_coord = vtkUnstructuredGridReader()
        reader_coord.SetFileName(out_path+'/meshes_WorldCoordinates/'+'frame_%02d.vtk' % (processed_exam.lge_time_frame,))
        print('reading mesh: frame_%02d.vtk' % (processed_exam.lge_time_frame,))
        reader_coord.ReadAllScalarsOn()
        reader_coord.ReadAllVectorsOn()
        reader_coord.Update()
        self.mesh_data=reader_coord.GetOutput()
        self.mesh_points=vtk_to_numpy(self.mesh_data.GetPoints().GetData())
        self.mesh_coordinates=[]
        self.mesh_coordinates.append(vtk_to_numpy(self.mesh_data.GetPointData().GetVectors('x_c')))
        self.mesh_coordinates.append(vtk_to_numpy(self.mesh_data.GetPointData().GetVectors('x_t')))
        self.mesh_coordinates.append(vtk_to_numpy(self.mesh_data.GetPointData().GetVectors('x_l')))


        #read lge sax segementation from vti (including the world coordiantes); this is the output of the function export_segmentations_vti in the meshfitter
        assert os.path.exists(out_path+'segementations_VTI/'), "no vti file including the lge segmentation available call mesh_fitter.export_segmentations_vti befor using this class"
        reader_coord = vtkXMLImageDataReader()
        reader_coord.SetFileName(out_path+'segementations_VTI/'+'lge_view_SAX.vti')
        reader_coord.Update()
        self.lge_data=reader_coord.GetOutput()
        #lge segmetnation labels
        self.lge_seg=[]
        #2std<i<5std
        self.lge_seg.append(vtk_to_numpy(self.lge_data.GetPointData().GetScalars('LV_0')))
        #healthy
        self.lge_seg.append(vtk_to_numpy(self.lge_data.GetPointData().GetScalars('LV_1')))
        #scar i>5std
        self.lge_seg.append(vtk_to_numpy(self.lge_data.GetPointData().GetScalars('LV_2')))
        #world coordinates of the lge mask output
        self.lge_seg_worldcoords=image3DToXYZ(self.lge_data.GetDimensions(),  self.affine)
        #circumferential, transmural and longitudinal coordinates on the lge mask output
        self.lge_seg_meshcoordinates=[]


    def interpolate_mesh_coordinates_to_scar(self):
        '''interpolate physiological coordinates (c,t,l) from LV mesh to lge mask; assign -5 to background'''
        #initialize the physiological coordinates with -5 (to have a defined value for the background that is not part of any lge mask)
        self.lge_seg_meshcoordinates=np.ones(self.lge_seg_worldcoords.shape)*-5
        #build interpolater from LV mesh points to the lge masks
        interp_x_c = LinearNDInterpolator(self.mesh_points, self.mesh_coordinates[0])
        interp_x_t = LinearNDInterpolator(self.mesh_points, self.mesh_coordinates[1])
        interp_x_l = LinearNDInterpolator(self.mesh_points, self.mesh_coordinates[2])
        interp_x_c_nearest=NearestNDInterpolator(self.mesh_points, self.mesh_coordinates[0])
        interp_x_t_nearest=NearestNDInterpolator(self.mesh_points, self.mesh_coordinates[1])
        interp_x_l_nearest=NearestNDInterpolator(self.mesh_points, self.mesh_coordinates[2])

        #select interpolation target points of the lge masks (where not all labels are 0); rest==background-->-5
        idx_seg_exists=np.argwhere((self.lge_seg[0]+self.lge_seg[1]+self.lge_seg[2])>0)
        #interpolate c,t,l coordinates from mesh to lge mask (traget region)
        self.lge_seg_meshcoordinates[idx_seg_exists,0]=interp_x_c(self.lge_seg_worldcoords[idx_seg_exists])
        self.lge_seg_meshcoordinates[idx_seg_exists,1]=interp_x_t(self.lge_seg_worldcoords[idx_seg_exists])
        self.lge_seg_meshcoordinates[idx_seg_exists,2]=interp_x_l(self.lge_seg_worldcoords[idx_seg_exists])
        idx_nan = np.argwhere(np.logical_or(np.isnan(self.lge_seg_meshcoordinates[idx_seg_exists,0]) ,np.isnan(self.lge_seg_meshcoordinates[idx_seg_exists,1]), np.isnan(self.lge_seg_meshcoordinates[idx_seg_exists,2])))
        self.lge_seg_meshcoordinates[idx_seg_exists[idx_nan],0]=interp_x_c_nearest(self.lge_seg_worldcoords[idx_seg_exists[idx_nan],:])
        self.lge_seg_meshcoordinates[idx_seg_exists[idx_nan],1]=interp_x_t_nearest(self.lge_seg_worldcoords[idx_seg_exists[idx_nan],:])
        self.lge_seg_meshcoordinates[idx_seg_exists[idx_nan],2]=interp_x_l_nearest(self.lge_seg_worldcoords[idx_seg_exists[idx_nan],:])

    def export_vti(self, vtkimage: np.ndarray, data: np.ndarray, array_names: List[str], out_path: str,filename:str ):
        '''adds data to vtkimage data object and exports vtkimage with appended data to vtk file'''
        for i in range(data.shape[-1]):
            vtk_array = numpy_to_vtk(data[:,i].reshape(-1, 1, order="F"))
            vtk_array.SetName(array_names[i])
            vtkimage.GetPointData().AddArray(vtk_array)
            vtkimage.Modified()
        writer = vtkXMLImageDataWriter()
        writer.SetFileName(out_path+'segementations_VTI/'+filename)
        writer.SetInputData(vtkimage)
        writer.Write()

    def clean_up_lge_seg(self,threshold=10):
        '''removes areas from lge masks (lge_seg) that are further away from the LV mask as the threshold (=10 as defaulft)
            sets the lge_seg_meshcoordinates (c,t,l) to the background value -5
        '''
        idx_seg_exists=np.squeeze(np.argwhere((self.lge_seg[0]+self.lge_seg[1]+self.lge_seg[2])>0))
        dist=sp.spatial.distance.cdist(self.lge_seg_worldcoords[idx_seg_exists], self.mesh_points)
        min_dist=np.min(dist,axis=1)
        idx_exclude=np.argwhere(min_dist>threshold)
        self.lge_seg_meshcoordinates[idx_seg_exists[idx_exclude]]=-5
        #remove points from segmenation
        for channel in range(3):
            idx_seg_channel=np.argwhere(np.logical_and(self.lge_seg[channel]>0,self.lge_seg_meshcoordinates[:,0]<-4))
            self.lge_seg[channel][idx_seg_channel]=0

    def post_process_lge(self):
        '''performes main postprocessing steps:
        1) interpolate c,t,l coordinates from LV mesh to lge masks
        2/4)exports vti files with lge masks+coordinates
        3) cleans up unrealistic lge mask labels ("far"(>threshold) away from LV mesh)
        '''
        self.interpolate_mesh_coordinates_to_scar()
        self.export_vti(self.lge_data,self.lge_seg_meshcoordinates,['x_c','x_t','x_l'],self.out_path,'lge_view_SAX_coords.vti')
        self.clean_up_lge_seg(10)
        self.export_vti(self.lge_data,self.lge_seg_meshcoordinates,['x_c','x_t','x_l'],self.out_path,'lge_view_SAX_coords_cleanup.vti')

    def visualize_lge_seg_in_shapecoords(self):
        '''prints out different plots to visualize the lge segmentation:
        1) c-l scatter plot across all transmural positions
        2/3) c-l scatter slider+plot with transmural bins
        4) bullseye plot (AHA sectors) with local scar density
        5) scar density as grid (c-l) for end0/mid/epi each
        '''
        if not os.path.exists(self.out_path+'scar_analysis/'):
            os.makedirs(self.out_path+'scar_analysis/')
        self.continuous_scatter_cl_all_transmural(2)
        self.get_data_transmural_depth(0.45,10)
        #self.scatter_cl_transmural_slider(2)
        self.scatter_cl_transmural_bins(2,10)
        self.scar_density_bullseye()
        self.plot_density_grid(2,0.5)

    def continuous_scatter_cl_all_transmural(self,scar_id:int):
        ''' plots c-l scatter plot across all transmural positions for the selected scar_id (is assumed to be relevant if predicted probabiltiy is >2/8)
            currently available scar ids coincide with the original lge segmentation output cahnnels
            (TODO: adapt this when final version of the new network is trained/ possibly add >std2)
        '''
        if scar_id==0:
            scar_name='2stdto5std_scar'
        elif scar_id==1:
            scar_name='healthy'
        elif scar_id==2:
            scar_name='5std_scar'
        else:
            print("scar_id must be 0,1,or 2. Set to default: 2=scar")
            scar_id=2
            scar_name='scar'

        #get points where selected label has a probability >2/8
        idx_scar=np.argwhere(self.lge_seg[scar_id]>(2/8))
        #scatter plot c-l color coded by probability of the prediction; save to file
        plt.figure()
        scat=plt.scatter(self.lge_seg_meshcoordinates[idx_scar,0],self.lge_seg_meshcoordinates[idx_scar,2],c=self.lge_seg[scar_id][idx_scar],s=5)
        plt.ylim(0,1)
        plt.xlim(-np.pi,np.pi)
        plt.ylabel("longitudinal coordinate")
        plt.xlabel("circumferential coordinate")
        cbar = plt.colorbar(scat)
        cbar.set_label("tt certainty[2/8-8/8]")
        plt.title(scar_name+"; all transmural depths")
        plt.savefig(self.out_path+'scar_analysis/'+scar_name+"_cl_all_transmural.png")
        # plt.show()

    def plot_density_grid(self,scar_id,threshold=0.5):
        '''
        plot the scar density as grid (c-l) for endo/mid/epi part each
        currently available scar ids coincide with the original lge segmentation output cahnnels;default: >sd5
        (TODO: adapt this when final version of the new network is trained/ possibly add >std2)
        '''
        #set number of cricumferential and longitudinal bins; attention: if this number is too large, empty bins can occur, leading to division by zero
        n_bin_c=8
        n_bin_l=5
        #get bin bounaries and grid
        bins_c=np.linspace(-np.pi, np.pi, num=n_bin_c)
        bins_l=np.linspace(0.0, 1.0, num=n_bin_l)
        c,l=np.meshgrid(bins_c,bins_l)
        #select label of interest
        if scar_id==0:
            scar_name='scar sd 2 to 5'
            idx_scar=np.argwhere(np.logical_or(self.lge_seg[2]>threshold, self.lge_seg[0]>threshold))
        elif scar_id==1:
            scar_name='healthy'
            idx_scar=np.argwhere(self.lge_seg[1]>threshold)
        elif scar_id==2:
            scar_name='scar sd5'
            idx_scar=np.argwhere(self.lge_seg[2]>threshold)
        else:
            print("scar_id must be 0,1,or 2. Set to default: 2=scar")
            scar_id=2
            scar_name='scar'
            idx_scar=np.argwhere(self.lge_seg[2]>threshold)
        #get LV myocard mask idxs
        idx_in_myocard=np.argwhere((self.lge_seg[0]+self.lge_seg[1]+self.lge_seg[2])>0)
        #using 3 transmural bins
        n_bin_t=3
        transmural_title=['endo','mid','epi']
        #initialize figure and regional scar density grid, grid center coordinates
        fig, axs = plt.subplots(1,n_bin_t,figsize=(20,15))
        data_density_scar=np.zeros((n_bin_c-1,n_bin_l-1,n_bin_t))
        center_coord_cl=np.zeros((n_bin_c-1,n_bin_l-1,2))
        #loop over transmural bins
        for t_iter in range(n_bin_t):
            #get transmural bin center coordinate
            t_center=1/(n_bin_t*2)+t_iter/n_bin_t
            #get indices within current transmuarl bin
            idx_t_init=self.get_data_transmural_depth(t_center,n_bin_t)
            #get indices within current transmuarl bin and part of the scar mask of the current label/scar_id based on probability threshold (default 0.5)
            idx=[x for x in idx_scar if x in idx_t_init]
            #get indices within current transmuarl bin and part of the myocard of the LV
            idx_ref=[x for x in idx_in_myocard if x in idx_t_init]
            #assigns coordinates of the scar and reference to the bins
            binID_c=np.squeeze(np.digitize(self.lge_seg_meshcoordinates[idx,0], bins_c,right=True))
            binID_l=np.squeeze(np.digitize(self.lge_seg_meshcoordinates[idx,2], bins_l,right=True))
            binID_ref_c=np.squeeze(np.digitize(self.lge_seg_meshcoordinates[idx_ref,0], bins_c,right=True))
            binID_ref_l=np.squeeze(np.digitize(self.lge_seg_meshcoordinates[idx_ref,2], bins_l,right=True))
            #iterate over the bins and calculates regional scar density (number of scar voxels/total number of myocardial voxels)
            for iter_c in range(n_bin_c-1):
                for iter_l in range(n_bin_l-1):
                    current_c_idx=np.argwhere(binID_c==iter_c+1)
                    current_cl_idx=np.argwhere(binID_l[current_c_idx]==iter_l+1)
                    current_c_idx_ref=np.argwhere(binID_ref_c==iter_c+1)
                    current_cl_idx_ref=np.argwhere(binID_ref_l[current_c_idx_ref]==iter_l+1)
                    if len(current_cl_idx_ref)==0:
                        print('too many longitudinal bins (empty bins exist)')
                        break
                    data_density_scar[iter_c,iter_l,t_iter]=len(current_cl_idx)/len(current_cl_idx_ref)
                    #get center coordinate to get location to print label in plot
                    center_coord_cl[iter_c,iter_l,0]=(bins_c[iter_c]+bins_c[iter_c+1])/2
                    center_coord_cl[iter_c,iter_l,1]=(bins_l[iter_l]+bins_l[iter_l+1])/2
            #plot data
            im = axs[t_iter].pcolormesh(c,l,np.transpose(data_density_scar[:,:,t_iter]),cmap='viridis',vmin=0.0,vmax=1.0)
            axs[t_iter].set_title(transmural_title[t_iter])
            if t_iter==n_bin_t-1:
                cbar=plt.colorbar(im,ax=axs[t_iter])
                cbar.set_label("density "+scar_name+"; uncertainty threshold=%.1f"%threshold)
            for iter_c in range(n_bin_c-1):
                for iter_l in range(n_bin_l-1):
                    axs[t_iter].text(center_coord_cl[iter_c,iter_l,0],center_coord_cl[iter_c,iter_l,1], '%.3f' % data_density_scar[iter_c , iter_l,t_iter], horizontalalignment='center', verticalalignment='center')
        axs[0].set_ylabel("longitudinal coordinate")
        axs[1].set_xlabel("circumferential coordinate")
        plt.savefig(self.out_path+'scar_analysis/scar_density_uncertainty_threshold%.1f_pcolor.png'%threshold)



    def scatter_cl_transmural_slider(self,scar_id):
        '''
        displays a scatter plot with circumferential and longitudinal axisa of a mask label (select by scar_id; default=scar>sd5) with a slider to step through transmural bins
        color coded by the estimated probability of the segmentation (assumes to be relevant if predicted probabiltiy is >2/8)
        '''
        #number of transmural bins
        n_bins=10
        from matplotlib.widgets import Slider
        #select label
        if scar_id==0:
            scar_name='scar sd 2 to 5'
        elif scar_id==1:
            scar_name='healthy'
        elif scar_id==2:
            scar_name='scar sd5'
        else:
            print("scar_id must be 0,1,or 2. Set to default: 2=scar")
            scar_id=2
            scar_name='scar >sd5'
        #get indices where this label has a prediction >2/8
        idx_scar=np.argwhere(self.lge_seg[scar_id]>(2/8))
        #set initial transmural bin
        t_init=0.45
        #get indices of voxels within current bin
        idx_t_init=self.get_data_transmural_depth(t_init,n_bins)
        #get indices of voxels were current mask label is assingned (e.g. scar >sd5) and within current/initial bin
        idx=[x for x in idx_scar if x in idx_t_init]
        #initialize plot
        fig, ax = plt.subplots()
        ax.set_ylabel("longitudinal coordinate")
        ax.set_xlabel("circumferential coordinate")
        ax.set_xlim(-np.pi,+np.pi)
        ax.set_ylim((2/8)-0.05,+1.05)
        #scatter plot
        sct=ax.scatter(self.lge_seg_meshcoordinates[idx,0],self.lge_seg_meshcoordinates[idx,2],c=self.lge_seg[scar_id][idx],s=5)
        axcolor = fig.add_axes([0.9, 0.3, 0.01, 0.5])
        cbar = plt.colorbar(sct,cax=axcolor)
        cbar.set_label("tt certainty[2/8-8/8]")
        # adjust the main plot to make room for the sliders
        fig.subplots_adjust(left=0.25, bottom=0.25)
        axcoord = fig.add_axes([0.25, 0.1, 0.65, 0.03])
        #slider setup
        t_coord = Slider(ax=axcoord, label='transmural coordinate', valmin=0.0, valmax=0.99, valinit=0.45)
        #update function: what is done when slider is moved
        def update(val):
            #get indices of current bin
            idx_t=self.get_data_transmural_depth(t_coord.val,n_bins)
            #get indices where active mask label has a prediction >2/8
            idx_scar=np.argwhere(self.lge_seg[scar_id]>(2/8))
            #get indices of maks within current bin
            idx=[x for x in idx_scar if x in idx_t]
            #plot
            ax.cla()
            axcolor.cla()
            sct=ax.scatter(self.lge_seg_meshcoordinates[idx,0],self.lge_seg_meshcoordinates[idx,2],c=self.lge_seg[scar_id][idx],s=5)
            cbar = plt.colorbar(sct,cax=axcolor)
            cbar.set_label("tt certainty[2/8-8/8]")
            ax.set_ylabel("longitudinal coordinate")
            ax.set_xlim(-np.pi,+np.pi)
            ax.set_ylim((2/8)-0.05,+1.05)
            ax.set_xlabel("circumferential coordinate")
            fig.canvas.draw_idle()
        t_coord.on_changed(update)
        plt.show()
        return

    def scatter_cl_transmural_bins(self,scar_id,n_bins):
        '''
        plot and save figure (scatter plot) for transmural bins of prdiciton/mask label(scar_id default= 2 == >sd5); n_bins=number of transmural bins can be selected
        same as scatter_cl_transmural_slider but without slider but direct figure output
        '''
        if scar_id==0:
            scar_name='remote_scar'
        elif scar_id==1:
            scar_name='healthy'
        elif scar_id==2:
            scar_name='scar'
        else:
            print("scar_id must be 0,1,or 2. Set to default: 2=scar")
            scar_id=2
            scar_name='scar'

        #loop over all transmural bins
        for t in np.linspace(0.0,1.0-(1/n_bins),n_bins):
            #get indices of voxels within current bin
            idx_t=self.get_data_transmural_depth(t,n_bins)
            #get indices where current mask label has a probability >2/8
            idx_scar=np.argwhere(self.lge_seg[scar_id]>(2/8))
            #get indices of maks within current bin
            idx=[x for x in idx_scar if x in idx_t]
            #plot
            plt.figure()
            scat=plt.scatter(self.lge_seg_meshcoordinates[idx,0],self.lge_seg_meshcoordinates[idx,2],c=self.lge_seg[scar_id][idx],s=5)
            plt.ylim(0,1)
            plt.xlim(-np.pi,np.pi)
            plt.ylabel("longitudinal coordinate")
            plt.xlabel("circumferential coordinate")
            cbar = plt.colorbar(scat)
            cbar.set_label("tt certainty[2/8-8/8]")
            plt.title(scar_name+"; transmural depth: %.1f to %.1f"%(t,(t+(1/n_bins)),))
            plt.savefig(self.out_path+'scar_analysis/'+scar_name+"_cl_transmuraldepth_%.1f.png"%(t))
            plt.close()
        return

    def get_data_transmural_depth(self,t,num_bins):
        '''
        get indices of all voxels within the current bin (selected by the variable t==current bin center) for a total number of num_bins between 0 and 1
        '''
        #get all lower and upper bin boundaries
        low_bounds=np.linspace(0.0,1.0-(1.0/num_bins),num=num_bins)
        upper_bounds=np.linspace((1.0/num_bins),1.0,num=num_bins)
        #find current bin index
        current_bin=np.argmin(np.abs(upper_bounds-t))
        #compensate for machine precision problem
        if abs(upper_bounds[current_bin]-t)<0.0001:
            current_bin+=1
        elif (upper_bounds[current_bin]-t)<0.0:
            current_bin+=1
        #get indices of all voxels within the current transmuarl bin by pysiological coordonte using the bin boundaries
        idx_t_range=np.argwhere(np.logical_and(self.lge_seg_meshcoordinates[:,1]>=low_bounds[current_bin],self.lge_seg_meshcoordinates[:,1]<upper_bounds[current_bin]))
        return idx_t_range

    def scar_density_bullseye(self):
        '''
        creates a bullseye plot of the scar (> sd2) density

        '''
        #get list of AHA sectors with lists of assigned voxel ids
        aha_zone_idx=get_aha_zones(self.lge_seg_meshcoordinates[:,0],self.lge_seg_meshcoordinates[:,1])
        #get indices of all voxels within at least one lge segmentation masks ==LV myocard
        idx_in_myocard=np.argwhere((self.lge_seg[0]+self.lge_seg[1]+self.lge_seg[2])>0)
        #get indices of all voxels with >sd2 scar brediciton with probabalilty >2/8
        idx_scar=np.argwhere(np.logical_or(self.lge_seg[2]>(2/8), self.lge_seg[0]>(2/8)))
        #initialize scar density
        scar_density_zones=[]
        #loop over AHA sectors
        for zone in range(17):
            #get indices of voxels within cuurent sector
            current_zone_idx=aha_zone_idx[16-zone][0]
            #get indices of voxels within the sector and scar label
            current_idx_scar=[x for x in current_zone_idx if x in idx_scar]
            current_idx_myocard=[x for x in current_zone_idx if x in idx_in_myocard]
            #carculate scar density as number of voxels with scar within the sector/all voxels within the sector
            scar_density_zones.append(len(current_idx_scar)/len(current_idx_myocard))

        #plot
        fig= plt.figure(constrained_layout=False,figsize=(4.5,4.5))
        gs = fig.add_gridspec(ncols=2,nrows=1, wspace=0.04, hspace=0.04,width_ratios=[1,0.08], height_ratios=[1])
        ax=fig.add_subplot(gs[0,0],projection='polar')
        import matplotlib as mpl
        norm = mpl.colors.Normalize(vmin=0.0, vmax=np.max(scar_density_zones))
        cmap = mpl.cm.viridis
        bullseye_plot(ax,scar_density_zones, cmap=cmap, norm=norm)
        axcm = fig.add_subplot(gs[0,1])#fig.add_axes([ 0.89,0.25,  0.01,0.5])
        cb1 = mpl.colorbar.ColorbarBase(axcm, cmap=cmap, norm=norm, orientation='vertical')
        plt.savefig(self.out_path+'scar_analysis/scar_density_bullseye.png')


def bullseye_plot(ax, data,seg_bold=None, cmap=None, norm=None):
    import matplotlib as mpl
    """
    Bullseye representation for the left ventricle.
    attention rescaled the radius for filling manually to compesate pcolormesh polar coordinate mapping error in this matplotlib version (not needed for other versions)

    Parameters
    ----------
    ax : axes
    data : list of int and float
        The intensity values for each of the 17 segments
    seg_bold : list of int, optional
        A list with the segments to highlight
    cmap : ColorMap or None, optional
        Optional argument to set the desired colormap
    norm : Normalize or None, optional
        Optional argument to normalize data into the [0.0, 1.0] range


    Notes
    -----
    This function create the 17 segment model for the left ventricle according
    to the American Heart Association (AHA) [1]_

    References
    ----------
    .. [1] M. D. Cerqueira, N. J. Weissman, V. Dilsizian, A. K. Jacobs,
        S. Kaul, W. K. Laskey, D. J. Pennell, J. A. Rumberger, T. Ryan,
        and M. S. Verani, "Standardized myocardial segmentation and
        nomenclature for tomographic imaging of the heart",
        Circulation, vol. 105, no. 4, pp. 539-542, 2002.
    """
    if seg_bold is None:
        seg_bold = []

    linewidth = 2
    data = np.array(data).ravel()
    #data_std = np.array(data_std).ravel()

    if cmap is None:
        cmap = plt.cm.viridis

    if norm is None:
        norm = mpl.colors.Normalize(vmin=data.min(), vmax=data.max())

    theta = np.linspace(0, 2 * np.pi, 768)
    r = np.linspace(0.2, 1, 4)
    # Create the bound for the segment 17
    for i in range(r.shape[0]):
        ax.plot(theta, np.repeat(r[i], theta.shape), '-k', lw=linewidth)


    # Create the bounds for the segments 1-12
    for i in range(6):
        theta_i = np.deg2rad(i * 60)
        ax.plot([theta_i, theta_i], [r[1], 1], '-k', lw=linewidth)

    # Create the bounds for the segments 13-16
    for i in range(4):
        theta_i = np.deg2rad(i * 90 - 45)
        ax.plot([theta_i, theta_i], [r[0], r[1]], '-k', lw=linewidth)

    # Fill the segments 1-6
    r0 = r[2:4]/1.125
    r0 = np.repeat(r0[:, np.newaxis], 128, axis=1).T
    rot=[0,60,295,0,60,295]
    for i in range(6):
        # First segment start at 60 degrees
        r_shift=[-0.03,-.02,.03,0,0,-0.035]
        theta0 = theta[i * 128:i * 128 + 128] + np.deg2rad(60)
        cell_center=(((theta[i * 128]+theta[(i * 128 + 127)])/2+np.deg2rad(60)),((r[2]+r[3])/2)+r_shift[i])
        theta0 = np.repeat(theta0[:, np.newaxis], 2, axis=1)
        z = np.ones((128, 2)) * data[i]
        ax.pcolormesh(theta0, r0, z, cmap=cmap, norm=norm)
        ax.annotate(str("%.2f" % round(data[i],2)), xy=cell_center,
                                xytext=(cell_center[0], cell_center[1]),
                                horizontalalignment='center', verticalalignment='center', size=7.0,rotation=rot[i])
        #ax.annotate(str("%.1f+/-%.1f" % (round(data[i],1),round(data_std[i],1))), xy=cell_center, xytext=(cell_center[0], cell_center[1]), horizontalalignment='center', verticalalignment='center', size=7.0)
        if i + 1 in seg_bold:
            ax.plot(theta0, r0, '-k', lw=linewidth + 2)
            ax.plot(theta0[0], [r[2], r[3]], '-k', lw=linewidth + 1)
            ax.plot(theta0[-1], [r[2], r[3]], '-k', lw=linewidth + 1)

    # Fill the segments 7-12
    r0 = r[1:3]*0.85
    r0 = np.repeat(r0[:, np.newaxis], 128, axis=1).T
    rot=[0,60,295,0,60,295]
    for i in range(6):
        # First segment start at 60 degrees
        r_shift=[-0.03,-.02,.03,0,0,-0.035]
        theta0 = theta[i * 128:i * 128 + 128] + np.deg2rad(60)
        cell_center=(((theta[i * 128]+theta[(i * 128 + 127)])/2+np.deg2rad(60)),((r[1]+r[2])/2)+r_shift[i])
        theta0 = np.repeat(theta0[:, np.newaxis], 2, axis=1)
        z = np.ones((128, 2)) * data[i + 6]
        ax.pcolormesh(theta0, r0, z, cmap=cmap, norm=norm)
        ax.annotate(str("%.2f" % round(data[i + 6],2)), xy=cell_center,
                                xytext=(cell_center[0], cell_center[1]),
                                horizontalalignment='center', verticalalignment='center', size=7.0,rotation=rot[i])
        if i + 7 in seg_bold:
            ax.plot(theta0, r0, '-k', lw=linewidth + 2)
            ax.plot(theta0[0], [r[1], r[2]], '-k', lw=linewidth + 1)
            ax.plot(theta0[-1], [r[1], r[2]], '-k', lw=linewidth + 1)
    #
    # Fill the segments 13-16
    r0 = r[0:2]*0.78
    r0 = np.repeat(r0[:, np.newaxis], 192, axis=1).T
    for i in range(4):
        # First segment start at 45 degrees
        r_shift=[-0.04,-.035,0,-0.04]
        theta0 = theta[i * 192:i * 192 + 192] + np.deg2rad(45)
        cell_center=(((theta[i * 192]+theta[(i * 192 + 191)])/2+np.deg2rad(45)),((r[0]+r[1])/2)+r_shift[i])
        theta0 = np.repeat(theta0[:, np.newaxis], 2, axis=1)
        z = np.ones((192, 2)) * data[i + 12]
        ax.pcolormesh(theta0, r0, z, cmap=cmap, norm=norm)
        rot=[0, 90,0,270]
        x_offset=[]
        y_offset=[0.2,0.2]
        ax.annotate(str("%.2f" % round(data[i + 12],2)), xy=cell_center,
                                horizontalalignment='center', verticalalignment='center', size=7.0,rotation=rot[i])
        if i + 13 in seg_bold:
            ax.plot(theta0, r0, '-k', lw=linewidth + 2)
            ax.plot(theta0[0], [r[0], r[1]], '-k', lw=linewidth + 1)
            ax.plot(theta0[-1], [r[0], r[1]], '-k', lw=linewidth + 1)

    # Fill the segments 17
    if data.size == 17:
        r0 = np.array([0, r[0]/1.5])
        r0 = np.repeat(r0[:, np.newaxis], theta.size, axis=1).T
        theta0 = np.repeat(theta[:, np.newaxis], 2, axis=1)
        z = np.ones((theta.size, 2)) * data[16]
        ax.pcolormesh(theta0, r0, z, cmap=cmap, norm=norm)
        if 17 in seg_bold:
            ax.plot(theta0, r0, '-k', lw=linewidth + 2)
        cell_center = (0, 0)
        ax.annotate(str("%.2f" % round(data[16],2)), xy=cell_center,
                                xytext=(cell_center[0], cell_center[1]),
                                horizontalalignment='center', verticalalignment='center', size=7.0)

    #ax.set_ylim([0, 1])
    ax.set_yticklabels([])
    ax.set_xticklabels([])
    # plt.tight_layout()
    return

def get_aha_zones(coord_c: np.ndarray, coord_l: np.ndarray):
    '''
    assignes each voxel to a aha sector (using 17 sectors including the apical cap)
    aha_zone_matrix: list (len 17) of arrays with indices for the 17 sectors
    aha_zone: array of the length as number of voxels; contains the AHA sector label (1 to 17) for each voxel respectively
    '''
    coord_c=coord_c/np.max(coord_c)
    aha_zone_matrix=[]
    aha_zone=np.zeros(coord_l.shape[0])
    #17
    sec17=np.where(coord_l<0.1)
    aha_zone_matrix.append(sec17)
    aha_zone[sec17]=17
    #16
    sec_16=np.where(np.logical_and(np.logical_and(coord_l>0.1,coord_l<=0.4),np.logical_and(coord_c<0.75,coord_c>=0.5)))
    aha_zone_matrix.append(sec_16)
    aha_zone[sec_16]=16
    #15
    sec_15=np.where(np.logical_and(np.logical_and(coord_l>0.1,coord_l<=0.4),np.logical_and(coord_c<0.5,coord_c>=0.25)))
    aha_zone_matrix.append(sec_15)
    aha_zone[sec_15]=15
    #14
    sec_14=np.where(np.logical_and(np.logical_and(coord_l>0.1,coord_l<=0.4),np.logical_and(coord_c<0.25,coord_c>=0.0)))
    aha_zone_matrix.append(sec_14)
    aha_zone[sec_14]=14
    #13
    sec_13=np.where(np.logical_and(np.logical_and(coord_l>0.1,coord_l<=0.4),np.logical_and(coord_c<=1.0,coord_c>=0.75)))
    aha_zone_matrix.append(sec_13)
    aha_zone[sec_13]=13
    #12
    sec_12=np.where(np.logical_and(np.logical_and(coord_l>0.4,coord_l<=0.7),np.logical_and(coord_c<=5/6,coord_c>=4/6)))
    aha_zone_matrix.append(sec_12)
    aha_zone[sec_12]=12
    #11
    sec_11=np.where(np.logical_and(np.logical_and(coord_l>0.4,coord_l<=0.7),np.logical_and(coord_c<=4/6,coord_c>=3/6)))
    aha_zone_matrix.append(sec_11)
    aha_zone[sec_11]=11
    #10
    sec_10=np.where(np.logical_and(np.logical_and(coord_l>0.4,coord_l<=0.7),np.logical_and(coord_c<=3/6,coord_c>=2/6)))
    aha_zone_matrix.append(sec_10)
    aha_zone[sec_10]=10
    #9
    sec_9=np.where(np.logical_and(np.logical_and(coord_l>0.4,coord_l<=0.7),np.logical_and(coord_c<=2/6,coord_c>=1/6)))
    aha_zone_matrix.append(sec_9)
    aha_zone[sec_9]=9
    #8
    sec_8=np.where(np.logical_and(np.logical_and(coord_l>0.4,coord_l<=0.7),np.logical_and(coord_c<=1/6,coord_c>=0.0)))
    aha_zone_matrix.append(sec_8)
    aha_zone[sec_8]=8
    #7
    sec_7=np.where(np.logical_and(np.logical_and(coord_l>0.4,coord_l<=0.7),np.logical_and(coord_c<=1.0,coord_c>=5/6)))
    aha_zone_matrix.append(sec_7)
    aha_zone[sec_7]=7
    #6
    sec_6=np.where(np.logical_and(np.logical_and(coord_l>0.7,coord_l<=1.0),np.logical_and(coord_c<=5/6,coord_c>=4/6)))
    aha_zone_matrix.append(sec_6)
    aha_zone[sec_6]=6
    #5
    sec_5=np.where(np.logical_and(np.logical_and(coord_l>0.7,coord_l<=1.0),np.logical_and(coord_c<=4/6,coord_c>=3/6)))
    aha_zone_matrix.append(sec_5)
    aha_zone[sec_5]=5
    #4
    sec_4=np.where(np.logical_and(np.logical_and(coord_l>0.7,coord_l<=1.0),np.logical_and(coord_c<=3/6,coord_c>=2/6)))
    aha_zone_matrix.append(sec_4)
    aha_zone[sec_4]=4
    #3
    sec_3=np.where(np.logical_and(np.logical_and(coord_l>0.7,coord_l<=1.0),np.logical_and(coord_c<=2/6,coord_c>=1/6)))
    aha_zone_matrix.append(sec_3)
    aha_zone[sec_3]=3
    #2
    sec_2=np.where(np.logical_and(np.logical_and(coord_l>0.7,coord_l<=1.0),np.logical_and(coord_c<=1/6,coord_c>=0.0)))
    aha_zone_matrix.append(sec_2)
    aha_zone[sec_2]=2
    #1
    sec_1=np.where(np.logical_and(np.logical_and(coord_l>0.7,coord_l<=1.0),np.logical_and(coord_c<=1.0,coord_c>=5/6)))
    aha_zone_matrix.append(sec_1)
    aha_zone[sec_1]=1
    return aha_zone_matrix

def image3DToXYZ(img_size: np.ndarray, affine_matrix: np.ndarray):
    '''
    outputs the world cordinates of image of shape img_size, calculated using the affine_matrix
    '''
    xv, yv ,zv = np.meshgrid(np.linspace(0, img_size[0], img_size[0]),
                         np.linspace(0, img_size[1], img_size[1]),np.linspace(0, img_size[2], img_size[2]), indexing='ij')
    one_vector = np.ones_like(xv)
    PixCoords = np.concatenate((xv.reshape(-1, 1, order='F'), yv.reshape(-1, 1, order='F'),zv.reshape(-1, 1, order='F'),one_vector.reshape(-1, 1, order='F')), 1)
    WorldCoords = np.dot(affine_matrix, PixCoords.T).T

    return WorldCoords[:, 0:3]
