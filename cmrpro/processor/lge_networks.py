__all__ = ["load_lge_net"]


import segmentation_models_pytorch as smp
import torch
import os
import numpy as np

def load_lge_net():
    '''
    load LGE segmentation network
    '''
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    model_path = f"{os.path.dirname(__file__)}/networks/LGE_SAX/network"
    model = smp.Unet(
    encoder_name="resnet34",  # choose encoder, e.g. mobilenet_v2 or efficientnet-b7
    encoder_weights="imagenet",  # use `imagenet` pre-trained weights for encoder initialization
    in_channels=1,  # model input channels (1 for gray-scale images, 3 for RGB, etc.)
    classes=3,  # model output channels (number of classes in your dataset)
    activation='sigmoid').to(device)
    model.load_state_dict(torch.load(model_path, map_location=device))
    model.eval()
    return model

def prediction_to_mask(predictions):
    '''
    converts a segmentation with channels: LV myocard, >sd2, >sd5 to output masks: std2to5, healthy, >std5
    '''

    print('prediction_to_mask')
    import matplotlib.pyplot as plt

    #assuming 3 channels (LV myocard, SD2, SD5) as input labels
    #calculate labels(std2to5, healthy, >std5)
    healthy=np.clip(predictions[:,:,:,0]-predictions[:,:,:,1],0,1)
    std2to5=np.clip(predictions[:,:,:,2]-predictions[:,:,:,1],0,1)
    std5=np.clip(predictions[:,:,:,2],0,1)

    segmentation_masks=np.stack((std2to5,healthy,std5),axis=-1)
    # print(segmentation_masks.shape)
    #visualize original and calculated labels
    # for slice in range(segmentation_masks.shape[0]):
    #     fig, axs = plt.subplots(2,3)
    #     axs[0,0].imshow(predictions[slice,:,:,0])
    #     axs[0,0].set_title('LV myocard')
    #     axs[0,1].imshow(predictions[slice,:,:,1])
    #     axs[0,1].set_title('SD 2')
    #     axs[0,2].imshow(predictions[slice,:,:,2])
    #     axs[0,2].set_title('SD 5')
    #
    #     axs[1,0].imshow(segmentation_masks[slice,:,:,0])
    #     axs[1,0].set_title('SD 2 < x < SD 5')
    #     axs[1,1].imshow(segmentation_masks[slice,:,:,1])
    #     axs[1,1].set_title('healthy')
    #     axs[1,2].imshow(segmentation_masks[slice,:,:,2])
    #     axs[1,2].set_title('SD 5')
    #     plt.show()
    return segmentation_masks
