__all__ = ["process_cine_exam","process_cine_xam_multi_class","process_cine_series","get_multi_class_segmentation",
           "get_region_of_interest_center","crop_region_of_interest"]

import numpy as np
import torch

import scipy.ndimage
from nibabel.affines import apply_affine

import cmrpro.io.utils
import cmrpro.processor.cine_networks
import cmrpro.io
from cmrpro.io._processed_exam import ProcessedExamObject

def process_cine_exam(input_exam, external_lge_segmentation: bool =False, useTTA: bool = True) -> ProcessedExamObject:
    """ Segments all contained series (cine and lege) and stores them in a new ProcessedExam object"""
    #construct an empty ProcessedExamObject
    peo = ProcessedExamObject()
    input_exam.standardise_time_frames()

    add_stack_orientation = True
    #processes all series within the exam (CMRExam object) including segmentation; add processed series to the empty ProcessedExamObject
    #processes cine data
    for serie in input_exam.series:
        if serie.view.lower() == 'sax' and serie.stack_orientation is not None and add_stack_orientation:
            peo.stack_orientation_fromSAX = serie.stack_orientation
            add_stack_orientation = False
        #process current cine series (find ROI +segmentation[classes: lm-myo, lv-bp and rv-bp]) if useTTA==True default use average from 8 repetitions
        seg_predictions, series_coords, series_data, affine_res = process_cine_series(serie,useTTA)
        #add processed data to ProcessedExamObject
        peo.addSeries(seg_predictions, series_coords, series_data, serie.view,
                      serie.affine_original, affine_res)#, serie.stack_orientation)
        if serie.view=='SAX':
            temp_cine_seg=seg_predictions
            temp_cine_series=series_data
    #processes lge data
    for lges in input_exam.lge_series:
        if lges.view=='SAX':
            #get the time frame from the cine data that matches the lge data the best
            lge_trigger_time_frame=get_matching_time_frame(input_exam,lges)
            #process current lge series (preprocess+segment);
            #only preprocessing if external_lge_segmentation==True (segmentation is done outside the pipeline in this case, enables using other networks or manual segmentations for comparison)
            seg_predictions_lge, series_coords_lge, series_data_lge, seg_LV = preprocess_lge_seires(input_exam, lges, external_lge_segmentation, True)
                #can only be used when lge segmentations are calculated internaly
            # visualize_lge_seg(seg_predictions_lge,series_data_lge,temp_cine_seg[lge_trigger_time_frame,:,:,:],temp_cine_series[lge_trigger_time_frame,:,:,:],input_exam.base_dir)
            #add processed data to ProcessedExamObject
            peo.addLGESeries(seg_predictions_lge, series_coords_lge, series_data_lge, lges.view,
                          lges.affine_original, lges.affine,lge_trigger_time_frame,seg_LV)
    return peo

def process_cine_exam_multi_class(input_exam):
    peo = ProcessedExamObject()
    for counter,serie in enumerate(input_exam.series):
        seg_predictions, series_coords, data = get_multi_class_segmentation(serie,view=serie.view)
        peo.addSeries(seg_predictions, series_coords, data, serie.view,
                      serie.affine_original, serie.affine)
    return peo

def preprocess_lge_seires(input_exam,input_serie, external_lge_segmentation:bool, useTTA: bool):
    """"Precrocesses the lge data; if  external_lge_segmentation==True stores the preproceed data (for external segmentation); else: applys LGE segmentation network
    input_serie: leg serie"""
    center_of_mass=None
    #get center of mass from cine data
    for s in input_exam.series:
        if s.view=='SAX':
            center_of_mass = get_region_of_interest_center(s)
            #compensate for difference to cine data in pixel_spacing
            center_of_mass[0]=center_of_mass[0]*s.pixel_spacing[-2]/input_serie.pixel_spacing[-2]
            center_of_mass[1]=center_of_mass[1]*s.pixel_spacing[-1]/input_serie.pixel_spacing[-2]
            print(center_of_mass)
    # if no center of mass can be detected from the cine data (e.g. no SAX Cine image available)
    if center_of_mass is None:
        center_of_mass=[200,200]#first guess: set default
    print(center_of_mass)
    #data preprocessing (corp,zoom...)
    series_data, series_coords = input_serie.get_preprocessed_data(res=1., sz=192,
                                                            clip_percentile=99.5,
                                                            normalize=True,
                                                            return_pixels_added=False,
                                                            center=center_of_mass,
                                                            check_plots=False)

    if external_lge_segmentation==True:
        #if external lge segmentation is used (manual or from tensorflow based network)
        #place holder for segmentation
        seg_predictions = np.zeros((1, input_serie.slices, 3, 192, 192))
        seg_LV=np.zeros(( input_serie.slices,192, 192))
        #export series data for external segementation
        export_series_data(series_data,input_exam.base_dir)
    else:
        #if internal network is used --> segment preprocessed data
        #load trained lge segmenation network
        lgeNet = cmrpro.processor.lge_networks.load_lge_net()
        #apply network in the same way as the cine network is applyed to the cine data (same function)
        seg_predictions = cmrpro.processor.cine_networks.apply_sax_net(series_data, lgeNet, useTTA)
        #shift the label channel to the last index
        seg_predictions=np.transpose(seg_predictions, (0,2,3,1))
        #using current lge network output: LV myocard, >sd2, >sd5 --> change to seg_LV=sum(std2to5, healthy, >std5)
        seg_LV=seg_predictions[:,:,:,0]
        #correct other choise of output labels (network output: LV myocard, >sd2, >sd5 expected output for postprocessing: std2to5, healthy, >std5) can be left out when lge segmentation network is updated
        seg_predictions = cmrpro.processor.lge_networks.prediction_to_mask(seg_predictions)
    return seg_predictions, series_coords, series_data, seg_LV

def visualize_lge_seg(seg_predictions,series_data,seg_predictions_cine,series_data_cine,dir):
    """visualize overlay predictions scar segmentation and cine/lge image/cine mask"""
    import os
    if not os.path.exists(dir+'/ovelay_segmentations/'):
        os.makedirs(dir+'/ovelay_segmentations/')
    seg_predictions_cine=np.transpose(seg_predictions_cine,(0,2,3,1))
    print('visualize')
    print(seg_predictions.shape)
    print(series_data.shape)
    print(seg_predictions_cine.shape)
    print(series_data_cine.shape)
    import matplotlib.pyplot as plt
    for slice in range(seg_predictions.shape[0]):
        fig, axs = plt.subplots(3,3)

        axs[0,0].imshow(series_data[slice,0,:,:], 'gray')
        axs[0,1].imshow(series_data[slice,0,:,:], 'gray')
        axs[0,2].imshow(series_data[slice,0,:,:], 'gray')

        axs[0,0].imshow(seg_predictions_cine[slice,:,:,1], 'jet',  alpha=0.5*(seg_predictions_cine[slice,:,:,1]>0.25))
        axs[0,1].imshow(seg_predictions[slice,:,:,1],'jet',  alpha=0.5*(seg_predictions[slice,:,:,1]>0.25))
        axs[0,2].imshow(seg_predictions[slice,:,:,2],'jet',  alpha=0.5*(seg_predictions[slice,:,:,2]>0.25))

        axs[1,0].imshow(series_data_cine[slice,:,:], 'gray')
        axs[1,1].imshow(series_data_cine[slice,:,:], 'gray')
        axs[1,2].imshow(series_data_cine[slice,:,:], 'gray')
        axs[1,0].imshow(seg_predictions_cine[slice,:,:,1], 'jet',  alpha=0.5*(seg_predictions_cine[slice,:,:,1]>0.25))
        axs[1,1].imshow(seg_predictions[slice,:,:,1],'jet',  alpha=0.5*(seg_predictions[slice,:,:,1]>0.25))
        axs[1,2].imshow(seg_predictions[slice,:,:,2],'jet',  alpha=0.5*(seg_predictions[slice,:,:,2]>0.25))

        axs[2,0].imshow(series_data[slice,0,:,:], 'gray')
        axs[2,1].imshow(seg_predictions_cine[slice,:,:,1], 'gray')
        axs[2,1].imshow(seg_predictions[slice,:,:,1],'jet',  alpha=0.5*(seg_predictions[slice,:,:,1]>0.25))
        axs[2,2].imshow(seg_predictions[slice,:,:,1], 'gray')
        axs[2,2].imshow(seg_predictions_cine[slice,:,:,1], 'jet',  alpha=0.5*(seg_predictions_cine[slice,:,:,1]>0.25))
        # plt.show()
        plt.savefig(dir+'/ovelay_segmentations/seg_slice_'+str(slice)+'.png')
        plt.close()

    return

def get_matching_time_frame(input_exam,lges):
    """finds the time frame from the sax cine series corresponding to the trigger time of the lge image
        uses the averagetrigger time over the slices
    """
    for s in input_exam.series:
        if s.view=='SAX':
            trigger_time_cine=np.mean(s.trigger_time,axis=1)
            print('cie trigger time')
            print(trigger_time_cine)
    lge_trigger_time=np.mean(lges.trigger_time)
    print(lge_trigger_time)
    matching_time_frame=np.argmin(np.abs(trigger_time_cine-(np.ones(trigger_time_cine.shape)*lge_trigger_time)))
    print('matching time frame')
    print(matching_time_frame)
    return matching_time_frame

def process_cine_series(input_serie, useTTA: bool):
    """performs the following steps:
        1. locates the region of interest in the series (i.e. tries to find the heart)
        2. resized to 1mm x 1mm and crops out a 192x192 image centered on the ROI
        3. segments the images into thre classes: lm-myo, lv-bp and rv-bp
        4. returns the segmentations, the coordinates, and the 192x192 images

    :param useTTA: if true, test-time-augmentation is used for prediction ('auto'/default setting 8 iteration for tta average)
    """
    torch.set_default_dtype(torch.float32)
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    center_of_mass = get_region_of_interest_center(input_serie)

    saxNet = cmrpro.processor.cine_networks.load_sax_net().to(device)
    laxNet = cmrpro.processor.cine_networks.load_lax_net().to(device)

    series_data, series_coords, affine_res = input_serie.get_preprocessed_data(res=1., sz=192,
                                                            clip_percentile=99.5,
                                                            normalize=True,
                                                            return_pixels_added=False,
                                                            center=center_of_mass,
                                                            check_plots=False)


    if input_serie.view == 'SAX':
        seg_predictions = cmrpro.processor.cine_networks.apply_sax_net(series_data, saxNet, useTTA)
    else:
        seg_predictions = cmrpro.processor.cine_networks.apply_lax_net(series_data, laxNet, useTTA)

    seg_predictions = seg_predictions.reshape((input_serie.frames, input_serie.slices, 3, 192, 192))

    input_serie.seg = True
    series_data = series_data.reshape((input_serie.frames, input_serie.slices, 192, 192))
    return seg_predictions, series_coords, series_data, affine_res

def get_multi_class_segmentation(input_serie, view: str = 'SAX'):
    """extracts the central 256x256mm region of the data and applies many-class segmentation"""

    # get the image data at the required size/resolution centered on the heart:
    series_data, series_coords = crop_region_of_interest(input_serie, 256, 1.0, True)

    # load the multi class segmentation network and apply it to the data:
    mcUNet = cmrpro.processor.cine_networks.load_multi_class_unet(view)
    seg_predictions = cmrpro.processor.cine_networks.apply_multi_class_unet(series_data, mcUNet)

    # select the most probable class for each pixel
    seg_predictions = seg_predictions.argmax(axis=1)
    seg_predictions = seg_predictions.reshape((input_serie.frames, input_serie.slices, 256, 256))

    # todo: currently this is labelled with integer class values (12 classes), but possibly
    #  should be reshaped to (self.frames, self.slices, 12, 256, 256) and represented one-hot?
    # return the segmentation results, the spatial postions of the extracted ROIs,
    # and the corresponding image data
    series_data = series_data.reshape((input_serie.frames, input_serie.slices, 256, 256))
    return seg_predictions, series_coords, series_data

def get_region_of_interest_center(series_object) -> np.ndarray:
    """Returns the 2D point that is at the center of the region of interest (i.e. should be the
    center of the heart) for the series

    Note: the returned point is in mm from the edge of the image (i.e. to get the central pixel
    you need to factor in the image resolution)
    -> For an example of using the returned point to crop the image around the ROI see
    the .cropRegionOfInterest() method

    :return: array of shape (n_slices, 2) containing the coordinates
    """
    torch.set_default_dtype(torch.float32)
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    roiNet = cmrpro.processor.cine_networks.load_roi_net()

    series_data, _, pixels_added,_ = series_object.get_preprocessed_data(res=2.0, sz=256, clip_percentile=99.5,
                                                                       normalize=True,
                                                                       return_pixels_added=True)

    roi_predictions = cmrpro.processor.cine_networks.apply_roi_net(series_data, roiNet)

    centers = []
    for p in roi_predictions:
        centers.append(scipy.ndimage.center_of_mass(p[0]))
    centers = np.array(centers)
    center_of_mass = np.array([np.median(centers[:, 0]), np.median(centers[:, 1])])

    center_of_mass[0] -= pixels_added[0][0]
    center_of_mass[1] -= pixels_added[1][0]
    center_of_mass[0] *= 2. / series_object.pixel_spacing[-2]
    center_of_mass[1] *= 2. / series_object.pixel_spacing[-1]
    return center_of_mass

def crop_region_of_interest(input_serie, image_size=128, image_resolution=1., return_coords=False):
    """Returns the series cropped and scaled to the given values, centered on the heart"""
    #estimates the location of the center of the mask
    center_of_mass = get_region_of_interest_center(input_serie)
    #data preprocessing (corp,zoom...)
    series_data, series_coords = input_serie.get_preprocessed_data(res=image_resolution, sz=image_size,
                                                            clip_percentile=99.5,
                                                            normalize=True,
                                                            return_pixels_added=False,
                                                            center=center_of_mass)

    if not return_coords:
        return series_data
    else:
        return series_data, series_coords
def export_series_data(series_data, path_out):
    "exports the preprocessed series data (used for external lge segementation)"
    np.save(path_out+'/lge_data_preprocessed.npy',series_data)
