import torch
import torch.nn as nn
#from torchvision.transforms import functional as F

class DoubleConv(nn.Module):
    def __init__(self, in_channels, out_channels):
        super().__init__()

        self.network = nn.Sequential(
            nn.Conv2d(in_channels, out_channels, 3, padding=1),
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(out_channels),
            nn.Conv2d(out_channels, out_channels, 3, padding=1),
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(out_channels),
        )

    def forward(self, x):
        return self.network(x)


class EncoderBlock(nn.Module):
    def __init__(self, in_channels, out_channels):
        super().__init__()

        self.double_conv = DoubleConv(in_channels, out_channels)
        self.downsample = nn.MaxPool2d(2, stride=2)

    def forward(self, x):
        x = self.double_conv(x)
        return x, self.downsample(x)


class DecoderBlock(nn.Module):
    def __init__(self, in_channels, out_channels):
        super().__init__()

        self.upsample = nn.Sequential(
            nn.UpsamplingBilinear2d(scale_factor=2),
            nn.Conv2d(in_channels, out_channels, 2, padding=1),
        )
        self.double_conv = DoubleConv(in_channels, out_channels)

    def forward(self, x, skip):

        x = self.upsample(x)
        x = F.resize(x, skip.shape[2:])
        x = torch.cat([x, skip], dim=1)
        x = self.double_conv(x)
        return x


class Encoder(nn.Module):
    def __init__(self, channels=[1, 64, 128, 256, 512]):
        super().__init__()

        self.blocks = nn.ModuleList(
            [EncoderBlock(channels[i], channels[i+1])
            for i in range(len(channels)-1)]
        )

    def forward(self, x):
        skips = []

        for block in self.blocks:
            x_skip, x = block(x)
            skips.append(x_skip)

        return x, skips


class Decoder(nn.Module):
    def __init__(self, channels=[1024, 512, 256, 128, 64]):
        super().__init__()

        self.blocks = nn.ModuleList(
            [DecoderBlock(channels[i], channels[i+1])
            for i in range(len(channels)-1)]
        )

    def forward(self, x, skips):

        for block, x_skip in zip(self.blocks, skips):
            x = block(x, x_skip)

        return x

class MultiClassUNet(nn.Module):

    def __init__(self, in_channels, n_classes, channels=[64, 128, 256, 512], channels_bottleneck=1024):
        super().__init__()

        self.conv = nn.Conv2d(channels[0], n_classes, 1)

        self.encoder = Encoder([in_channels, *channels])

        self.conv_bottleneck = DoubleConv(channels[-1], channels_bottleneck)

        self.decoder = Decoder([channels_bottleneck, *channels[::-1]])

        self.output_conv = nn.Conv2d(channels[0], n_classes, 1)

        self.softmax = nn.Softmax(dim=1)

    def forward(self, x):

        x, skips = self.encoder(x)
        x = self.conv_bottleneck(x)
        x = self.decoder(x, skips[::-1])
        x = self.output_conv(x)

        return self.softmax(x), x

