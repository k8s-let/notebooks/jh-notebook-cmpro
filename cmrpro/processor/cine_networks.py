__all__ = ["predict_with_test_time_augmentation", "load_roi_net", "load_lax_net", "load_sax_net",
           "load_multi_class_unet",
           "apply_roi_net", "apply_lax_net", "apply_sax_net", "apply_multi_class_unet"]
from typing import Union

import numpy as np
import os
from tqdm import tqdm
import torch
torch.set_default_dtype(torch.float32)
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
import segmentation_models_pytorch as smp

# import required model architectures:
from cmrpro.processor import MultiClassUNet
from cmrpro.processor import CineResUNet

def predict_with_test_time_augmentation(model: torch.nn.Module,
                                        data: Union[np.ndarray, torch.Tensor]) -> np.ndarray:
    """Applies a pytorch model to given image data using test time augmentation, specifically,
    it produces the 8 predictions resulting from all 8 possible 90 degree rotations + reflections
    the predictions are all transformed back to the original orientation (so in the end you can
    e.g. just take the average)

    :param model:
    :param data: array of shape (N, 1, X, Y) input 2D images in (B, C, H, W) format for torch
    """
    all_res = []
    model.eval()
    for i in range(data.shape[0]):
        res = []
        for rot in [0, 1, 2, 3]:
            # rotation:
            pred = torch.rot90(model(torch.rot90(data[i:i + 1], rot, dims=(2, 3))),
                               (4 - rot), dims=(2, 3)).to('cpu').detach().numpy()
            res.append(pred[0])
            # rotation with reflection
            pred = torch.flip(torch.rot90(
                                          model(torch.rot90(torch.flip(data[i:i + 1], dims=[2, ]),
                                                            rot, dims=(2, 3))), (4 - rot),
                                          dims=(2, 3)),
                                dims=[2, ]).to('cpu').detach().numpy()
            res.append(pred[0])

        all_res.append(np.array(res))

    all_res = np.array(all_res)

    return all_res


def load_multi_class_unet(view: str) -> torch.nn.Module:
    """Loads the multi-class segmentation network for short axes views, contained in this package
    """
    view = view.lower()

    if view.lower() not in ['lax', 'sax']:
        print(f"uknown view ({view}) requested when loading multi-class segmentation network.."
              "\ndefaulting to SAX view")
        view = 'sax'

    if view.lower() != 'sax':
        print('TODO: currently applying SAX multi-class segmentation network to all views'
              ' (e.g. including LAX) Gloria has a trained multi-class LAX network, which needs to'
              ' be added to the code here for LAX cases')
        view = 'SAX'

    model = MultiClassUNet(1, 12)
    model_path = f"{os.path.dirname(__file__)}/networks/MultiOrgan/{view.lower()}_multiclass_seg_network.pth"
    model.load_state_dict(torch.load(model_path, map_location=device))
    model = model.to(device)
    model.eval()
    return model


def apply_multi_class_unet(data: Union[np.ndarray, torch.Tensor], mcUnet: torch.nn.Module,
                           batch_size: int = 8) -> np.ndarray:
    """Evaluates the prediction of the multi-class segmentation network on the given data"""
    print('doing multi-class segmentation on data with shape:', data.shape)

    predictions = []
    for i in tqdm(range(0, data.shape[0], batch_size)):
        batch = data[i:min(i + batch_size, data.shape[0])]
        with torch.no_grad():
            batch_predictions = mcUnet(torch.Tensor(batch * 255).to(device))[0].cpu().numpy()
        predictions.append(batch_predictions)
    return np.concatenate(predictions)


def load_roi_net() -> torch.nn.Module:
    """Load the region \proposal network shipped with this package"""
    model = smp.Unet(
        encoder_name="resnet18",
        encoder_weights="imagenet",
        in_channels=1,
        classes=2,
    ).to(device)
    model_path = f"{os.path.dirname(__file__)}/networks/ROI/trained_roi_unet_with_classifier"
    model.load_state_dict(torch.load(model_path, map_location=device))
    model.eval()
    return model


def apply_roi_net(data: np.ndarray, roiNet: torch.nn.Module, batch_size='auto'):
    """ Evaluates the region of interest network"""
    print('predicting ROI..', device, batch_size)

    if batch_size == 'auto':
        if str(device) == 'cpu':
            batch_size = 1
        else:
            batch_size = 8

    print(data.shape, device, batch_size)

    predictions = []
    for i in tqdm(range(0, data.shape[0], batch_size)):
        batch = data[i:min(i + batch_size, data.shape[0])]
        with torch.no_grad():
            batch_predictions = torch.sigmoid(roiNet(torch.Tensor(batch).to(device))).cpu().numpy()
        predictions.append(batch_predictions)
    return np.concatenate(predictions)


def load_sax_net() -> torch.nn.Module:
    """Loads the network for short-axis LV/RV/bloodpool segmentation shipped with this package"""
    net = CineResUNet(1, 3).to(device)
    model_path = f"{os.path.dirname(__file__)}/networks/SAX/SAXsegnet_192_refined"
    net.load_state_dict(torch.load(model_path, map_location=device))
    net.eval()
    return net


def apply_sax_net(data: np.ndarray, saxNet: torch.nn.Module,
                  useTTA: bool, batch_size='auto'):
    """Evaluates prediction of the network for short-axis LV/RV/bloodpool
    segmentation shipped with this package"""
    print('segmenting..', device, batch_size)

    if batch_size == 'auto':
        if str(device) == 'cpu':
            batch_size = 1
        else:
            batch_size = 8

    print(data.shape, device, batch_size)

    predictions = []
    for i in tqdm(range(0, data.shape[0], batch_size)):
        batch = data[i:min(i + batch_size, data.shape[0])]
        with torch.no_grad():
            if useTTA:
                batch_predictions = np.mean(predict_with_test_time_augmentation(saxNet, torch.Tensor(batch).to(device)), axis=1)
            else:
                batch_predictions = saxNet(torch.Tensor(batch).to(device)).cpu().numpy()
        predictions.append(batch_predictions)

    return np.concatenate(predictions)


def load_lax_net() -> torch.nn.Module:
    """Loads he network for long-axis LV/RV/bloodpool
    segmentation shipped with this package"""
    net = CineResUNet(1, 3).to(device)
    model_path = f"{os.path.dirname(__file__)}/networks/LAX/LAX_192x192@1mm"
    net.load_state_dict(torch.load(model_path, map_location=device))
    net.eval()
    return net


def apply_lax_net(data: np.ndarray, laxNet: torch.nn.Module, useTTA: bool, batch_size=1):
    """Evaluates prediction of the network for long-axis LV/RV/bloodpool
    segmentation shipped with this package"""
    segs = apply_sax_net(data, laxNet, useTTA, batch_size)  # works the same way
    # but I "hilariously" assigned different channels to the bloodpools when training,
    # so I swap them here for the LAX model
    segs = segs[:, ::-1]
    return segs
