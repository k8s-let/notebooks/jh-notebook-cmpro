__all__ = ["PointToMeshDistances"]

import torch
import numpy as np

device ='cpu'#  torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
class PointToMeshDistances(torch.nn.Module):

	"""This is a small class that lets you use pytroch3D's code for calulating the distance between
	points and a triangulated surface mesh, without needing to use the pytorch3D data structures
	more generally

	:param pt_faces:
	"""
	#: Contains the indice of vertices forming the faces vectors of all surface mesh triangles
	mesh_vertices: torch.Tensor
	#:
	# first_index: torch.Tensor

	def __init__(self, pt_faces: torch.Tensor):
		"""pt_faces should be a pytorch tensor with shape (num_faces,3) where num_faces is the
		number of faces in the mesh and each row contains the three vertex indecies for the
		triangular face
		"""
		super().__init__()

		self.faces = pt_faces
		self.verts = torch.unique(pt_faces)

	def forward(self, SM_points: torch.Tensor, mask_points: torch.Tensor) -> torch.Tensor:
		"""Evaluates the distance of all points to all verts

		:param SM_points: of shape (M, 3) position of mesh vertices  3D space
		:param points: of shape (N, 3) position of segmentation mask points in 3D space
		:returns: an array of shape (num_points,) giving the distance of each point to the
		closes point on the surface mesh
		"""
		SM_coords = SM_points[self.verts,:]

		distances, ids_position = torch.min(torch.cdist(mask_points,SM_coords,p=2.0),1)

		return distances
