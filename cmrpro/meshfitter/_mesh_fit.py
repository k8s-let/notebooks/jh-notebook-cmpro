__all__ = ["MeshModel"]

from typing import List, Tuple, Union, Any
import os
import logging

import torch
import numpy as np
import meshio
import imageio
from tqdm import tqdm
import pyvista as pv

from scipy import ndimage

from cmrpro.meshfitter._point_to_mesh_distance import PointToMeshDistances
from cmrpro.meshfitter._learnable_mesh import LearnableMesh
from cmrpro.meshfitter._slice_data import SliceData

import cmrpro.meshfitter.utils

import vtk

from PIL import Image

_SHAPE_MODEL_FOLDER = f"{os.path.dirname(__file__)}/Shape_model_ML_v3"

torch.set_default_dtype(torch.float32)
device = 'cpu'#torch.device('cuda' if torch.cuda.is_available() else 'cpu')

class MeshModel:
    """Takes a ProcessedExamObject object and prepares the mesh model for mesh fitting (fit function)

    :param ProcessedExamObject:
    :param out_folder:
    :param sm_modes:
    """
    cmr_exam: 'ProcessedExamObject'
    out_path: str
    point_to_endo_distance: 'PointToMeshDistances'  # subclass of torch module
    point_to_epi_distance: 'PointToMeshDistances'  # subclass of torch module
    mesh: 'meshio.Mesh'
    model: 'LearnableMesh'
    segmentations: List[np.ndarray]
    series_coords: List[np.ndarray]
    affine_matrix: List[np.ndarray]
    affine_matrix_resampled: List[np.ndarray]
    view_available: List
    #: Is equivalent to len(self.segmentations[0])
    time_steps: int

    def __init__(self, cmr_exam, out_folder: str, SM_modes: int = 15, n_control_points=9):

        self.cmr_exam = cmr_exam
        self.out_path = out_folder

        # endo surface faces in SM coordinate system (Origin at MV plane):
        # returns surface in term of mesh indices and face connectivity
        _, faces  = self.make_triangular_surface_mesh('endo')  # get the endo mesh surface

        pt_endo_faces = torch.tensor(faces, dtype=torch.long).to(device)
        self.point_to_endo_distance = PointToMeshDistances(pt_endo_faces)

        self.valve_pts = np.squeeze(np.load(f"{_SHAPE_MODEL_FOLDER}/Boundary_nodes/VALVE_points.npy"))

        # epi surface faces in SM coordinate system (Origin at MV plane):
        # returns surface in term of mesh indices and face connectivity
        _, faces  = self.make_triangular_surface_mesh('epi')  # get the epi mesh surface
        pt_epi_faces = torch.tensor(faces, dtype=torch.long).to(device)
        self.point_to_epi_distance = PointToMeshDistances(pt_epi_faces)

        # Load reference shape model
        self.mesh  = meshio.read(f"{_SHAPE_MODEL_FOLDER}/Mean/LV_mean.vtk")
        self.model = LearnableMesh(num_modes_to_use=SM_modes,k=n_control_points).to(device)

        # Set lists to track info per slice
        self.images        = []
        self.segmentations = []
        self.series_coords = []
        self.affine_matrix = []
        self.affine_matrix_resampled = []
        self.view_available = []
        self.stack_orientation = None

        if cmr_exam.stack_orientation_fromSAX is not None:
            self.stack_orientation = cmr_exam.stack_orientation_fromSAX

        # Here we load all the segmentation masks and also clean them up a using simple heursitics
        for i, frame in enumerate(cmr_exam.series):
            # Should be in [time_frames, slices, H, W, seg_channels] (target shape)
            sp    = np.round(np.moveaxis(frame['seg'], 2, -1) * 1.)
            simag = frame['img']

            imageio.imwrite(self.out_path + '/series_%s_%d.png' % (frame['view'],i),
                            Image.fromarray(np.concatenate(np.concatenate(sp*255, 2)).astype(np.uint8)))

            sp = cmrpro.meshfitter.utils.cleanSegmentationData(sp)

            self.images.append(simag)
            self.segmentations.append(sp)
            self.affine_matrix.append(frame['affine'])
            self.affine_matrix_resampled.append(frame['affineRes'])

            image_shape = sp.shape[2:4]
            n_slices = sp.shape[1]

            #sc = np.stack(self.image_coords_affine([*image_shape, n_slices], frame['affineRes']), -1)
            # X, Y, Z = self.image_coords_affine([*image_shape, n_slices], frame['affineRes'])
            # sc = np.zeros((*image_shape, n_slices, 3))
            # sc[..., 0] = X
            # sc[..., 1] = Y
            # sc[..., 2] = Z
            # self.series_coords.append(np.moveaxis(sc, 2, 0))

            self.view_available.append(frame['view'])

        self.time_steps = self.segmentations[0].shape[0]

    @staticmethod
    def image_coords_affine(img_size: Tuple[int, int, int], affine_matrix: np.ndarray) \
            -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        """Transforms the integer-image coordinates of a 3D image by using the affine
        transformation.

        :param img_size: (3, ) Tuple containing the dimension of the image
        :param affine_matrix:
        :return: X, Y, Z coordinates
        """
        xv, yv, zv = np.meshgrid(np.linspace(0, img_size[0], img_size[0]),
                                 np.linspace(0, img_size[1], img_size[1]),
                                 np.linspace(0, img_size[2], img_size[2]), indexing='ij')
        one_vector = np.ones_like(xv)
        pix_coords = np.concatenate((xv.reshape(-1, 1, order='F'), yv.reshape(-1, 1, order='F'),
                                     zv.reshape(-1, 1, order='F'),
                                     one_vector.reshape(-1, 1, order='F')), 1)
        world_coords = np.dot(affine_matrix, pix_coords.T).T

        X = world_coords[:, 0].reshape(xv.shape, order='F')
        Y = world_coords[:, 1].reshape(xv.shape, order='F')
        Z = world_coords[:, 2].reshape(xv.shape, order='F')
        return X, Y, Z

    @staticmethod
    def slice_coords_affine(img_size, sliceN, affine_matrix):
        """Transforms the integer-image coordinates of a 2D image slice by using the affine
        transformation to world coordinates."""
        xv, yv, zv = np.meshgrid(np.linspace(0, img_size[0], img_size[0]),
                                 np.linspace(0, img_size[1], img_size[1]), indexing='ij')
        one_vector = np.ones_like(xv)
        PixCoords = np.concatenate((xv.reshape(-1, 1, order='F'), yv.reshape(-1, 1, order='F'),
                                    sliceN * one_vector.reshape(-1, 1, order='F'),
                                    one_vector.reshape(-1, 1, order='F')), 1)
        WorldCoords = np.dot(affine_matrix, PixCoords.T).T

        X = WorldCoords[:, 0].reshape(xv.shape, order='F')
        Y = WorldCoords[:, 1].reshape(xv.shape, order='F')
        Z = WorldCoords[:, 2].reshape(xv.shape, order='F')

        return X, Y, Z

    @staticmethod
    def plane_to_grid(img_size, slice_index, affine_matrix):
        """Transforms the integer-image coordinates of a 2D image slice by using the affine
        transformation to world coordinates and converts it into a stuctured grid."""
        xv, yv = np.meshgrid(np.linspace(0, img_size[0], img_size[0] + 1),
                             np.linspace(0, img_size[1], img_size[1] + 1), indexing='ij')
        one_vector = np.ones_like(xv)
        PixCoords = np.concatenate((xv.reshape(-1, 1, order='F'), yv.reshape(-1, 1, order='F'),
                                    slice_index * one_vector.reshape(-1, 1, order='F'),
                                    one_vector.reshape(-1, 1, order='F')), 1)
        WorldCoords = np.dot(affine_matrix, PixCoords.T).T

        X = WorldCoords[:, 0].reshape(xv.shape, order='F')
        Y = WorldCoords[:, 1].reshape(xv.shape, order='F')
        Z = WorldCoords[:, 2].reshape(xv.shape, order='F')
        grid = pv.StructuredGrid(X, Y, Z)

        return grid

    def saveDataImages(self):
        cp, lax, rvax = None, None, None
        cam_pos = None

        for time_step in range(self.time_steps):

            slices = []
            for i in range(len(self.segmentations)):  # gather slices from across each series..
                sp = self.segmentations[i][time_step]
                sc = self.series_coords[i]
                slices.append(np.concatenate([sp, sc], -1))

            # create a SliceData object of the target slices:
            sd = SliceData(slices, self.out_path, contour_res=5)
            cp = sd.set_center_point(cp)
            lax, rvax = sd.set_anatomical_axes(lax, rvax)
            sd.make_tensors()
            sd.learn_rotation = False
            sd.learn_offset = False

        # cam_pos = sd.show(cam_pos)

    def vti_writer(self,image,affine,out_filename):
        '''
        write data/image to vti file
        image: array with shape H*B*number of slices*segmentation channels
        affine: affine matrix of resamepled image
        '''
        vti_image = vtk.vtkImageData()
        vti_image.SetDimensions(image.shape[:3])
        vti_image.SetSpacing((1.0, 1.0, 1.0))
        vti_image.SetOrigin(tuple(affine[:3, 3]))
        Img2World_vtk = vtk.vtkMatrix3x3()
        for i in range(3):
            for j in range(3):
                Img2World_vtk.SetElement(i, j, affine[i, j])
        vti_image.SetDirectionMatrix(Img2World_vtk)
        for channel in range(3):
            linear_array = image[:,:,:,channel].reshape(-1, 1, order="F")
            vtk_array = numpy(linear_array)
            vtk_array.SetName('LV_'+str(channel))
            vti_image.GetPointData().AddArray(vtk_array)
            vti_image.Modified()

        writer = vtk.vtkXMLImageDataWriter()
        writer.SetFileName(out_filename)
        writer.SetInputData(vti_image)
        writer.Write()

    def export_segmentations_vti(self, target_folder: str, export_time_step, external_lge_segmentation: bool, export_lge: bool):
        """
        exports segmentations to vti in world coordinates
        """
        print(target_folder)
        vti_out = target_folder + '/' + 'segementations_VTI'
        os.makedirs(vti_out, exist_ok=True)

        slices, slice_views = self.get_slices_at_timestep(export_time_step, return_view=True)
        for sliceIter in range(len(slice_views)):
            seg = np.transpose(slices[sliceIter], (1,2,0,3))
            out_filename=vti_out + '/view_'+slice_views[sliceIter]+'.vti'
            self.vti_writer(seg,self.affine_matrix_resampled[sliceIter],out_filename)
            #for lge segmentation export
            if slice_views[sliceIter]=='SAX' and export_lge==True:
                for lges in self.cmr_exam.lge_series:
                    if lges['view']=='SAX':
                        # seg_lge=cmrpro.meshfitter.utils.cleanSegmentationData(lges['seg'])
                        seg_lge=lges['seg']
                        seg_lge=np.transpose(np.squeeze(seg_lge), (1,2,0,3))
                        for s in self.cmr_exam.series:
                            if s['view']=='SAX':
                                current_cine_image=s['img']
                        # self.visualize_lge_seg(seg_lge,lges['img'],seg,current_cine_image,target_folder,export_time_step)

                        shifts_lgetocine=self.adapt_slice_shift_lgetocine_sax(seg, seg_lge,lges['seg_LV'],target_folder, external_lge_segmentation)
                        # lge_seg_shifted=self.shift_lge_seg(seg,seg_lge,shifts_lgetocine)
                        if seg.shape[2]==seg_lge.shape[2]:
                            for currentSlice in range(seg_lge.shape[2]):
                                import matplotlib.pyplot as plt
                                fig, axs = plt.subplots(2,3)
                                axs[0,0].imshow(np.sum(seg_lge[:,:,currentSlice,:],axis=-1))
                                tmp=np.sum(seg_lge[:,:,currentSlice,:],axis=-1)
                                for channel in range(3):
                                    seg_lge[:,:,currentSlice,channel]=ndimage.shift(seg_lge[:,:,currentSlice,channel],-1*shifts_lgetocine[currentSlice])

                                axs[0,1].imshow(np.sum(seg_lge[:,:,currentSlice,:],axis=-1))
                                axs[0,2].imshow(seg[:,:,currentSlice,1])
                                axs[1,1].imshow(np.sum(seg_lge[:,:,currentSlice,:],axis=-1)+3*seg[:,:,currentSlice,1])
                                axs[1,0].imshow(3*seg[:,:,currentSlice,1]+tmp)
                                # plt.show()
                                plt.close()
                        out_filename=vti_out + '/lge_view_'+slice_views[sliceIter]+'.vti'
                        # self.vti_writer(seg_lge,self.affine_matrix_resampled[sliceIter],out_filename)
                        # print(lges['pixel_spacing'])
                        self.vti_writer(seg_lge,lges['affineRes'],out_filename)

    def adapt_slice_shift_lgetocine_sax(self,cine_seg, lge_seg,lge_seg_LV,dir,external_lge_segmentation):
        '''
        corrects slice mismatch between lge and cine by using the center of mass of the segmentation per slice
        visualizes the overlay of the mesh based LV mask and the sum of all lge segmentation channels (row 2); the center points and edge images (row 1)

        if the external_lge_segmentation flag is true the sum of all lge segmentation channels is used to compute the center of the LGE segmentation else the LV prediction of the LGE network is used
        '''
        if external_lge_segmentation==True:
            plot_columns=2
        else:
            plot_columns=3
        if not cine_seg.shape[2]==lge_seg.shape[2]:
            print('Attention: slice position missmatchbetween cine and lge data. Additional implementation of matching slice positions/interpolation in needed in function adapt_slice_shift_lgetocine_sax()')
            return np.zeros((lge_seg.shape[2],2))
        else:
            slice_shifts=[]
            for sliceIter in range(lge_seg.shape[2]):
                import matplotlib.pyplot as plt
                import scipy as sp
                fig, axs = plt.subplots(2,plot_columns)
                axs[0,0].imshow(np.transpose(cine_seg[:,:,sliceIter,1]))
                #get cine center based on mask
                center_cine= np.asarray(ndimage.measurements.center_of_mass(cine_seg[:,:,sliceIter,1]))
                # get edge based center
                sx = ndimage.sobel(cine_seg[:,:,sliceIter,1], axis=0, mode='constant')
                sy = ndimage.sobel(cine_seg[:,:,sliceIter,1], axis=1, mode='constant')
                sob = np.hypot(sx, sy)
                center_cine_edge= np.asarray(ndimage.measurements.center_of_mass(sob))
                #visualize center points (mass based  blue, edge based magenta)
                axs[0,0].scatter(center_cine[0],center_cine[1],color='b')
                axs[0,0].scatter(center_cine_edge[0],center_cine_edge[1],color='m')
                #get region of interest from cine
                roi_list=np.argwhere(cine_seg[:,:,sliceIter,1]>0.1)
                lge_seg_all_inroi=np.sum(lge_seg[:,:,sliceIter,:],axis=-1)
                lge_seg_all_inroi_LV=lge_seg_LV[sliceIter,:,:]
                #apply padded bounding box from cine mask to lge segmentation
                lge_seg_all_inroi[0:np.min(roi_list[:,0])-5,:]=0
                lge_seg_all_inroi[np.max(roi_list[:,0])+5:-1,:]=0
                lge_seg_all_inroi[:,0:np.min(roi_list[:,1])-5]=0
                lge_seg_all_inroi[:,np.max(roi_list[:,1])+5:-1]=0

                lge_seg_all_inroi_LV[0:np.min(roi_list[:,0])-5,:]=0
                lge_seg_all_inroi_LV[np.max(roi_list[:,0])+5:-1,:]=0
                lge_seg_all_inroi_LV[:,0:np.min(roi_list[:,1])-5]=0
                lge_seg_all_inroi_LV[:,np.max(roi_list[:,1])+5:-1]=0

                #mask based center
                center_lge=  np.asarray(ndimage.measurements.center_of_mass(lge_seg_all_inroi))
                #edge based center
                sx = ndimage.sobel(lge_seg_all_inroi, axis=0, mode='constant')
                sy = ndimage.sobel(lge_seg_all_inroi, axis=1, mode='constant')
                sob = np.hypot(sx, sy)
                center_lge_edge= np.asarray(ndimage.measurements.center_of_mass(sob))
                #LGE net output LV as reference for center (if the internal network with LV output is used)
                sx_LV = ndimage.sobel(lge_seg_all_inroi_LV, axis=0, mode='constant')
                sy_LV = ndimage.sobel(lge_seg_all_inroi_LV, axis=1, mode='constant')
                sob_LV = np.hypot(sx_LV, sy_LV)
                center_lge_edge_LV= np.asarray(ndimage.measurements.center_of_mass(sob_LV))
                #visualize different centers of masks
                axs[0,1].scatter(center_lge[0],center_lge[1],color='b')
                axs[0,1].scatter(center_lge_edge_LV[0],center_lge_edge_LV[1],color='c')
                axs[0,1].scatter(center_lge_edge[0],center_lge_edge[1],color='m')
                #visualize edge image
                axs[0,1].imshow(np.transpose(sob))
                if not external_lge_segmentation:
                    #visualize edge image and center for LGE net LV output
                    axs[0,2].imshow(np.transpose(sob_LV))
                    axs[0,2].scatter(center_lge_edge[0],center_lge_edge[1],color='m')
                    axs[0,2].scatter(center_lge_edge_LV[0],center_lge_edge_LV[1],color='c')

                #calculate shifts to align the LG geometry and LGE segmentations
                shifts_lgetocine_edge=center_lge_edge-center_cine_edge
                # shifts_lgetocine=center_lge-center_cine
                shifts_lgetocine_edge_LV=center_lge_edge_LV-center_cine_edge
                lge_seg_shifted_edge_LV=sp.ndimage.shift(lge_seg_all_inroi,-1*shifts_lgetocine_edge_LV)
                lge_seg_shifted_edge=sp.ndimage.shift(lge_seg_all_inroi,-1*shifts_lgetocine_edge)
                # lge_seg_shifted=sp.ndimage.shift(lge_seg_all_inroi,-1*shifts_lgetocine)

                #visualize shifted LGE segmentations overlay with cine mask
                axs[1,0].imshow(np.transpose(cine_seg[:,:,sliceIter,1]), 'gray')
                axs[1,0].imshow(np.transpose(lge_seg_all_inroi),'jet',  alpha=0.5*(np.transpose(lge_seg_all_inroi)>0.25))
                axs[1,0].set_title('no shift')
                axs[1,1].imshow(np.transpose(cine_seg[:,:,sliceIter,1]), 'gray')
                axs[1,1].imshow(np.transpose(lge_seg_shifted_edge),'jet',  alpha=0.5*(np.transpose(lge_seg_shifted_edge)>0.25))
                axs[1,1].set_title('sum edge shift')
                #visualize shifted LGE segmentations based on the LV mask of the LGE net prediction as overlay with cine mask
                if not external_lge_segmentation:
                    axs[1,2].imshow(np.transpose(cine_seg[:,:,sliceIter,1]), 'gray')
                    axs[1,2].imshow(np.transpose(lge_seg_shifted_edge_LV),'jet',  alpha=0.5*(np.transpose(lge_seg_shifted_edge_LV)>0.25))
                    axs[1,2].set_title('LV edge shift')

                # plt.show()
                #save images to folder
                import os
                if not os.path.exists(dir+'/shift_leg_seg/'):
                    os.makedirs(dir+'/shift_leg_seg/')
                plt.savefig(dir+'/shift_leg_seg/'+'slice_'+str(sliceIter)+'.png')
                plt.close()
                #not used: shifts based on center of mass of the masks (using edge based center)
                # slice_shifts.append(center_lge-center_cine)
                # print(center_lge-center_cine)
                #save shifts: for external network edge based on sum of labels; for internal network based on LV mask prediction of the LGE net (to avoid missmatch due to low uncertainty at label edges)
                if external_lge_segmentation:
                    slice_shifts.append(center_lge_edge-center_cine_edge)
                else:
                    slice_shifts.append(center_lge_edge_LV-center_cine_edge)
                # print(center_lge_edge-center_cine_edge)
            return slice_shifts

    def fit(self, save_slicing_results=True, save_mesh_images=True, show_each_frame=False,
            save_meshes=True, use_warp=False, allow_slice_shift=False,
            write_log: bool = True,force_valve_estimation=None,
            smooth_loss_weight=0.1, warp_loss_weight=0.3,
            mode_loss_weight=0.0001, sl_offset_loss_weight=0.5, sl_rotation_loss_weight=0.1):
        """
        fits a mesh to the cine segmentations for each time step
            1) slice offset estimation
            2) SliceData object of the target slices is created
            3) get center point and anatomical axis from first time frame
            4) fit mesh from shape model by optimization of a cost function (compute_loss(...))
            5) save transformations form world to shape model coordinates
            6) export meshes in world cordinates, shape model coordinates and image coordinates as vtk (all time frames)
            7) save sliceimages (overlay segmentation mask resulting shape model) if save_slicing_results==True
            8) save visualization of half LV mesh if save_mesh_images==True
        :param save_slicing_results:
        :param save_mesh_images:
        :param show_each_frame:
        :param save_meshes:
        :param use_warp:
        :param pre_compute_offsets:
        :param allow_slice_shift:
        :param reverse_LV_estimation: this is needed if the estimation is wrong and the LV is in
                the wrong direction
        :return:
        """
        slice_offset_lists = None

        # various properties that are tracked over the timesteps (initialised as None):
        cp, lax, rvax = None, self.stack_orientation, None
        cam_pos = None
        previous_mesh_points = None

        if write_log:
            logging.basicConfig(filename=f"{self.out_path}/mesh_fitting_log.txt",
                                level=logging.INFO)

        pbar_time = tqdm(range(self.time_steps), desc="Fitting Time step: ")

        f_valve_motion = open(self.out_path +'/MV_position.txt','w')
        for time_step in pbar_time:

            if time_step == self.time_steps - 2:
                # print('smoothing 25%')
                previous_mesh_points = previous_mesh_points * 0.75 + mesh_at_timestep_0 * 0.25
            elif time_step == self.time_steps - 1:
                # print('smoothing 50%')
                previous_mesh_points = previous_mesh_points * 0.5 + mesh_at_timestep_0 * 0.5

            # get the slices for this timestep
            # segmentations and image point coordinates
            slices, images, slice_views, img2world_matrices = self.get_slices_at_timestep(time_step, return_view=True)

            # create a SliceData object of the target slices
            sd = SliceData(slices, self.stack_orientation, self.out_path, img2world_matrices, force_valve_estimation, images, slice_views,contour_res=5,
                           slice_offset_lists=slice_offset_lists)
            # if lax and rvax are None (i.e. on the first timestep) then these are computed and returned,
            # otherwise they are just set to the given value (i.e. we calculate the values on the first time
            # frame and then use those results over all time frames to keep a constant coordinate system)
            f_valve_motion.write('%d %.4f %.4f %.4f' %(time_step,
                                                       sd.valve_plane_center[0],
                                                       sd.valve_plane_center[1],
                                                       sd.valve_plane_center[2]))
            if time_step < self.time_steps - 1:
                f_valve_motion.write('\n')
            else:
                f_valve_motion.close()

            sd.center_MV_position()
            lax, rvax = sd.align_with_SM_ref_system(lax, rvax)
            LAX_extension = sd.lengthLAX()
            SAX_extension = sd.lengthSAX()

            sd.make_tensors()
            sd.show()

            # Initialize some parameters for the SM to match the current geometry
            init_SM_scaling = LAX_extension/abs(self.model()[:,2].detach().cpu().numpy().min())
            self.model.scale = torch.nn.Parameter(torch.tensor([init_SM_scaling], dtype=torch.float32,
                                                     requires_grad=True)).to(device)

            self.model.cp_initial_pos[:,:2] *= 0.5*SAX_extension*1.01

            self.model.cp_initial_pos[:, 2] *= LAX_extension

            # create optimizer:
            params = list(sd.parameters()) + list(self.model.parameters())
            optimizer = torch.optim.Adam(params, lr=1e-2)

            # use more fitting steps on the first frame:
            if time_step == 0:
                iters = 150
            else:
                iters = 100

            if write_log:
                logging.info(f"Time step: {time_step:04}")
            pbar_iter = tqdm(range(iters), desc=f"Optimizing mesh for t={time_step}")

            for i in pbar_iter:
                if previous_mesh_points is not None:
                    loss, status_string = self.compute_loss(smooth_loss_weight, warp_loss_weight,
                                                            mode_loss_weight, sl_offset_loss_weight, sl_rotation_loss_weight, sd, previous_mesh_points= previous_mesh_points.to(device),
                                                            )
                else:
                    loss, status_string = self.compute_loss(smooth_loss_weight, warp_loss_weight,
                                                            mode_loss_weight, sl_offset_loss_weight, sl_rotation_loss_weight, sd, previous_mesh_points=previous_mesh_points,
                                                            )
                pbar_iter.set_postfix_str(status_string)
                optimizer.zero_grad()
                loss.backward()
                optimizer.step()

                # after an initial fitting period, allow slice shift
                # (if we have it on from the start the slices get pulled toward the mesh before it
                # has a chance to get the approximate shape right)
                if i == int(iters * 0.375) and allow_slice_shift:
                    sd.set_transform_learning(True)

                # after 75% of the fitting steps, if "use_warp" is True hen we enable the non-linear
                # fitting (which should now hopefully correct any small errors the linear model
                # can't do)
                if i == int(iters * 0.5) and use_warp:
                    self.model.learn_nonlinear_warp = True

                if write_log:
                    logging.info(f"step={i}: {status_string}")

            MMCoords2World = np.linalg.inv(sd.transformation_world2mesh)
            # World2Img = np.linalg.inv(self.affine_matrix[0])

            if time_step == 0:
                np.save(self.out_path + '/MeshModel2World_affine.npy', MMCoords2World)

            if save_meshes:

                with torch.no_grad():
                    pts = self.model()

                pointsMMCoords = pts.to('cpu').detach().numpy().copy()
                meshWorldCoords = np.dot(MMCoords2World,
                                         np.concatenate((pointsMMCoords,
                                                         np.ones((pointsMMCoords.shape[0], 1))),
                                                        1).T).T

                self.mesh.points = pointsMMCoords.copy()

                if not os.path.exists(self.out_path + '/meshes_SMCoordinates'):
                    os.makedirs(self.out_path + '/meshes_SMCoordinates/')
                self.mesh.write(self.out_path + '/meshes_SMCoordinates/frame_%02d.vtk' % (time_step,))

                self.mesh.points = meshWorldCoords[:, :3].copy()
                if not os.path.exists(self.out_path + '/meshes_WorldCoordinates'):
                    os.makedirs(self.out_path + '/meshes_WorldCoordinates/')
                self.mesh.write(
                    self.out_path + '/meshes_WorldCoordinates/frame_%02d.vtk' % (time_step,))

                # meshPixCoords = np.dot(World2Img, meshWorldCoords.T).T
                # self.mesh.points = meshPixCoords[:, :3].copy()
                # if not os.path.exists(self.out_path + '/meshes_ImgCoordinates'):
                #     os.makedirs(self.out_path + '/meshes_ImgCoordinates/')
                # self.mesh.write(self.out_path + '/meshes_ImgCoordinates/frame_%02d.vtk' % (time_step,))

                self.mesh.points = pointsMMCoords

                # vti_out = self.out_path + '/sax_shifted_VTI/'
                # if not os.path.exists(vti_out):
                #     os.makedirs(vti_out)
                #
                # for id,serieS in enumerate(self.cmr_exam.series):
                #
                #     if serieS['view'].lower() == 'sax':
                #         offset_warp = sd.offset.to('cpu').detach().numpy()
                #         shifted_img = serieS['img'][time_step,...]
                #         pixelShift_stack = np.zeros((serieS['img'][time_step,...].shape[0],2))
                #
                #         for counterSlice,sliceN in enumerate(sd.slice_ids):
                #             if sliceN-sd.sax_slices_id_offset>0:
                #                 pixelShift = np.dot(World2Img,np.dot(MMCoords2World,
                #                              np.array([offset_warp[counterSlice,0],offset_warp[counterSlice,1],offset_warp[counterSlice,2],1]).reshape(-1,1)))[:,0]\
                #                              - np.dot(World2Img,np.dot(MMCoords2World,np.array([0,0,0,1]).reshape(-1,1)))[:,0]
                #
                #                 TM = np.array([[1,0,pixelShift[0]], [0,1,pixelShift[1]]])
                #                 _, rows, cols = serieS['img'][time_step,...].shape
                #                 shifted_img[sliceN-sd.sax_slices_id_offset,...] = cv.warpAffine(serieS['img'][time_step,-sd.sax_slices_id_offset,...], TM, (cols, rows))
                #                 pixelShift_stack[sliceN-sd.sax_slices_id_offset,:] = [pixelShift[0],pixelShift[1]]
                #             else:
                #                 continue
                #
                #         affine_sax = serieS['affine']
                #         np.save(vti_out + '/Img2World_matrix4x4.npy', affine_sax)
                #         shifted_img = np.transpose(shifted_img, (1, 2, 0))
                #         original_img = np.transpose(serieS['img'][time_step,...],(1, 2, 0))
                #
                #         vti_image = vtk.vtkImageData()
                #         vti_image.SetDimensions(shifted_img.shape)
                #         vti_image.SetSpacing((1.0, 1.0, 1.0))
                #         vti_image.SetOrigin(tuple(affine_sax[:3, 3]))
                #
                #         # This automatically allows to visualize the correct orientation in space,
                #         # but paraview has issues with the slicing.
                #         Img2World_vtk = vtk.vtkMatrix3x3()
                #         for i in range(3):
                #             for j in range(3):
                #                 Img2World_vtk.SetElement(i, j, affine_sax[i, j])
                #         vti_image.SetDirectionMatrix(Img2World_vtk)
                #
                #         linear_array = shifted_img.reshape(-1, 1, order="F")
                #         vtk_array = numpy_to_vtk(linear_array)
                #         vtk_array.SetName('shiftedImage')
                #         vti_image.GetPointData().AddArray(vtk_array)
                #         vti_image.Modified()
                #
                #         linear_array = original_img.reshape(-1, 1, order="F")
                #         vtk_array = numpy_to_vtk(linear_array)
                #         vtk_array.SetName('originalImage')
                #         vti_image.GetPointData().AddArray(vtk_array)
                #         vti_image.Modified()
                #
                #         writer = vtk.vtkXMLImageDataWriter()
                #         writer.SetFileName(vti_out+'/frame_%02d.vti' %(time_step))
                #         writer.SetInputData(vti_image)
                #         writer.Write()
                #         np.save(vti_out+'/frame_%02d_offsets.npy' %(time_step,),pixelShift_stack)

            with torch.no_grad():
                previous_mesh_points = self.model()
                if time_step == 0:
                    mesh_at_timestep_0 = previous_mesh_points

            if save_slicing_results:
                if not os.path.exists(self.out_path + '/sliceimages'):
                    os.makedirs(self.out_path + '/sliceimages')
                imageio.imwrite(
                    self.out_path + '/sliceimages/frame_%02d.png' % (time_step,),
                    Image.fromarray((self.make_comparison_slice_image(self.model, sd, self.mesh)*255).astype(np.uint8)))

            if show_each_frame:
                im, cam_pos = self.vizualise_current_fit(sd, only_mesh=True, camera_pos=cam_pos)

            if save_mesh_images:
                im, cam_pos = self.vizualise_current_fit(sd, return_image=True, only_mesh=False, camera_pos=cam_pos)
                #im = im[:, 150:-150]  # trim the image a bit
                if not os.path.exists(self.out_path + '/meshimages'):
                    os.makedirs(self.out_path + '/meshimages')

                imageio.imwrite(self.out_path + '/meshimages/frame_%02d.png' % (time_step,), im.astype(np.uint8))
            if write_log:
                logging.shutdown()

    def estimate_slice_shifts(self, smooth_loss_weight, warp_loss_weight,
            mode_loss_weight, sl_offset_loss_weight, sl_rotation_loss_weight, save_log: bool = False):
        """

        :param save_log:
        :return:
        """
        slice_offset_lists = {}
        cp, lax, rvax = None, self.stack_orientation, None

        for time_step in range(0, self.time_steps, 4):

            slices,_ = self.get_slices_at_timestep(time_step)
            slice_data = SliceData(slices, self.out_path, contour_res=5,
                                   slice_offset_lists=slice_offset_lists)

            cp = slice_data.center_MV_position(cp)
            lax, rvax = slice_data.align_with_SM_ref_system(lax, rvax)
            slice_data.make_tensors()
            slice_data.learn_rotation = False
            slice_data.learn_offset = False

            # sd.show()

            # create optimizer:

            params = list(slice_data.parameters()) + list(self.model.parameters())
            optimizer = torch.optim.Adam(params, lr=1e-2)

            # use more fitting steps on the first frame:
            if time_step == 0:
                iters = 60  # 120
            else:
                iters = 25  # 50

            for i in range(iters):
                loss, status_string = self.compute_loss(valve_plane_loss_weight, smooth_loss_weight, warp_loss_weight,
                                                            mode_loss_weight, sl_offset_loss_weight, sl_rotation_loss_weight, slice_data)
                optimizer.zero_grad()
                loss.backward()
                optimizer.step()

                if i == int(iters * 0.5):
                    slice_data.set_transform_learning(True)
                    self.model.learn_nonlinear_warp = True

            # self.vizualiseCurrentFit(sd, only_mesh=False)

            for i, slc_id in enumerate(slice_data.slice_ids):
                slice_offset_lists.setdefault(slc_id, []).append(
                    slice_data.offset[i].detach().cpu().numpy())

            if save_log:
                for k in slice_offset_lists:
                    print(k, np.median(np.array(slice_offset_lists[k]), 0), np.std(np.array(slice_offset_lists[k]), 0))

        return slice_offset_lists

    def get_slices_at_timestep(self, time_step, return_view: bool = False) \
            -> Union[List[np.ndarray], Tuple[List[np.ndarray], List[np.ndarray]]]:
        """
        returns segmentations per slice as list
        :param time_step:
        :param return_view:
        :return:
        """
        slices = []
        slice_view = []
        img2world_matrices = []
        images = []
        for i in range(len(self.segmentations)):  # gather slices from across each series ...
            sp = self.segmentations[i][time_step]
            #sc = self.series_coords[i]
            si = self.images[i][time_step]
            slices.append(sp)#np.concatenate([sp, sc], -1))
            images.append(si)
            slice_view.append(self.view_available[i])
            img2world_matrices.append(self.affine_matrix_resampled[i])

        if return_view:
            return slices, images, slice_view, img2world_matrices
        else:
            return slices, images, img2world_matrices

    def compute_loss(self, smooth_loss_weight, warp_loss_weight,
                     mode_loss_weight, sl_offset_loss_weight, sl_rotation_loss_weight, slice_data: SliceData,
                     previous_mesh_points: torch.Tensor = None) -> Tuple[Union[int, Any], str]:
        """ cost function for fitting of the shape model
                2) calculate the (mean point-wise) distance to the mesh in the previous fit  iteration
                    --> cost function contribution: "smoothness_loss"
                3) compute dergee of alignment between anaomical axis (LV long axis and rv axis) of shape model and segemntation/slice data
                    --> cost function contribution: "align_loss"
                4)clculate mean distance between endo-/epi- sufraces of mesh to points on segmentation boundary
                    -->cost function contribution: "mesh_loss"
                5) "warp_loss"
                6) penalize shape model amplitudes "mode_loss"
                7) penalize large slice shifts "slice_offset_loss"
                8) penalize large slice rotations "slice_rotation_loss"
            -->weighted sum == cost function for fitting the shape model to the slice data of the cine segmentation

        :param slice_data:
        :param verbose:
        :param step:
        :param previous_mesh_points:
        :return:
        """
        pts = self.model()
        t_points = slice_data()

        # calculate the (mean point-wise) distance to the mesh in the previous frame
        # this loss helps to smooth mesh movement over time (should reduce unrequired movements):
        smoothness_loss, smoothness_loss_item = 0, 0
        if previous_mesh_points is not None:
            smoothness_loss = torch.mean((pts - previous_mesh_points) ** 2)
            smoothness_loss_item = smoothness_loss.item()*smooth_loss_weight

        # alignment loss (i.e. make the long axis and rv-axis of the model align with those
        # of the slice data)
        # align_loss ranges from -2 to 2, with 2 meaning totally aligned, so because we are
        # minimising we make it negative, and also scale it down to be comparable to the other
        # loss terms:
        rotated_long_axis, rotated_rv_axis = self.model.get_axes()
        align_loss = (torch.dot(rotated_long_axis[0], slice_data.t_long_axis)
                      + torch.dot(rotated_rv_axis[0], slice_data.t_rv_axis))
        align_loss *= -0.01
        # mesh distance loss (i.e. how close are the epi/endo points to the mesh surface):
        epi_err  = self.point_to_epi_distance(pts,t_points[0])#, t_points[0])
        endo_err = self.point_to_endo_distance(pts,t_points[1])#, t_points[1])
        mesh_loss = torch.mean(epi_err) + torch.mean(endo_err)

        warp_loss = torch.mean(self.model.cp_offsets ** 2) * warp_loss_weight
        mode_loss = torch.mean(self.model.amps ** 2) * mode_loss_weight

        slice_offset_loss = torch.mean(slice_data.offset ** 2) * sl_offset_loss_weight
        slice_rotation_loss = torch.mean(slice_data.rotation ** 2) * sl_rotation_loss_weight

        status_string = (f"mesh={float(mesh_loss.item()):1.4f},"
                         f" warp={float(warp_loss.item()):1.7f}, "
                         f"mode={float(mode_loss.item()):1.7f},"
                         f" align={float(align_loss.item()):1.4f}, "
                         f"slice_offset={float(slice_offset_loss.item()):1.4f}, "
                         f"slice_rotation={float(slice_rotation_loss.item()):1.4f},"
                         f" smoothness={float(smoothness_loss_item):1.4f}")

        return (mesh_loss + align_loss + warp_loss + mode_loss + slice_offset_loss +
                slice_rotation_loss + smoothness_loss), status_string

    def vizualise_current_fit(self, sd, return_image=True, only_mesh=True, show_cell_volume=False,
                              camera_pos=None):
        '''
        visualizes the mesh resulting form the current fit iteration
        if only_mesh==False: visulaize segmentation slice data by epi and endo point clouds
        '''
        with torch.no_grad():
            pts = self.model()
            t_points = sd()

        np_pts = pts.to('cpu').detach().numpy().copy()
        self.mesh.points = np_pts

        # make some point clouds for visulaising the slice data:
        epi_point_cloud = pv.PolyData(np.concatenate(t_points[0].detach().cpu().numpy()))
        endo_point_cloud = pv.PolyData(np.concatenate(t_points[1].detach().cpu().numpy()))

        if return_image:
            plotter = pv.Plotter(off_screen=True)
        else:
            plotter = pv.Plotter()

        if camera_pos is not None:
            plotter.camera = camera_pos

        # if not only_mesh:
        #
        #     # display the ground truth target epi and endo points, coloured based on current
        #     # distance to the mesh:
        #     with torch.no_grad():
        #         err = self.point_to_epi_distance(pts, t_points[
        #             0]).cpu().numpy() * 64  # should give error in mm
        #         epi_point_cloud['point_color'] = err
        #         err = self.point_to_endo_distance(pts, t_points[
        #             1]).cpu().numpy() * 64  # should give error in mm
        #         endo_point_cloud['point_color'] = err
        #         origins, normals = sd.getOriginsAndNormals()
        #
        plotter.add_mesh(epi_point_cloud,  color='blue')
        plotter.add_mesh(endo_point_cloud,  color='red')
        #
        #     mesh_vp_points = pts.detach().cpu().numpy()[self.valve_pts]
        #     mesh_vp_points = pv.PolyData(mesh_vp_points)
        #     if sd.enough_valve_plane_points:
        #         mesh_vp_points['point_color'] = valve_plane_loss
        #         plotter.add_mesh(mesh_vp_points, scalars='point_color', cmap='jet')
        #     else:
        #         plotter.add_mesh(mesh_vp_points, color='green')
        #
        #     # show original slice position (i.e. not applying the predicted slice shifts):
        #     original_epi_point_cloud = pv.PolyData(np.concatenate(sd.epi_points))
        #     original_endo_point_cloud = pv.PolyData(np.concatenate(sd.endo_points))
        #     plotter.add_mesh(original_epi_point_cloud, color='white')
        #     plotter.add_mesh(original_endo_point_cloud, color='white')
        #
        #     '''
        #     for i in range(sd.n):
        #         xyz = coords[i].cpu().numpy()
        #         X,Y,Z = xyz[...,0], xyz[...,1], xyz[...,2]
        #         grid = pv.StructuredGrid(X,Y,Z)
        #         plotter.add_mesh(grid, scalars=sd.images[i][...,1].T, opacity=[0,1])
        #     '''
        #
        #     # display mesh slices at the slice locations:
        #     for i in range(sd.n_slices):
        #         # single_slice = pv.wrap(mesh).slice(origin=sd.origins[i], normal=sd.normals[i])
        #         single_slice = pv.wrap(self.mesh).slice(origin=origins[i].cpu().numpy(),
        #                                                 normal=normals[i].cpu().numpy())
        #         if single_slice.n_cells != 0:
        #             plotter.add_mesh(single_slice, color='grey')
        #
        #     # display the axies of the slice data:
        #     if sd.t_long_axis is not None:
        #         lax = sd.t_long_axis.cpu().numpy()
        #         plotter.add_mesh(pv.Line(pointa=sd.bp_com, pointb=lax + sd.bp_com), line_width=10,
        #                          color='orange')
        #     if sd.t_rv_axis is not None:
        #         rvax = sd.t_rv_axis.cpu().numpy()
        #         plotter.add_mesh(pv.Line(pointa=sd.bp_com, pointb=rvax + sd.bp_com), line_width=10,
        #                          color='red')
        #
        #     # display the axies of the mesh model:
        #     with torch.no_grad():
        #         model_lax, model_rvax = self.model.get_axes()
        #         model_lax = model_lax.cpu().numpy()
        #         model_rvax = model_rvax.cpu().numpy()
        #         mesh_center = np.mean(self.mesh.points, axis=0)
        #         plotter.add_mesh(pv.Line(pointa=mesh_center, pointb=model_lax + mesh_center),
        #                          line_width=10, color='yellow')
        #         plotter.add_mesh(pv.Line(pointa=mesh_center, pointb=model_rvax + mesh_center),
        #                          line_width=10, color='pink')
        #
        # display the full mesh model adjacent to the data:
        #self.mesh.points += np.array([[2.5, 0, 0]])
        slices = pv.wrap(self.mesh)  # .slice_orthogonal()
        if show_cell_volume:
            slices['cellvol'] = slices.compute_cell_sizes()['Volume']
        slices = slices.clip(normal='y')
        if show_cell_volume:
            plotter.add_mesh(slices, scalars='cellvol', cmap='jet')
        else:
            plotter.add_mesh(slices, color='pink')  # , opacity=0.5)
        self.mesh.points -= np.array([[2.5, 0, 0]])
        #
        # if not only_mesh:

        # show the control points for the warp, along with their learned offsets
        # (if some offset has been learned):
        if np.mean(self.model.cp_offsets.detach().numpy() ** 2) > 0:
            for i in range(self.model.cp_initial_pos.shape[1]):
                plotter.add_mesh(pv.Sphere(0.02, self.model.cp_initial_pos[0, i]))
                plotter.add_mesh(pv.Line(pointa=self.model.cp_initial_pos[0, i],
                                         pointb=self.model.cp_initial_pos[0, i] +
                                                self.model.cp_offsets[0, i].detach().numpy()),
                                 line_width=3, color='orange')

        if return_image:
            im = plotter.screenshot()
            plotter.close()
            return np.array(im), plotter.camera.copy()

        else:
            plotter.show()

        return plotter.camera.copy()


    def visualize_lge_seg(self,seg_predictions,series_data,seg_predictions_cine,series_data_cine,dir,timestep):
        '''
        plots an overlay image of the lge segmentation and cine segmentation
        '''
        import os
        if not os.path.exists(dir+'/ovelay_segmentations_shape/'):
            os.makedirs(dir+'/ovelay_segmentations_shape/')
        # seg_predictions_cine=np.transpose(seg_predictions_cine,(0,2,3,1))
        series_data_cine=series_data_cine[timestep,:,:,:]
        print('visualize')
        print(seg_predictions.shape)
        print(series_data.shape)
        print(seg_predictions_cine.shape)
        print(series_data_cine.shape)
        import matplotlib.pyplot as plt
        for slice in range(seg_predictions.shape[2]):
            fig, axs = plt.subplots(3,3)
            # axs[0,0].imshow(series_data[slice,0,:,:], 'gray')
            # axs[0,1].imshow(seg_predictions[slice,:,:,0],'jet')
            # axs[0,2].imshow(seg_predictions[slice,:,:,1],'jet')
            # axs[0,3].imshow(seg_predictions[slice,:,:,2],'jet')
            #lge data
            axs[0,0].imshow(series_data[slice,0,:,:], 'gray')
            axs[0,1].imshow(series_data[slice,0,:,:], 'gray')
            axs[0,2].imshow(series_data[slice,0,:,:], 'gray')

            # axs[0,3].imshow(series_data[slice,0,:,:], 'gray')
            # axs[1,1].imshow(seg_predictions[slice,:,:,0],'jet',  alpha=0.5*(seg_predictions[slice,:,:,0]>0.25))
            #cine segmentation
            axs[0,0].imshow(seg_predictions_cine[:,:,slice,1], 'jet',  alpha=0.5*(seg_predictions_cine[:,:,slice,1]>0.25))
            #lge segmentation
            axs[0,1].imshow(seg_predictions[slice,:,:,1],'jet',  alpha=0.5*(seg_predictions[slice,:,:,1]>0.25))
            axs[0,2].imshow(seg_predictions[slice,:,:,2],'jet',  alpha=0.5*(seg_predictions[slice,:,:,2]>0.25))

            #cine image data
            axs[1,0].imshow(series_data_cine[slice,:,:], 'gray')
            axs[1,1].imshow(series_data_cine[slice,:,:], 'gray')
            axs[1,2].imshow(series_data_cine[slice,:,:], 'gray')
            #cine segmentation
            axs[1,0].imshow(seg_predictions_cine[:,:,slice,1], 'jet',  alpha=0.5*(seg_predictions_cine[:,:,slice,1]>0.25))
            #lge segmentation
            axs[1,1].imshow(seg_predictions[slice,:,:,1],'jet',  alpha=0.5*(seg_predictions[slice,:,:,1]>0.25))
            axs[1,2].imshow(seg_predictions[slice,:,:,2],'jet',  alpha=0.5*(seg_predictions[slice,:,:,2]>0.25))
            # axs[2,3].imshow(series_data_cine[slice,:,:], 'gray')
            # axs[2,1].imshow(seg_predictions[slice,:,:,0],'jet',  alpha=0.5*(seg_predictions[slice,:,:,0]>0.25))
            # axs[2,2].imshow(seg_predictions[slice,:,:,1],'jet',  alpha=0.5*(seg_predictions[slice,:,:,1]>0.25))
            # axs[2,3].imshow(seg_predictions[slice,:,:,2],'jet',  alpha=0.5*(seg_predictions[slice,:,:,2]>0.25))
            #
            # axs[3,0].imshow(series_data_cine[slice,:,:], 'gray')
            # axs[3,0].imshow(seg_predictions_cine[slice,:,:,1], 'jet',  alpha=0.5*(seg_predictions_cine[slice,:,:,1]>0.25))
            # axs[3,1].imshow(series_data[slice,0,:,:], 'gray')
            # axs[3,1].imshow(seg_predictions_cine[slice,:,:,1], 'jet',  alpha=0.5*(seg_predictions_cine[slice,:,:,1]>0.25))
            #lge data
            axs[2,0].imshow(series_data[slice,0,:,:], 'gray')
            #cine segmentation
            axs[2,1].imshow(seg_predictions_cine[slice,:,:,1], 'gray')
            #lge segmentation
            axs[2,1].imshow(seg_predictions[slice,:,:,1],'jet',  alpha=0.5*(seg_predictions[slice,:,:,1]>0.25))
            axs[2,2].imshow(seg_predictions[slice,:,:,1], 'gray')
            #cine segmentation
            axs[2,2].imshow(seg_predictions_cine[slice,:,:,1], 'jet',  alpha=0.5*(seg_predictions_cine[slice,:,:,1]>0.25))
            # plt.show()
            plt.savefig(dir+'/ovelay_segmentations/seg_slice_'+str(slice)+'.png')

        return

    def shift_lge_seg(self,seg,seg_lge,shifts_lgetocine):
        '''
        minly for visualization of the shift result (currently not used)
        shifts the lge segmentation within the slice plane (input seg_lge) by a shift value pair for each slice given in shifts_lgetocine
        compensated the inplane slice missmatch between cine and lge data/segmentations
        visualizes the segmentations (cine and lge) befor and after the shift in an overlay plot
        '''
        import numpy as np
        import matplotlib.pyplot as plt
        import scipy as sp
        print('shift')
        print(seg_lge.shape)
        print(seg.shape)
        seg_lge=np.transpose(seg_lge,(2,0,1,3))
        seg=np.transpose(seg,(2,0,1,3))
        for slice in range(seg_lge.shape[0]):
            # print(np.round(shifts_lgetocine[slice][0]))
            # print(np.round(shifts_lgetocine[slice][1]))
            # lge_seg_shifted=np.roll(seg_lge[slice,:,:,:],-1*int(np.round(shifts_lgetocine[slice][0])),axis=0)
            # lge_seg_shifted=np.roll(lge_seg_shifted,-1*int(np.round(shifts_lgetocine[slice][1])),axis=1)
            lge_seg_shifted=sp.ndimage.shift(np.sum(seg_lge[slice,:,:,:],axis=-1),-1*shifts_lgetocine[slice])
            print(lge_seg_shifted.shape)
            fig, axs = plt.subplots(1,2)
            axs[0].imshow(seg[slice,:,:,1], 'gray')
            axs[0].imshow(np.sum(seg_lge[slice,:,:,:],axis=-1),'jet',  alpha=0.5*(np.sum(seg_lge[slice,:,:,:],axis=-1)>0.25))
            axs[1].imshow(seg[slice,:,:,1], 'gray')
            axs[1].imshow(lge_seg_shifted[:,:],'jet',  alpha=0.5*(lge_seg_shifted[:,:]>0.25))
            plt.show() ## TODO: change to plt.savefig if no interactive use enabled

        return lge_seg_shifted

    @staticmethod
    def make_triangular_surface_mesh(to_include: str = 'all') -> Tuple[np.ndarray, np.ndarray]:
        """This loads the mesh model (which is a volumetric terahedral mesh) and converts it to a
        triangular surface mesh, by using the known boundary nodes

        the points in the mesh are each labelled with a class from 0 to 5 indicating their position:
            0 = inner points (not on boundaries)
            1 = epicardium label
            2 = endocardium label
            3 = endo_valve_label
            4 = epi_valve_label
            5 = inner_valve_label

        :param to_include: can be used to get only a particular part of the surface
                (e.g. just the points on the endocardium)
        :return:
        """

        assert to_include in ['all', 'epi', 'endo', 'valve']

        mesh = meshio.read(f"{_SHAPE_MODEL_FOLDER}/Mean/LV_mean.vtk")

        epi_pts = np.squeeze(np.load(f"{_SHAPE_MODEL_FOLDER}/Boundary_nodes/EPI_points.npy"))
        endo_pts = np.squeeze(np.load(f"{_SHAPE_MODEL_FOLDER}/Boundary_nodes/ENDO_points.npy"))
        valve_pts = np.squeeze(np.load(f"{_SHAPE_MODEL_FOLDER}/Boundary_nodes/VALVE_points.npy"))

        endo_valve_indicies = valve_pts[np.where(mesh.point_data['labels'][valve_pts] == 3)[0]]
        epi_valve_indicies = valve_pts[np.where(mesh.point_data['labels'][valve_pts] == 4)[0]]

        full_endo_indecies = np.concatenate([endo_valve_indicies, endo_pts])
        full_epi_indecies = np.concatenate([epi_valve_indicies, epi_pts])

        if to_include == 'all':
            parts = [full_epi_indecies, full_endo_indecies, valve_pts]
        elif to_include == 'epi':
            parts = [full_epi_indecies, ]
        elif to_include == 'endo':
            parts = [full_endo_indecies, ]
        elif to_include == 'valve':
            parts = [valve_pts, ]

        faces = []
        for r in mesh.cells_dict['tetra']:
            for p in parts:
                keep = []
                for index in r:
                    if index in p:
                        keep.append(index)
                if len(keep) == 3:
                    faces.append(keep)

        vertices = mesh.points
        faces = np.array(faces)

        # note, if we want to make a pyvista mesh then we need to first change the faces array
        # like this:
        #	faces = np.concatenate([np.ones((faces.shape[0],1))*3, faces], axis=1)
        # because pyvista expects each face row to start with the number of vertecies in the face
        # (i.e. 3 for triangular faces) we can then visualize the surface mesh like this:
        #	surf = pv.PolyData(vertices, faces.astype('int'))
        #	surf.plot()
        # which is useful for checking things are working as expected

        return vertices , faces

    @staticmethod
    def make_comparison_slice_image(model, sd, mesh, add_labels=True):
        """
        makes an image comparing the myocardium masks in the input data to the myocardium masks resulting from slicing the mesh
        if add_labels == True then also we compute the dice, and number of over/under estimated pixels and adds those values to the image

        in the resulting image:
         - the first row shows the taget masks (i.e the results from the segmentation network) in blue
         - the second row shows the masks resulting from slicing the mesh, in red
         - the third row combines the two images, showing the areas of overlap in white
        """
        red = np.array([[[1., 0, 0]]])
        blue = np.array([[[0.1, 0.55, 0.88]]])
        white = np.array([[[1., 1., 1.]]])

        pred = np.array(MeshModel.replicate_slicing(model, sd))
        targ = np.array(sd.images)[..., 1]
        intersect = pred * targ

        resulting_slices = np.concatenate(pred, 1)
        target_slices = np.concatenate(targ, 1)

        intersection = (resulting_slices * target_slices)[..., None]

        ts = target_slices[..., None] * blue
        rs = resulting_slices[..., None] * blue#red

        comparison = (ts + rs) * (1 - intersection) + intersection * white

        img = (np.concatenate([ts, rs, comparison]) * 255).astype('uint8')

        if add_labels:
            dice_scores = 2 * np.sum(pred * targ, (1, 2)) / np.sum(pred + targ, (1, 2))
            excess_red = np.sum(pred, (1, 2)) - np.sum(intersect, (1, 2))
            excess_blue = np.sum(targ, (1, 2)) - np.sum(intersect, (1, 2))
            total_whites = np.sum(intersect, (1, 2))

            dice_scores = ["%.2f" % (ds,) for ds in dice_scores]
            excess_red = ["%d" % (ds,) for ds in excess_red]
            excess_blue = ["%d" % (ds,) for ds in excess_blue]
            total_whites = ["%d" % (ds,) for ds in total_whites]

            img = np.pad(img, ((0, 180), (0, 0), (0, 0)))
            img = cmrpro.meshfitter.utils.addTextLabels(img, 80, img.shape[0] - 155,
                                                  sd.images[0].shape[1], 0, texts=dice_scores,
                                                  font_size=30, font_color='white')
            img = cmrpro.meshfitter.utils.addTextLabels(img, 80, img.shape[0] - 120,
                                                  sd.images[0].shape[1], 0, texts=total_whites,
                                                  font_size=30, font_color='white')
            img = cmrpro.meshfitter.utils.addTextLabels(img, 80, img.shape[0] - 85, sd.images[0].shape[1],
                                                  0, texts=excess_red, font_size=30,
                                                  font_color='red')
            img = cmrpro.meshfitter.utils.addTextLabels(img, 80, img.shape[0] - 50, sd.images[0].shape[1],
                                                  0, texts=excess_blue, font_size=30,
                                                  font_color=(26, 134, 224))

        return img

    @staticmethod
    def replicate_slicing(model: LearnableMesh, sd: SliceData):
        """extracts voxelized slices from the mesh at the (corrected) slice locations of the data"""

        with torch.no_grad():
            coords = sd.getImageCoords()
            coords = [c.cpu().numpy() for c in coords]
            np_pts = model().to('cpu').detach().numpy().copy()

        _, faces = MeshModel.make_triangular_surface_mesh()
        vertices = np_pts
        faces = np.concatenate([np.ones((faces.shape[0], 1)) * 3, faces], axis=1).astype('int')
        mesh_model = pv.PolyData(vertices, faces)
        resulting_slices = []
        for i in range(len(coords)):
            points_poly = pv.PolyData(coords[i].reshape((-1, 3)))
            select = points_poly.select_enclosed_points(mesh_model)
            mask = select['SelectedPoints'].reshape((192, 192),order='F')
            resulting_slices.append(mask)

        return resulting_slices
