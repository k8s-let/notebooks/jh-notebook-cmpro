__all__ = ["LearnableMesh"]

from typing import Tuple
import os

import numpy as np
import torch
import meshio

from cmrpro.meshfitter._interpolate_spline import interpolate_spline
import cmrpro.meshfitter.utils

_MAX_MODES = 75
_UNIT_SCALE = 1000

torch.set_default_dtype(torch.float32)
device = 'cpu'#torch.device('cuda' if torch.cuda.is_available() else 'cpu')


class LearnableMesh(torch.nn.Module):
    """
    adjustable mesh based on shape model
    """
    #: Determines if the model should learn the POD modes; defaults to False
    learn_modes: bool
    #: Determines if the mesh model can be shifted/rotated to better match the data;
    #: defaults to True
    learn_linear_transform: bool
    #: Determine if  a global scaling of the mesh is allowed (probably not needed but
    # eases the fitting) defaults to True
    learn_global_scale: bool
    #: Determines if non-linear adjustments to the mesh are allowed. Should generally only set
    #: to True after already getting a good initial fit. defaults to False
    learn_nonlinear_warp: bool
    #: rotation params, shape (3, )
    rotation: torch.nn.Parameter
    #: offset params, shape (3, )
    offsets: torch.nn.Parameter
    #: a global scaling param (just 1 number)
    scale: torch.nn.Parameter
    #: these are the learnable modes
    amps: torch.nn.Parameter
    #: params, shape (1, k**3, 3)
    cp_offsets: torch.Tensor
    #:
    cp_initial_pos: torch.nn.Parameter
    #: Zeros and ones determining which modes are used
    mode_mask: torch.Tensor
    #:
    offset_mask: torch.Tensor
    #:
    long_axis: torch.Tensor
    #:
    rv_axis: torch.Tensor
    #:
    tPHI: torch.Tensor
    #:
    t_mode_scales: torch.Tensor
    #:
    t_mean_modes: torch.Tensor

    def __init__(self, num_modes_to_use: int = 10, k: int = 6):
        super().__init__()

        self.learn_modes = False
        self.learn_linear_transform = True
        self.learn_global_scale = True
        self.learn_nonlinear_warp = False

        # PCA mesh model details:
        PHI, mode_scales, mean_modes, num_modes_to_use = load_reference_mesh_model(num_modes_to_use)

        self.tPHI = torch.tensor(PHI, dtype=torch.float32).to(device)
        self.t_mode_scales = torch.tensor(mode_scales, dtype=torch.float32).to(device)
        self.t_mean_modes = torch.tensor(mean_modes,  dtype=torch.float32).to(device)

        # Instantiate learnable mesh parameters:
        # These are optimized during the fitting process
        self.rotation = torch.nn.Parameter(torch.tensor([0, 0, 0], dtype=torch.float32,
                                                        requires_grad=True)).to(device)
        self.offsets = torch.nn.Parameter(torch.tensor([0, 0, 0], dtype=torch.float32,
                                                       requires_grad=True)).to(device)
        self.scale = torch.nn.Parameter(torch.tensor(np.ones((1,)), dtype=torch.float32,
                                                     requires_grad=True)).to(device)
        self.amps = torch.nn.Parameter(torch.tensor(np.zeros((num_modes_to_use,)), dtype=torch.float32,
                                                    requires_grad=True)).to(device)

        # For warp:
        # k  <-- this is quite a key param for controlling the power of the warp,
        # empirically 6 seemed to work well. It is the number of control points to use along each
        # dimension of the control point gris, e.g. k=3 gives a 3x3x3 grid of control points
        # (i.e. 27 control points in total)

        self.cp_offsets = torch.nn.Parameter(torch.tensor(np.zeros((1, k ** 3, 3)),
                                                          dtype=torch.float32, requires_grad=True)).to(device)

        initial_pos = (np.mgrid[:k, :k, :k].reshape((3, k ** 3)).T / (k - 1)) * 2 - 1
        initial_pos[:,2] = (initial_pos[:,2]-1)/2.0

        self.cp_initial_pos = torch.tensor(initial_pos[None], dtype=torch.float32).to(device)

        # # maks to enforce only using required number of modes:
        # mode_mask = np.array([1 for i in range(num_modes_to_use)] +
        #                      [0 for i in range(_MAX_MODES - num_modes_to_use)])
        #
        # self.mode_mask = torch.tensor(mode_mask, dtype =torch.float32).to(device)

        # we have an option to restrict the warp, but seems to work well unrestricted
        restrict_warp_to_xy_plane = True

        if restrict_warp_to_xy_plane:
            self.offset_mask = torch.tensor([1, 1, 0], dtype=torch.float32)[None, None]
        else:
            self.offset_mask = torch.tensor([1, 1, 1], dtype=torch.float32)[None, None]

        self.offset_mask.to(device)

        self.long_axis = torch.tensor([0, 0, 1], dtype=torch.float32).to(device)
        self.rv_axis = torch.tensor([-2 ** 0.5 / 2, -2 ** 0.5 / 2, 0], dtype=torch.float32).to(device)

    def get_axes(self) -> Tuple[torch.Tensor, torch.Tensor]:
        """get the current anatomical axes of the mesh model (i.e. after applying the learned
        rotation) """
        theta, phi, psi = self.rotation[:, None, None, None]
        rot_mat = cmrpro.meshfitter.utils.rotation_tensor(theta, phi, psi, 1)
        rotated_long_axis = torch.matmul(self.long_axis, rot_mat).to(device)
        rotated_rv_axis = torch.matmul(self.rv_axis, rot_mat).to(device)
        return rotated_long_axis, rotated_rv_axis

    def forward(self) -> torch.Tensor:
        """Evaluates the forward-model to reconstruct the mesh from the current values of
        modes, mode-amplitudes, mode-masks

        :return:
        """
        # get current shape modes:
        #if self.learn_modes:
        amps = self.amps * self.t_mode_scales + self.t_mean_modes
        #else:
        #    amps = self.t_mean_modes

        # convert modes to mesh points using projection matrix:
        pts = torch.t(torch.matmul(self.tPHI, amps).reshape((3, -1))).to(device)
        # shift/scale mesh points to roughly the range [-1,1]:
        pts = pts * _UNIT_SCALE
        pts = pts /64  # HARD coded also in image coordinates

        pts = pts * self.scale[0]

        # linear transform (e.g. rotation and translation of the mesh):
        theta, phi, psi = self.rotation[:, None, None, None]
        rot_mat = cmrpro.meshfitter.utils.rotation_tensor(theta, phi, psi, 1)
        rotated_pts = torch.matmul(pts, rot_mat)
        shifted_rotated_pts = rotated_pts + self.offsets

        # non-linear warp:
        if self.learn_nonlinear_warp:
            shifted_rotated_pts = interpolate_spline(
                                self.cp_initial_pos.to(device),
                                self.cp_initial_pos.to(device) +
                                self.cp_offsets.to(device) * self.offset_mask.to(device),
                                shifted_rotated_pts.to(device), 1)

        return shifted_rotated_pts[0]


def load_reference_mesh_model(max_modes: int) -> Tuple[np.ndarray, np.ndarray, np.ndarray, int]:
    """Loads the reference mesh containing the shape model from the folder in the relative
    path f'{os.path.dirname(__file__)}/Shape_model_ML_v3' """
    smfolder = f"{os.path.dirname(__file__)}/Shape_model_ML_v3"
    mesh = meshio.read(f'{smfolder}/Mean/LV_mean.vtk')
    projection_matrix_path = f'{smfolder}/ML_augmentation/Projection_matrix.dat'

    if os.path.isfile(projection_matrix_path):
        with open(projection_matrix_path, 'rb') as infile:
            import pickle
            _phi = pickle.load(infile)
    else:
        projection_matrix_path = f'{smfolder}/ML_augmentation/Projection_matrix.npy'
        _phi = np.load(projection_matrix_path)

    # get the means and scales for the modes (i.e. the modes aren't necessarily Normal(0, 1),
    # so we need to shift and scale the learned modes)
    max_modes = np.min([max_modes, _phi.shape[1]])

    mean_modes = np.dot(mesh.points.reshape((-1,), order='F'), np.array(_phi[:, :max_modes]))
    mode_bounds = np.load(os.path.join(smfolder, 'boundary_coeffs.npy'))
    mode_scales = (mode_bounds[:, 1] - mode_bounds[:, 0]) / 2

    return _phi[:, :max_modes], mode_scales[:max_modes], mean_modes, max_modes
