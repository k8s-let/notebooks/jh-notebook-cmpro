__all__ = ["SliceData"]

from typing import Sequence, List, Tuple, Iterable, Dict, Optional
from warnings import warn

import torch
import numpy as np
import imageio
import scipy.interpolate
import skimage
from scipy.ndimage.measurements import center_of_mass as scipy_center_of_mass
from scipy.ndimage import binary_dilation as scipy_binary_dilation
from PIL import Image

import pyvista as pv
import os

import cmrpro.meshfitter.utils

import cmrpro.processor.MVLocator

torch.set_default_dtype(torch.float32)
device = 'cpu'# torch.device('cuda' if torch.cuda.is_available() else 'cpu')

class SliceData(torch.nn.Module):
    """
    This SliceData object is for representing the multiple slices coming from an io, inparticular:

        - it extracts epi/endo edge points, and does affine trasformation to with within the [0,-1] cube
        - it performs a global rotation so that the long axis and rv-axis match those of the initial
            mesh model
        - it allows for learning of per-slice offsets and rotations

    Usage:

        - first init with a list of slices, like so:

            sd = SliceData([slc1, slc2, ..., slcn])

            - each slice should be a an array of shape (H,W,6), where:
                - the first three channels are the RGB segmentation masks
                - the last three channels are the XYZ coordinates of the pixels

        - this will try and guess the long axis and rv-axis directions from the data, but you can
         also manually set them

    :param list_of_series: Each of the contained arrays should have the shape (H,W,6), where the
                            (..., :3) are the one-hot encoded labels (LV, RV, blood-pool) and the
                            slice (..., 3:) contains the coordinates
    :param out_dir:
    :param reverse_LV_estimation: if true the attribute `stack_direction` will be flipped
    :param slice_views:
    :param contour_res:
    :param slice_offset_list:
    :param learn_transform:
    """
    #:
    valve_plane_points: Dict[int, np.ndarray]
    #: Should be true if we have found enough groups of points (believed to be) lying on the valve
    # plane in order to robustly estimate a plane in 3D space.
    enough_valve_plane_points: bool
    #: 1 for (base-at-top-apex-at-bottom) and -1 for opposite
    stack_direction: float
    #: Indices of slices used for processing (contains LV and is belove valve point)
    slice_ids: List[int]
    #: Number of used slices, is equal to len(self.slice_ids)
    n_slices: int
    #: List of segmentation-mask images, which corresponds to the result of indexing the input with
    #: self.slice_ids and only using the slice (..., :3). Shapes of each entry is [H, W, 3]
    images: List[np.ndarray]
    #: List of 3D coordinates of each pixel of the corresponding entry in self.images. These are
    #: scaled by a factor of 64 to make sure they are between -1 and 1
    coords: List[np.ndarray]
    #: List of (n, 3) arrays containing the contours of epi-cardium interpolated on the regular grid
    #: of coordinates contained in self.coords
    epi_points: List[np.ndarray]
    #: List of (n, 3) arrays containing the contours of endo-cardium interpolated on the regular
    #: grid of coordinates contained in self.coords
    endo_points: List[np.ndarray]
    #: List of (3, ) arrays containing the center of mass coordinates of  the blood-pool contour
    bp_com: List[np.ndarray]
    #: List of (3, ) arrays containing the center of mass coordinates of the right ventricle contour
    rv_com: List[np.ndarray]
    #: List if (3, ) arrays storing one point on each slice plane (so we know its position in 3D )
    origins: List[np.ndarray]
    #: List if (3, ) arrays storing the normal of each slice plane (so we know its orientation)
    normals: List[np.ndarray]
    #: List if (3, ) arrays storing one in-plane direction of each slice so we know its orientation
    in_plane_axies: List[np.ndarray]
    #: direction of the axis going up the heart's long axis
    long_axis: np.ndarray
    #: direction of the axis going from LV to RV
    rv_axis: np.ndarray
    #: (4, 4) Matrix to move mesh model coordinates into world coordinates and real images
    transformation_world2mesh: np.ndarray
    #: Variables for learning slice offsets of shape (n_slices, 3)
    offset: torch.nn.Parameter
    #: Variables for learning rotations of shape (n_slices, 3)
    rotation: torch.nn.Parameter
    #: 3D coordinate used as reference for the mesh model coordinate system
    center_point: np.ndarray
    #: equivalent to self.epi_points but as torch tensor
    t_epi_points: List[torch.Tensor]
    #: equivalent to self.endo_points but as torch tensor
    t_endo_points: List[torch.Tensor]
    #: equivalent to self.origins but as torch tensor
    t_origins: List[torch.Tensor]
    #: equivalent to self.normals but as torch tensor
    t_normals: List[torch.Tensor]
    #: equivalent to self.in_plane_axies but as torch tensor
    t_in_plane_axies: List[torch.Tensor]
    #: equivalent to self.coords but as torch tensor
    t_coords: List[torch.Tensor]
    #: equivalent to self.long_axis but as torch tensor
    t_long_axis: List[torch.Tensor]
    #: equivalent to self.long_axis but as torch tensor
    t_rv_axis: List[torch.Tensor]

    def __init__(self, list_of_series: Iterable[np.ndarray],stack_orientation,
                 out_dir: str, img2world_matrices, force_valve_estimation, list_of_images,
                 slice_views: Sequence[str] = None, contour_res: int = 5,
                 slice_offset_lists=None, learn_transform=False):
        super(SliceData, self).__init__()

        self.out_dir = out_dir

        n_lax_views = 0
        for count, series in enumerate(list_of_series):
            if 'lax' in slice_views[count].lower():
                n_lax_views += 1

        # Get valve center coordinates and normal direction in xzy coordinates
        if n_lax_views == 0 or force_valve_estimation == 'sax':
            # Estimate valve plane from short axis, take plane above the last one with both LV and RV blood pool
            self.valve_plane_normal, self.valve_plane_center, above_vp = self.estimate_valve_position_from_SAX(
                list_of_series, slice_views)
        else:
            self.valve_plane_normal, self.valve_plane_center, above_vp = self.estimate_valve_position_withMVLocator(list_of_series, list_of_images, img2world_matrices, slice_views,  self.out_dir)

        self.stack_orientation = np.array(stack_orientation).reshape(1,-1)

        no_lv = self.slice_not_containing_LV(list_of_series,slice_views)

        slices_ignore = no_lv + above_vp
        # visualise the slice data, including showing which slices have been
        # excluded (useful for debugging)
        imageio.imwrite(out_dir + '/processed_series.png',
                        Image.fromarray((self.make_image_of_processed_slices(list_of_series, above_vp, no_lv)*255).astype(np.uint8)))

        self.images = []

        # lists for (numpy) contour points and slice position information:
        self.epi_points = []
        self.endo_points = []
#        self.normals = []
#        self.in_plane_axies = []
        self.t_long_axis = None
        self.t_rv_axis = None
        self.bp_com = []
        self.rv_com = []
        self.slice_original_view = []
        self.slice_affine_and_id = [] # stores the affine matrix and the slice index for calculation of 3D coords
        # same as above but pytorch versions:
        self.t_epi_points = []
        self.t_endo_points = []
#        self.t_normals = []
#        self.t_in_plane_axies = []

        # Matrix to move mesh model coordinates into world coordinates and real images
        self.transformation_world2mesh = np.eye(4)
        self.transformation_world2mesh [3, 3] = 1.0

        for counter,ser in enumerate(list_of_series):
            for k, s in enumerate(ser):
                if [counter,k] in slices_ignore:
                    continue
                # Find contours of blood bool (endo) and myocardium for epi
                bp_contours = skimage.measure.find_contours(s[..., 2], 0.5)
                bp_contours = np.concatenate([c[1:] for c in bp_contours])
                myo_contours = skimage.measure.find_contours(s[..., 1], 0.5)
                myo_contours = np.concatenate([c[1:] for c in myo_contours])

                myo_contours = skimage.measure.find_contours(s[..., 1], 0.5)
                myo_contours = np.concatenate([c[1:] for c in myo_contours])
                
                # count points as endo if they touch both myo (green) and LV blood pool (blue),
                # and epi if they touch only myo (green)
                endo, epi = [], []
                for pt in myo_contours.tolist():
                    if pt in bp_contours.tolist():
                        endo.append(pt)
                    elif pt not in epi:
                        epi.append(pt)
                endo = np.array(endo)[::contour_res]
                epi  = np.array(epi)[::contour_res]

                endo_xyz = self.pix_coords_affine(endo,img2world_matrices[counter],k, concat_comp = True)
                epi_xyz  = self.pix_coords_affine(epi, img2world_matrices[counter],k, concat_comp = True)

                # find the center of mass of the LV and RV in 3D space:
                if np.sum(s[..., 2]) > 0:  # LV blood-pool
                    bp_pixel_com = np.array(scipy_center_of_mass(s[..., 2]))
                    self.bp_com.append(self.pix_coords_affine(bp_pixel_com.reshape(1,-1),img2world_matrices[counter],k,concat_comp = True)[0,:])
                if np.sum(s[..., 0]) > 0:  # RV blood-pool
                    rv_pixel_com = np.array(scipy_center_of_mass(s[..., 0]))
                    self.rv_com.append(self.pix_coords_affine(rv_pixel_com.reshape(1,-1),img2world_matrices[counter],k,concat_comp = True)[0,:])

                # save the slice details
                self.images.append(s[..., :3])
                #self.coords.append(s[..., 3:])
                self.epi_points.append(epi_xyz)
                self.endo_points.append(endo_xyz)
                self.slice_affine_and_id.append([img2world_matrices[counter],k])
                # # TODO: CHECK WHERE THIS IS USED
                # # save the position and orientation of the slice:
                # # just any point in the plane (we just take the first point)
                # # self.origins.append(s[0, 0, 3:])
                #
                # ipax1 = s[0, 1, 3:] - s[0, 0, 3:]
                # ipax1 /= np.sum(ipax1 ** 2) ** 0.5
                # ipax2 = s[1, 0, 3:] - s[0, 0, 3:]
                # ipax2 /= np.sum(ipax2 ** 2) ** 0.5
                # ipaxies = np.array([ipax1, ipax2])
                # self.in_plane_axies.append(ipaxies)
                #
                # # normal = np.cross(s[0,1,3:]-s[0,0,3:],s[1,0,3:]-s[0,0,3:])
                # normal = np.cross(ipax1, ipax2)
                # normal /= np.sum(normal ** 2) ** 0.5
                # self.normals.append(normal)  # normal vector to the plane
                self.slice_original_view.append(slice_views[counter])

        self.n_slices = len(self.epi_points)

        # # For debugging
        # plotter = pv.Plotter()
        #
        # # for i in range(self.n_slices):
        # #     xyz = self.coords[i]
        # #     X, Y, Z = xyz[..., 0], xyz[..., 1], xyz[..., 2]
        # #     grid = pv.StructuredGrid(X, Y, Z)
        # #     plotter.add_mesh(grid, scalars=self.images[i][..., 1].T, opacity=[0, 1])
        #
        # # make some point clouds for visulaising the (transformed) slice data:
        # for k, endo_p in enumerate(self.endo_points):
        #     endo_point_cloud = pv.PolyData(endo_p)
        #     plotter.add_mesh(endo_point_cloud, color='red')
        # for k, epi_p in enumerate(self.epi_points):
        #     epi_point_cloud = pv.PolyData(epi_p)
        #     plotter.add_mesh(epi_point_cloud, color='blue')
        #
        # plotter.add_mesh(pv.Sphere(5.0, self.valve_plane_center), color='green')
        # plotter.add_mesh(pv.Line(pointa=self.valve_plane_center, pointb=self.valve_plane_center + self.valve_plane_normal), line_width=10,
        #                  color='red')
        #
        # valve_plane_disc = pv.Disc(center=self.valve_plane_center, inner=0., outer=50., normal=self.valve_plane_normal)
        # plotter.add_mesh(valve_plane_disc, color='green', opacity=0.5)
        # valve_plane_disc = pv.Disc(center=[0, 0, 0], inner=0., outer=50., normal=self.valve_plane_normal)
        # plotter.add_mesh(valve_plane_disc, color='blue', opacity=0.1)
        #
        #
        # if not os.path.exists(self.out_dir + '/slice_data_noTrans/'):
        #     os.makedirs(self.out_dir + '/slice_data_noTrans/')
        #
        # counter_images = len(next(os.walk(self.out_dir + '/slice_data_noTrans/'))[2])
        # plotter.save_graphic(self.out_dir + '/slice_data_noTrans/Frame_%02d.pdf' % (counter_images))
        # plotter.close()

        # the centers of the LV and RV are estimated to be the median of the per-slice centers:
        self.bp_com = np.median(np.array(self.bp_com), 0)
        self.rv_com = np.median(np.array(self.rv_com), 0)

        starting_offset = np.zeros((self.n_slices, 3))
        if slice_offset_lists is not None:
            for i in slice_offset_lists:
                if i < self.n_slices:
                    starting_offset[i] = np.median(np.array(slice_offset_lists[i]), 0)
        starting_rotation = np.zeros((self.n_slices, 3))

        is_sax = np.zeros((self.n_slices, 3))
        for i in range(self.n_slices):
            if 'sax' in self.slice_original_view[i].lower():
                is_sax[i] = 1.0

        # variables for learning slice offsets and rotations:
        # these learnable tensors allow for learing per-slice offests and rotations,
        # which can in theory learn to correct slice shifts in the data

        self.offset = torch.nn.Parameter(torch.tensor(starting_offset,
                                                      dtype=torch.float32, requires_grad=True).to(device))

        self.mask_offset = torch.tensor(is_sax,dtype=torch.float32).to(device)

        self.rotation = torch.nn.Parameter(torch.tensor(starting_rotation,
                                                        dtype=torch.float32, requires_grad=True).to(device))

        # # # This checks if all slices are with proper coordinates and intersects properly
        # plotter = pv.Plotter()
        # for i in range(1):
        # 	grid = pv.StructuredGrid(self.coords[i][...,0],self.coords[i][...,1],
        #                            self.coords[i][...,2])
        # 	plotter.add_mesh(grid, scalars=self.images[i][...,1].flatten(order='F'),
        # 				 opacity=.5, cmap='gray')
        # epi_point_cloud = pv.PolyData(np.concatenate(self.epi_points))
        # plotter.add_mesh(epi_point_cloud)
        # plotter.show()
        #
        # plotter = pv.Plotter()
        # epi_point_cloud = pv.PolyData(np.concatenate(self.epi_points))
        # endo_point_cloud = pv.PolyData(np.concatenate(self.endo_points))
        #
        # plotter.add_mesh(epi_point_cloud)
        # plotter.add_mesh(endo_point_cloud)
        #
        # plotter.show()
        self.set_transform_learning(learn_transform)

    def pix_coords_affine(self, pix_coords_vector, affine_matrix: np.ndarray, slice_number =None,concat_comp=False) \
            -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        """Transforms the integer-image coordinates by using the affine
        transformation.

        :param pixel coordinates of points
        :param affine_matrix:
        :param slice_number: if pixel is 2D vector, slice identifier is required
        :return: X, Y, Z coordinates
        """
        if pix_coords_vector.shape[-1] == 3:
            one_vector = np.ones_like(pix_coords_vector)[:,0].reshape(-1,1)
            coords4d = np.concatenate((pix_coords_vector,one_vector), 1)
        else:
            one_vector = np.ones_like(pix_coords_vector)[:,0].reshape(-1,1)
            coords4d = np.concatenate((pix_coords_vector,slice_number*one_vector,one_vector), 1)

        world_coords = np.dot(affine_matrix, coords4d.T).T

        if concat_comp:
            return world_coords[:,:3]
        else:
            X = world_coords[:, 0]
            Y = world_coords[:, 1]
            Z = world_coords[:, 2]

            return X, Y, Z

    def pix_coords_affine_torch(self, pix_coords_vector, affine_matrix, slice_number, concat_comp=True):
        """Transforms the integer-image coordinates by using the affine
        transformation.

        :param pixel coordinates of points
        :param affine_matrix:
        :param slice_number: if pixel is 2D vector, slice identifier is required
        :return: X, Y, Z coordinates
        """
        check_shape = pix_coords_vector.size()
        if len(check_shape) == 1:
            single_point = True
        else:
            single_point = False

        if single_point:
            #single point
            coords4d = torch.concatenate((pix_coords_vector,torch.tensor([0])))
        else:
            one_vector = torch.ones(1,pix_coords_vector.size()[1])
            coords4d = torch.concatenate((pix_coords_vector.T,slice_number*one_vector,0*one_vector), 1)

        world_coords = torch.matmul(affine_matrix, coords4d.type(torch.float).T).T
        if concat_comp:
            if single_point:
                return world_coords[:3]
            else:
                return world_coords[:,3]
        else:
            if single_point:
                X = world_coords[0]
                Y = world_coords[1]
                Z = world_coords[2]
            else:
                X = world_coords[:, 0]
                Y = world_coords[:, 1]
                Z = world_coords[:, 2]

            return X, Y, Z

    @staticmethod
    def slice_coords_affine(img_size, sliceN, affine_matrix):
        """Transforms the integer-image coordinates of a 2D image slice by using the affine
        transformation to world coordinates."""
        xv, yv, zv = np.meshgrid(np.linspace(0, img_size[0], img_size[0]),
                                 np.linspace(0, img_size[1], img_size[1]), indexing='ij')
        one_vector = np.ones_like(xv)
        PixCoords = np.concatenate((xv.reshape(-1, 1, order='F'), yv.reshape(-1, 1, order='F'),
                                    sliceN * one_vector.reshape(-1, 1, order='F'),
                                    one_vector.reshape(-1, 1, order='F')), 1)
        WorldCoords = np.dot(affine_matrix, PixCoords.T).T

        X = WorldCoords[:, 0].reshape(xv.shape, order='F')
        Y = WorldCoords[:, 1].reshape(xv.shape, order='F')
        Z = WorldCoords[:, 2].reshape(xv.shape, order='F')

        return X, Y, Z

    @staticmethod
    def estimate_valve_position_from_SAX(list_of_series: Iterable[np.ndarray],
                                       slice_views: Sequence[str] = None) -> Dict[int, np.ndarray]:
        '''
        This is the worst case. No information possible from LAX slices, so the valve plane is guessed based on SAX
        This has a potentially large error (comparable to slice thickness), but at least is an estimation and regularizes
        the loss function and the warping at the base.
        It is assumed that the valve plane starts when the LV myocardium is not closed anymore
        '''
        from scipy.ndimage import binary_fill_holes

        above_valve = []
        slice_index = 0
        update_valve_plane = False
        max_slices = 0

        for count, series in enumerate(list_of_series):

            if 'sax' in slice_views[count].lower():
                hasLV  = []
                # The stack with more slices is used as reference

                n_slices_new = len(series)
                if n_slices_new > max_slices:
                    update_valve_plane = True
                    max_slices = n_slices_new

                for k, s in enumerate(series):
                    if np.sum(s[..., 1])>0:
                        filled = 1 * binary_fill_holes(s[..., 1])
                        if np.sum(filled - s[..., 1]) > 0:  # there is a closed blood pool
                            hasLV.append(1)
                        else:
                            hasLV.append(0)
                    else:
                        hasLV.append(0)

                    # Check if there is a closed blood pool or not. If LV mask is not closed, the algorithm returns the input

                valve_plane_slice = np.min([len(hasLV),np.max(np.where(np.array(hasLV)==1)[0])+1])-1

                if update_valve_plane:
                    xc, yc = np.where(series[valve_plane_slice][..., 2] == 1)
                    valve_plane_points = series[valve_plane_slice][xc,yc,3:]
                    try:
                        valve_plane_center = series[valve_plane_slice][int(np.mean(xc)),int(np.mean(yc)),3:]
                    except:
                        valve_plane_slice -= 1
                        xc, yc = np.where(series[valve_plane_slice][..., 2] == 1)
                        valve_plane_points = series[valve_plane_slice][xc, yc, 3:]
                        valve_plane_center = series[valve_plane_slice-1][int(np.mean(xc)), int(np.mean(yc)), 3:]

                    if valve_plane_slice+1 < len(series):
                        norm_vector = series[valve_plane_slice+1][int(np.mean(xc)),int(np.mean(yc)),3:] - valve_plane_center
                        valve_plane_normal = norm_vector/np.linalg.norm(norm_vector)
                    else:
                        norm_vector = -(series[valve_plane_slice-1][int(np.mean(xc)),int(np.mean(yc)),3:] - valve_plane_center)
                        valve_plane_normal = norm_vector / np.linalg.norm(norm_vector)
                    update_valve_plane = False

                for jj in range(valve_plane_slice,len(hasLV)):
                    above_valve.append([count,jj])

            slice_index += series.shape[0]

        return valve_plane_normal, valve_plane_center, above_valve

    #@staticmethod
    def estimate_valve_position_withMVLocator(self,list_of_series: Iterable[np.ndarray],list_of_images: Iterable[np.ndarray],
                                       img2world_matrices, slice_views: Sequence[str] = None, out_dir=None) -> Dict[int, np.ndarray]:
        '''
        Identify valve plane points from long axis views using a network trained on 4ch,2ch,3ch
        '''

        above_valve = []

        # Points in 2D pixel coordinates
        valve_points1 = []
        valve_points2 = []
        valve_center_v = []
        normal_point  = []

        # Points in 3D xyz world coordinates
        valve_points1_xyz = []
        valve_points2_xyz = []
        valve_center_xyz  = []
        normal_point_xyz  = []

        lax_counter = 0
        MVLocator = cmrpro.processor.MVLocator.load_MVLocator()

        # Check if there is 4ch view
        any_lax = True

        for count, series in enumerate(list_of_series):
            if '4ch' in slice_views[count].lower():
                any_lax = False

        for count, series in enumerate(list_of_series):
            if '4ch' not in slice_views[count].lower() and not any_lax:
                continue

            if 'lax' in slice_views[count].lower():
                # Just use mid slice if there are more than 1
                k = len(series)//2
                s = series[k]

                bpCenter = scipy_center_of_mass(s[..., 2]) # I just need a random one

                img = np.expand_dims(np.expand_dims(list_of_images[count][k,...],0),0)

                rec_landmarks = MVLocator(torch.tensor(img).to(device)).cpu().detach().numpy()[0,...]

                # max_val = np.max(rec_landmarks[0, ...])
                # idx1, idy1 = np.where(rec_landmarks[0, ...] == max_val)
                # point1 = [idx1[0], idy1[0]]
                # max_val = np.max(rec_landmarks[1, ...])
                # idx1, idy1 = np.where(rec_landmarks[1, ...] == max_val)
                # point2 = [idx1[0], idy1[0]]
                point1 = np.unravel_index(np.argmax(rec_landmarks[0, ...], axis=None), rec_landmarks[0, ...].shape)
                point2 = np.unravel_index(np.argmax(rec_landmarks[1, ...], axis=None), rec_landmarks[1, ...].shape)

                valve_points1.append(point1)
                valve_points2.append(point2)
                valve_center_ = 0.5*(np.asarray(point1)+np.asarray(point2))

                valve_center_v.append(valve_center_)

                valve_points1_xyz.append(self.pix_coords_affine(np.asarray(point1).reshape(1,-1), img2world_matrices[count], k,
                                                                concat_comp=True))
                valve_points2_xyz.append(self.pix_coords_affine(np.asarray(point2).reshape(1, -1), img2world_matrices[count], k,
                                                           concat_comp=True))

                # x = np.linspace(0, s.shape[0] - 1, s.shape[0])
                # y = np.linspace(0, s.shape[1] - 1, s.shape[1])
                #
                # # takes x,y coords of the image and use those
                # f = scipy.interpolate.RegularGridInterpolator((x, y), s[..., 3:])
                #
                # valve_points1_xyz.append(f(point1))
                # valve_points2_xyz.append(f(point2))

                valve_center_xyz.append(self.pix_coords_affine(valve_center_.reshape(1,-1), img2world_matrices[count], k,True))

                lax_counter += 1

                valve_plane_direction = (point2[1] - point1[1]) / (point2[0] - point1[0])
                valve_plane_normal = 0.2*(valve_center_ - bpCenter) # -1 / valve_plane_direction

                valve_normal_point = valve_center_ + valve_plane_normal#np.array([2,2*valve_plane_normal])

                if np.dot(valve_normal_point - valve_center_, valve_center_ - np.asarray(bpCenter)) < 0:
                     valve_normal_point = valve_center_ - np.array([2,2*valve_plane_normal])

                normal_point.append(valve_normal_point)
                normal_point_xyz.append(self.pix_coords_affine(valve_normal_point.reshape(1, -1), img2world_matrices[count], k,
                                                           concat_comp=True))

        valve_center = valve_center_xyz[0].reshape(1,3)
        valve_normal_point = normal_point_xyz[0].reshape(1,3) - valve_center

        if np.linalg.norm(valve_normal_point) > 0:
            valve_normal_point /= np.linalg.norm(valve_normal_point)
            valve_normal_point = valve_normal_point

        # import matplotlib.pyplot as plt
        # plt.imshow(list_of_images[0][k,...])
        # plt.scatter(point1[0],point1[1],s=13)
        # plt.scatter(point2[0], point2[1], s=13)
        # plt.scatter(valve_center_[0], valve_center_[1], s=13)
        # plt.savefig(out_dir+'/Valve_locator_points.png')
        # plt.close()

        # else:
        #     valve_center = np.array([0.0,0.0,0.0])
        #     for kk in range(lax_counter):
        #         valve_center += np.asarray(valve_center_xyz[kk].reshape(3,))/float(lax_counter)
        #
        #     # for valve normal i just consider 2 views and compute the normal vector between those points
        #     valve_normal_point = np.cross(np.asarray(valve_points2_xyz[0])-np.asarray(valve_points1_xyz[0]),np.asarray(valve_points2_xyz[1])-np.asarray(valve_points1_xyz[1]))
        #     if np.linalg.norm(valve_normal_point) > 0:
        #         valve_normal_point /= np.linalg.norm(valve_normal_point)

        for count, series in enumerate(list_of_series):

            if 'sax' in slice_views[count].lower():
                for k, s in enumerate(series):
                    cdm_worldCoords = self.pix_coords_affine(np.array([96,96]).reshape(1,-1), img2world_matrices[count], k,
                                                             concat_comp=True)
                    if np.inner(valve_normal_point,cdm_worldCoords - valve_center) > 5.0:
                        #above_valve.append(slice_index + k)
                        above_valve.append([count,k])
#            slice_index += series.shape[0]

        return valve_normal_point[0,:], valve_center[0,:], above_valve

    @staticmethod
    def slice_not_containing_LV(list_of_series: Sequence[np.ndarray],slice_views) -> List[int]:
        '''
        retruns a list of slice indices whre the LV segmentation maks is empty
        '''
       # list_of_slices = np.concatenate(list_of_series)
        to_remove = []

        for count, series in enumerate(list_of_series):
            for k, s in enumerate(series):
                if 0 in np.sum(s[..., :3], (0, 1))[1:]:
                    #above_valve.append(slice_index + k)
                    to_remove.append([count,k])

        # for slice_id, slc in enumerate(list_of_slices):
        #     if 0 in np.sum(slc[..., :3], (0, 1))[1:]:
        #         to_remove.append(slice_id)

        # # removing LAX segmentations as they are not very accurate at the moment
        # for count, serie in enumerate(list_of_series):
        #     if 'sax' not in slice_views[count].lower():
        #         for k in range(serie.shape[0]):
        #             to_remove.append([count, k])

        return to_remove

    @staticmethod
    def make_image_of_processed_slices(list_of_series: Iterable[np.ndarray],
                                       above_vp: List[int], no_lv: List[int]) -> np.ndarray:
        """ make a single image of all the slices in the series, label those that are being
        excluded"""

        to_remove = above_vp + no_lv
        for_img = []

        for i, series in enumerate(list_of_series):

            for k, s in enumerate(series):  # here we aim to find the valve plane..
                if [i,k] in to_remove:

                    im = ((series[k, ..., :3] * 0.3 + 0.3) * 255).astype('uint8')

                    if  [i,k] in above_vp:
                        im = cmrpro.meshfitter.utils.addTextLabels(im, 20, 5, 0, 0, ['above VP'],
                                                             font_size=30)
                    if  [i,k] in no_lv:
                        # here "no LV" really means "the slice is missing either myocardium
                        # or LV bloodpool (or both)"
                        im = cmrpro.meshfitter.utils.addTextLabels(im, 20, im.shape[0] - 32, 0, 0,
                                                             ['no LV'], font_size=30)

                    for_img.append(im / 255.)
                else:
                    for_img.append(series[k, ..., :3])
            for_img.append(np.ones((series[0][0].shape[0], 20, 3)))

        return np.concatenate(for_img[:-1], 1)

    @staticmethod
    def remove_slices(list_of_slices: Iterable[np.ndarray], to_remove: List[int]) \
            -> Tuple[List[np.ndarray], List[int]]:
        """slices are removed if they don't contain both LV myo and LV blood-pool"""
        slices, slice_ids = [], []
        for slice_id, slc in enumerate(list_of_slices):
            if slice_id not in to_remove:
                slices.append(slc)
                slice_ids.append(slice_id)
        return slices, slice_ids

    def set_transform_learning(self, learn_transform: bool):
        """toggle the learning of the transform (setting the gradient to None is a "trick"
        that stops the momentum in Adam carrying on altering the offset / rotation values,
        which does happen if the grad is just set to zero)"""
        if not learn_transform:
            self.offset.requires_grad = False
            self.offset.grad = None
            self.rotation.requires_grad = False
            self.rotation.grad = None

        else:
            self.offset.requires_grad = True
            self.offset.grad = torch.zeros(self.offset.shape, dtype=torch.float32).to(device)
            self.rotation.requires_grad = True
            self.rotation.grad = torch.zeros(self.offset.shape, dtype=torch.float32).to(device)


    def center_MV_position(self) -> np.ndarray:
        """Calculates the center point (or uses the one provided) and then centers the
        data on that point.

        Sets the attribute self.center_point

        :param center_point: defines a 3D coordinate (3, )
        :return: The newly set center-point
        """
        self.center_point = self.valve_plane_center

        T1 = np.eye(4) # Translation so valve is at 0.0.0
        T1[:3, 3] = -self.center_point
        self.transformation_world2mesh = np.dot(T1, self.transformation_world2mesh)

        T2 = np.eye(4)/64 # Scaling to be in range 0-1 with coordinates
        T2[3, 3] = 1.0
        self.transformation_world2mesh = np.dot(T2, self.transformation_world2mesh)

        self.bp_com = (self.bp_com - self.center_point)/64
        self.rv_com = (self.rv_com - self.center_point)/64

        for i in range(self.n_slices):
            self.epi_points[i] = (self.epi_points[i] - self.center_point)/64
            self.endo_points[i] = (self.endo_points[i] - self.center_point)/64

        #self.valve_plane_center = (self.valve_plane_center -self.center_point)/64

        return 0

    def align_with_SM_ref_system(self, original_long_axis: np.ndarray = None,
                            original_rv_axis: np.ndarray = None) -> Tuple[np.ndarray, np.ndarray]:
        """Calculates the anatomical axies (or use those provided) then rotates all the data to a
        standard orientation (which matches the mesh models orientation)

        :param original_long_axis: (3, )
        :param original_rv_axis: (3, )
        """

        # we now try and estimate the anatomical axies from the slice data:

        # if original_long_axis is None:
        #     # 1. the long axis (pointing in the direction of apex to base):
        #     # as a heuristic, most slices are SAX and so the most frequent plane normal should be the long axis:
        #
        #     normal_strings = [str(n) for n in self.normals]
        #     val, counts = np.unique(normal_strings, return_counts=True)
        #     norm_st = val[np.argmax(counts)]
        #
        #     m = self.normals[normal_strings.index(norm_st)]
        #     # m = stats.mode( np.array(self.normals) )[0][0]
        #     original_long_axis = m
        #     # make it a unit vector:
        #     original_long_axis /= np.sum(original_long_axis ** 2) ** 0.5
        #
        #     # self.stack_direction should be 1 or -1 depending on whether the SAX stack are in the order base to apex (=1) or apex to base (=-1)
        #     # thus we flip the axis here to make sure it points in the apex to base direction regardless of SAX slice order
        #     original_long_axis = original_long_axis * self.stack_direction

        self.long_axis = original_long_axis

        if original_rv_axis is None:
            # 2. the axis from LV to RV (which we call the "original_rv_axis"):
            # this should point from the LV center of mass to the RV center of mass:
            original_rv_axis = self.rv_com - self.bp_com
            # make it a unit vector:
            original_rv_axis /= np.sum(original_rv_axis ** 2) ** 0.5
            # make it perpendicular to the long axis:
            original_rv_axis = original_rv_axis - np.dot(np.array(original_long_axis),
                                                         np.array(original_rv_axis) )* np.array(original_long_axis)

        self.rv_axis = original_rv_axis

        # at this point long_axis and rv_axis define the anatomy-relative co-ortinate system of
        # the slice data we now want to rotate all of the slice data together globally such that it
        # is broadly aligned with the starting shape mesh

        # first we rotate to align the slice data long axis with [0,0,1], which is the long axis
        # direction for the mesh model:
        rot_mat = cmrpro.meshfitter.utils.rotation_matrix_from_vectors(self.long_axis, np.array([0, 0, 1]))

        # Keeps track of transformations made to point coordinates
        ROT1 = np.eye(4)
        ROT1[:3, :3] = rot_mat
        self.transformation_world2mesh = np.dot(ROT1, self.transformation_world2mesh)

        #
        self.long_axis = np.dot(rot_mat, self.long_axis)
        self.rv_axis = np.dot(rot_mat, self.rv_axis)
        self.bp_com = np.dot(rot_mat, self.bp_com)
        self.rv_com = np.dot(rot_mat, self.rv_com)
        #
        for i in range(self.n_slices):
            self.epi_points[i] = np.dot(self.epi_points[i], rot_mat.T)
            self.endo_points[i] = np.dot(self.endo_points[i], rot_mat.T)
            # self.origins[i] = np.dot(rot_mat, self.origins[i])

            # h, w = self.coords[i].shape[:2]
            # self.coords[i] = np.moveaxis(self.coords[i], -1, 0).reshape((3, -1))
            # self.coords[i] = np.dot(rot_mat, self.coords[i])
            # self.coords[i] = np.moveaxis(self.coords[i].reshape((3, h, w)), 0, -1)

            # self.normals[i] = np.dot(rot_mat, self.normals[i])
            # self.in_plane_axies[i][0] = np.dot(rot_mat, self.in_plane_axies[i][0])
            # self.in_plane_axies[i][1] = np.dot(rot_mat, self.in_plane_axies[i][1])

        # next we rotate to align the slice data rv-axis with [-2**0.5/2, -2**0.5/2, 0],
        # which is the rv-axis direction for the mesh model:
        rot_mat = cmrpro.meshfitter.utils.rotation_matrix_from_vectors(self.rv_axis,
                                               np.array([-2 ** 0.5 / 2, -2 ** 0.5 / 2, 0]))

        # Keeps track of transformations made to point coordinates
        ROT2 = np.eye(4)
        ROT2[:3, :3] = rot_mat
        self.transformation_world2mesh = np.dot(ROT2, self.transformation_world2mesh)

        self.long_axis = np.dot(rot_mat, self.long_axis)
        self.rv_axis = np.dot(rot_mat, self.rv_axis)
        self.bp_com = np.dot(rot_mat, self.bp_com)
        self.rv_com = np.dot(rot_mat, self.rv_com)

        # for key in self.valve_plane_points:
        #     self.valve_plane_points[key] = np.moveaxis(
        #         np.dot(rot_mat, np.moveaxis(self.valve_plane_points[key], 0, 1)), 1, 0)

        for i in range(self.n_slices):
            self.epi_points[i]  = np.dot(self.epi_points[i], rot_mat.T)
            self.endo_points[i] = np.dot(self.endo_points[i], rot_mat.T)
            # self.origins[i] = np.dot(rot_mat, self.origins[i])

            # h, w = self.coords[i].shape[:2]
            # self.coords[i] = np.moveaxis(self.coords[i], -1, 0).reshape((3, -1))
            # self.coords[i] = np.dot(rot_mat, self.coords[i])
            # self.coords[i] = np.moveaxis(self.coords[i].reshape((3, h, w)), 0, -1)

            # self.normals[i] = np.dot(rot_mat, self.normals[i])
            # self.in_plane_axies[i][0] = np.dot(rot_mat, self.in_plane_axies[i][0])
            # self.in_plane_axies[i][1] = np.dot(rot_mat, self.in_plane_axies[i][1])


        point_np = np.array([self.valve_plane_center[0],self.valve_plane_center[1],self.valve_plane_center[2],1]).reshape(-1,1)
        self.valve_plane_center =  np.dot(self.transformation_world2mesh, point_np).reshape(1,-1)
        self.valve_plane_center = self.valve_plane_center[0,0:3]

        point_np = np.array([self.valve_plane_normal[0],self.valve_plane_normal[1],self.valve_plane_normal[2],0]).reshape(-1,1)
        self.valve_plane_normal =  np.dot(self.transformation_world2mesh, point_np).reshape(1,-1)
        self.valve_plane_normal =  self.valve_plane_normal[0,0:3]

        return original_long_axis, original_rv_axis

    def lengthLAX(self):
        #Compute the extension of the LV in the long axis to estimate scaling of SM
        min_z = 10
        for i in range(self.n_slices):
            min_z = np.min([min_z, self.epi_points[i][:, 2].min()])

        return  self.valve_plane_center[2] - min_z

    def lengthSAX(self):
        #Compute the extension of the LV in the sax axis to estimate positions of cp
        max_x = -10
        min_x = 10
        max_y = -10
        min_y = 10

        for i in range(self.n_slices):
            max_x = np.max([max_x,self.epi_points[i][:,0].max()])
            min_x = np.min([min_x, self.epi_points[i][:, 0].min()])
            max_y = np.max([max_y,self.epi_points[i][:,1].max()])
            min_y = np.min([min_y, self.epi_points[i][:, 2].min()])

        return np.max([max_x - min_x,max_y - min_y])

    def make_tensors(self):
        # make required pytorch tensors:
        # (these get used directly in the optimisation and so need to be pytorch tensors)
        for i in range(self.n_slices):
            self.t_epi_points.append(torch.tensor(self.epi_points[i],
                                                  dtype=torch.float32).contiguous().to(device))
            self.t_endo_points.append(torch.tensor(self.endo_points[i],
                                                   dtype=torch.float32).contiguous().to(device))
            # self.t_origins.append(torch.tensor(self.origins[i], dtype=torch.float32).to(device))
            # self.t_normals.append(torch.tensor(self.normals[i], dtype=torch.float32).to(device))
            # self.t_in_plane_axies.append(torch.tensor(self.in_plane_axies[i], dtype=torch.float32).to(device))
            #self.t_coords.append(torch.tensor(self.coords[i], dtype=torch.float32).to(device))
        self.t_long_axis = torch.tensor(self.long_axis, dtype=torch.float32).to(device)
        self.t_rv_axis = torch.tensor(self.rv_axis, dtype=torch.float32).to(device)
        #self.t_valve_plane_points = {}
        # for key in self.valve_plane_points:
        #     self.t_valve_plane_points[key] = torch.tensor(self.valve_plane_points[key],
        #                                                   dtype=torch.float32)

    def setLongAxis(self, axis: np.ndarray):
        """can be used to explicitly set the long axis"""
        self.long_axis   = axis + 0
        self.t_long_axis = torch.tensor(self.long_axis, dtype=torch.float32).to(device)

    def forward(self):
        """Returns the epi and endo points for all slices, after applying the per slice
        transformations """

        moved_epi_points, moved_endo_points = [], []

        for i in range(self.n_slices):
            moved_epi_points.append(self.applyTransform(self.t_epi_points[i],
                                                        self.slice_affine_and_id[i], self.offset[i]*self.mask_offset[i])
                                    )
            moved_endo_points.append(self.applyTransform(self.t_endo_points[i],
                                                        self.slice_affine_and_id[i], self.offset[i]*self.mask_offset[i])
                                     )
        return torch.cat(moved_epi_points), torch.cat(moved_endo_points)


    def applyTransform(self, points: torch.Tensor, affine_info, offset: torch.Tensor = None) -> torch.Tensor:
        """This function transform a set of points according to a given rotation and/or offset

        points: a pytorch tensor of 3D point, of shape (N,3)
        offset: a pytorch tensor of shape (3,) (or None, meaning don't apply any offset)
        rotation: a pytorch tensor of shape (3,) (or None, meaning don't apply any rotation)

        note that currently rotations are disabled (allowing all slices to rotate independently
        seemed to add too much freedom, and is probably not so realistic in most cases)

        in_plane_axies: a pytorch tensor of shape (2,3) giving the two in-plane axes
        normal: a pytorch tensor of shape (3,) giving the normal axes of the plane

        passing in_plane_axies and/or normal influences how the offsets are applied:

        a. if neither are passed then offset vector is just applied directly
        b. if just in_plane_axies is passed then the offset is restricted to be only in these
            in-pane directions
        c. if both in_plane_axies and normal are passed then offset in the in-plane directions is
            carried out as in case (b), but also a scaled down offset in the normal direction is
            performed (this means that larger offset values are required to produce movement in the
            out-of-plane direction, and thus if the size of the offset is regularized then the
            result is that movement in the in-plane direction is easier than than in the normal
            direction)
        """
        if offset is not None:
            toSM = torch.tensor(self.transformation_world2mesh.dot(affine_info[0]),dtype=torch.float)
            xyz_off = self.pix_coords_affine_torch(offset, toSM, affine_info[1])
            points = points + xyz_off.type(torch.float)
        return points

    # def getOriginsAndNormals(self) -> Tuple[List[torch.Tensor], List[torch.Tensor]]:
    #     """Get the transformed origins and normal for all slices (i.e. the origins and normals
    #     after each per-slice transform has been applied). Used e.g. for visulaising slices in their
    #     learned shifted position.
    #     """
    #     moved_origins, moved_normals = [], []
    #     for i in range(self.n_slices):
    #         moved_origins.append(self.applyTransform(self.t_origins[i], self.offset[i],
    #                                                  self.slice_affine_and_id[i], self.t_in_plane_axies[i],
    #                                                  self.t_normals[i]))
    #         moved_normals.append(self.applyTransform(self.t_normals[i], None, self.rotation[i]))
    #     return moved_origins, moved_normals

    def shiftImages(self, img, MM2World, Img2World):

        dims = img.shape

        x = np.linspace(0, dims[1], dims[1])
        y = np.linspace(0, dims[2], dims[2])
        z = np.linspace(0, dims[0], dims[0])

        Xpix, Ypix, Zpix = np.meshgrid(x, y, z, indexing='ij')

        # These should be arranged slicewise
        for i in range(dims[0]):
            slice_pix = np.concatenate((Xpix[..., i].reshape(-1, 1, order='F'),
                                        Ypix[..., i].reshape(-1, 1, order='F'),
                                        Zpix[..., i].reshape(-1, 1, order='F'),
                                        np.ones_like(Xpix[..., i].reshape(-1, 1, order='F'))), 1)
            sliceMM = np.dot(np.linalg.inv(MM2World).dot(Img2World), slice_pix.T).T

            sliceMM_corrected = self.applyTransform(
                torch.tensor(sliceMM[:, :3]), -self.offset[i], self.rotation[i], self.t_in_plane_axies[i],
                             self.t_normals[i]).to('cpu').detach().numpy()

            sliceMM[:, :3] = sliceMM_corrected.copy()

            ImgPix_new = (np.linalg.inv(Img2World).dot(np.dot(MM2World, sliceMM.T))).T
            Xpix_corrected = ImgPix_new[:, 0]#.reshape(dims[1], dims[2], order='F')
            Ypix_corrected = ImgPix_new[:, 1]#.reshape(dims[1], dims[2], order='F')
            interpImg = scipy.interpolate.interp2d(Xpix_corrected, Ypix_corrected, img[i,...].reshape(-1,1,order='F'),kind='linear')
            print(slice_pix[:, 0].shape)
            print(interpImg(slice_pix[:, 0].reshape(-1,1), slice_pix[:, 1].reshape(-1,1)).shape)
            img[i,...] = interpImg(slice_pix[:, 0], slice_pix[:, 1]).reshape(dims[1], dims[2], order='F')

        return img

    def getImageCoords(self) -> List[torch.Tensor]:
        """gets the transformed per-pixel coordinates for all slices in SM space"""

        coords = []
        xv, yv = np.meshgrid(np.linspace(0, 192, 192),
                                 np.linspace(0, 192, 192), indexing='ij')

        xv = xv.reshape(-1, 1, order='F')
        yv = yv.reshape(-1, 1, order='F')
        one_vector = np.ones_like(xv)

        for i in range(self.n_slices):
            worl2SM_transform = np.dot(self.transformation_world2mesh,self.slice_affine_and_id[i][0])
            PixCoords = np.concatenate((xv,yv,self.slice_affine_and_id[i][1]*one_vector, one_vector),1)
            SMCoords  = torch.tensor(np.dot(worl2SM_transform, PixCoords.T).T)
            # X = np.expand_dims(WorldCoords[:, 0].reshape(xv.shape, order='F'),-1)
            # Y = np.expand_dims(WorldCoords[:, 1].reshape(xv.shape, order='F'),-1)
            # Z = np.expand_dims(WorldCoords[:, 2].reshape(xv.shape, order='F'),-1)
            #
            # actual_coords = np.concatenate((X,Y,Z),-1)

            coords.append(self.applyTransform(SMCoords[:,:3], [worl2SM_transform,self.slice_affine_and_id[i][1]], self.offset[i]))

        return coords

    def getValvePlane(self) -> Tuple[torch.Tensor, torch.Tensor]:
        """Returns the valve plane (as a point on the plane and a normal vector) also accounts for
        slice shifts (i.e. if slices containing some of the valve_plane_points have moved this
        is taken into account when calculating the (current) valve plane)
        """

        # if we don't have enough valve plane points to reliably estimate the valve plane we just
        # return None

        # vp_points = []
        # n_lax_4ch_views = len(self.t_valve_plane_points.keys())
        #
        # if n_lax_4ch_views > 1: # computes plane from the point cloud
        #     for iter in self.t_valve_plane_points:
        #         # compensate for the removed slices in self.removeSlices() (over valve plane or not LV)
        #         i = np.squeeze(np.argwhere(np.array(self.slice_ids) == iter))
        #         if not i == []:
        #             vp_points.append(self.applyTransform(self.t_valve_plane_points[iter].to(device), self.offset[i].to(device), self.rotation[i].to(device),
        #                                             self.t_in_plane_axies[i].to(device), self.t_normals[
        #                                                 i].to(device)))  # , use_offset=self.learn_offset, use_rotation=self.learn_rotation) )
        #
        #     vp_points        = torch.cat(vp_points).to(device)
        #     vp_points_center = torch.mean(vp_points, 0, keepdim=False)
        #     x = vp_points - vp_points_center
        #     M = torch.matmul(torch.transpose(x, 0, 1), x)
        #
        #     norm_v = torch.linalg.svd(M)[0][:, -1]
        #     norm_v = norm_v / torch.sum(norm_v ** 2) ** 0.5
        #
        # else:
        #     vp_points_center = torch.tensor(self.valve_plane_centers_fromLAX[0][0]).to(device)
        #     norm_v = torch.tensor(self.valve_plane_normals_fromLAX[0][0]).to(device)

        return torch.tensor(self.valve_plane_center), torch.tensor(self.valve_plane_normal)

    def show(self, camera_pos=None, show_segmentation_masks=True) -> 'pyvista.Camera':
        """use pyvista to visualise the slice data"""

        plotter = pv.Plotter(off_screen=True)
        if camera_pos is not None:
            plotter.camera = camera_pos

        coords = self.getImageCoords()

        if show_segmentation_masks:
            for i in range(self.n_slices):
                xyz = coords[i].cpu().numpy()
                X, Y, Z = xyz[..., 0].reshape(192, 192, order='F'), xyz[..., 1].reshape(192, 192, order='F'), xyz[..., 2].reshape(192, 192, order='F')
                grid = pv.StructuredGrid(X, Y, Z)
                plotter.add_mesh(grid, scalars=self.images[i][..., 1].T, opacity=[0, 1])

        # make some point clouds for visulaising the (transformed) slice data:

        with torch.no_grad():
            t_points = self()
        epi_point_cloud = pv.PolyData(np.concatenate(t_points[0].detach().cpu().numpy()))
        endo_point_cloud = pv.PolyData(np.concatenate(t_points[1].detach().cpu().numpy()))

        plotter.add_mesh(endo_point_cloud, color='red')
        plotter.add_mesh(epi_point_cloud, color='blue')

        if self.t_long_axis is not None:
            lax = self.t_long_axis.cpu().numpy()
            plotter.add_mesh(pv.Line(pointa=np.array([0, 0, 0]), pointb=lax + np.array([0, 0, 0])),
                             line_width=10, color='orange')
        if self.t_rv_axis is not None:
            rvax = self.t_rv_axis.cpu().numpy()
            plotter.add_mesh(pv.Line(pointa=np.array([0, 0, 0]), pointb=rvax + np.array([0, 0, 0])),
                             line_width=10, color='red')

#        if self.enough_valve_plane_points:
        with torch.no_grad():
            vp_center, vp_normal = self.getValvePlane()
        vp_center = vp_center.cpu().numpy()
        vp_normal = vp_normal.cpu().numpy()

        plotter.add_mesh(pv.Sphere(0.05, vp_center), color='green')
        plotter.add_mesh(pv.Line(pointa=vp_center, pointb=vp_center + vp_normal), line_width=10,
                         color='red')

        valve_plane_disc = pv.Disc(center=vp_center, inner=0., outer=1., normal=vp_normal)
        plotter.add_mesh(valve_plane_disc, color='green', opacity=0.5)
        valve_plane_disc = pv.Disc(center=[0,0,0], inner=0., outer=1., normal=vp_normal)
        plotter.add_mesh(valve_plane_disc, color='blue', opacity=0.1)


        plotter.add_mesh(pv.Sphere(0.01, self.bp_com), color='blue')
        plotter.add_mesh(pv.Sphere(0.01, self.rv_com), color='red')

        plotter.add_mesh(pv.Sphere(0.01, vp_center), color='yellow')

        marker_args = dict(cone_radius=0.6, shaft_length=0.7, tip_length=0.3, ambient=0.5,
                           label_size=(0.4, 0.16))
        _ = plotter.add_axes(line_width=5, marker_args=marker_args)

        if not os.path.exists(self.out_dir+'/slice_data/'):
            os.makedirs(self.out_dir+'/slice_data/')

        counter_images = len(next(os.walk(self.out_dir+'/slice_data/'))[2])
        plotter.save_graphic(self.out_dir+'/slice_data/Frame_%02d.pdf' %(counter_images))
        camera = plotter.camera
        plotter.close()
        return camera
