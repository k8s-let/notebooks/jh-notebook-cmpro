__all__ = ["rotation_matrix_from_vectors", "rotation_tensor", "ensureOneHot",
           "cleanSegmentationData", "addTextLabels", "largestConnectedComponent", "smartFillLVBP",
           "convexHull",'pointToPlaneDistance']

import torch

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

import numpy as np

from scipy.ndimage import binary_erosion as erode
from scipy.ndimage import binary_dilation as dilate
from scipy.ndimage import binary_fill_holes as fill

from skimage.morphology.convex_hull import convex_hull_image
from skimage.measure import label as label_cc

import os
from PIL import Image, ImageFont, ImageDraw


def rotation_matrix_from_vectors(vec1, vec2):
    """ Find the rotation matrix that aligns vec1 to vec2
    vec1: a 3d "source" vector
    vec2: a 3d "destination" vector
    returns: a transform matrix (3x3) which when applied to vec1, aligns it with vec2
    i.e. vec1_rotated_to_align_with_vec2 = np.dot(rotation_matrix, vec1)
    """

    if np.dot(vec1, vec2) == 1:
        return np.eye(3)
    if np.dot(vec1, vec2) == -1:
        return -np.eye(3)

    a, b = (vec1 / np.linalg.norm(vec1)).reshape(3), (vec2 / np.linalg.norm(vec2)).reshape(3)
    v = np.cross(a, b)
    c = np.dot(a, b)
    s = np.linalg.norm(v)
    kmat = np.array([[0, -v[2], v[1]], [v[2], 0, -v[0]], [-v[1], v[0], 0]])
    rotation_matrix = np.eye(3) + kmat + kmat.dot(kmat) * ((1 - c) / (s ** 2))
    return rotation_matrix


def addTextLabels(arr, x, y, dx, dy, texts, font_size=16, font_color=(255, 255, 255), blackbox=True,
                  blackbox_color='black'):
    """
    adds labels (i.e. some text) to an image

    argument:
        arr : the image to add text to, should be a numy array
        x,y : the (bottom left) position of the first label text (in pixels, 0,0 = left,top)
        dx,dy : the ammount to shift (in pixels) between labels
        texts : a list of strings to be used as the labels

        optional:
        font_size : the font size (height in pixels I think)
        font_color : the font color as an RGB tuple, should be a tuple of 3 ints in [0,255]

    returns:
        the image (as a numpy array) with the text added
    """

    img = Image.fromarray(arr)
    draw = ImageDraw.Draw(img)

    font_path = f"{os.path.dirname(os.path.dirname(__file__))}/font/FreeMono.ttf"

    font = ImageFont.truetype(font_path, font_size)

    for i, text in enumerate(texts):
        if blackbox:
            w, h = font.getsize(text)
            draw.rectangle((x + i * dx, y + i * dy, x + i * dx + w, y + i * dy + h), fill='black')
        draw.text((x + i * dx, y + i * dy), text, font_color, font=font)

    return np.asarray(img)


def rotation_tensor(theta, phi, psi, n_comps):
    # from https://discuss.pytorch.org/t/constructing-a-matrix-variable-from-other-variables/1529/3
    theta = theta.to(device)
    phi = phi.to(device)
    psi = psi.to(device)

    one = torch.ones(n_comps, 1, 1).to(device)
    zero = torch.zeros(n_comps, 1, 1).to(device)
    rot_x = torch.cat((
        torch.cat((one, zero, zero), 1),
        torch.cat((zero, theta.cos(), theta.sin()), 1),
        torch.cat((zero, -theta.sin(), theta.cos()), 1),
    ), 2).to(device)
    rot_y = torch.cat((
        torch.cat((phi.cos(), zero, -phi.sin()), 1),
        torch.cat((zero, one, zero), 1),
        torch.cat((phi.sin(), zero, phi.cos()), 1),
    ), 2).to(device)
    rot_z = torch.cat((
        torch.cat((psi.cos(), psi.sin(), zero), 1),
        torch.cat((-psi.sin(), psi.cos(), zero), 1),
        torch.cat((zero, zero, one), 1)
    ), 2).to(device)
    # print(rot_x.shape)
    return torch.bmm(rot_z, torch.bmm(rot_y, rot_x))


def ensureOneHot(sp):
    """make sure the mask data is binary and make sure each pixel has at most one channel active"""

    sp = np.clip(np.round(sp), 0, 1)
    r, g, b = sp[..., 0], sp[..., 1], sp[..., 2]
    r *= 1 - g
    r *= 1 - b
    g *= 1 - b
    sp[..., 0], sp[..., 1], sp[..., 2] = r, g, b

    return sp


def fillHoles(sp):
    """
    fill holes in blue region blue,
    then fill holes in combined blue and green region blue
    then fill any holes in the  combined blue, green and red region red
    """

    for i in range(sp.shape[0]):
        for j in range(sp.shape[1]):
            sp[i, j, ..., 2] = fill(sp[i, j, ..., 2])

            gb = np.sum(sp[i, j, ..., 1:], -1)
            holes = fill(gb) - gb
            sp[i, j, ..., 2] += holes

            gb = np.sum(sp[i, j], -1)
            holes = fill(gb) - gb
            sp[i, j, ..., 0] += holes

    return sp


def largestConnectedComponent(img):
    # check its a 3 channel image:
    assert img.shape[2] == 3 and len(img.shape) == 3

    # combine channels to get a binary mask:
    binary = np.clip(np.sum(img, axis=2), 0, 1)

    # if the image is empty don't do anything:
    if np.sum(binary) == 0:
        return img

    # get mask for the largest connected component:
    labeled_image, count = label_cc(binary, return_num=True)
    component_sizes = []
    for i in range(1, count + 1):
        component_sizes.append(np.sum(labeled_image == i))
    largest_region_val = np.argmax(component_sizes) + 1
    largest_region_mask = labeled_image == largest_region_val

    return img * largest_region_mask[..., None]


def convexHull(sp):
    """
    fills the convex hull of the blue+green region with blue
    however, any added regions must touch an existing blue region
    """

    gb = np.sum(sp[..., 1:], -1)

    for_hulls = np.zeros(sp.shape[:-1])
    for i in range(sp.shape[0]):
        for j in range(sp.shape[1]):

            if np.sum(sp[i, j, ..., 2]) == 0:
                continue
            else:
                # get a pixel in the existing blue region:
                pixel_positions = np.where(sp[i, j, ..., 2])
                pp = [pixel_positions[0][0], pixel_positions[1][0]]
                assert sp[i, j, pp[0], pp[1], 2] == 1

                hull = convex_hull_image(gb[i, j]) * 1
                hull = hull - sp[i, j, ..., 1]
                labeled_image = label_cc(hull)
                to_keep = labeled_image[pp[0], pp[1]]
                mask = labeled_image == to_keep
                hull = hull * mask
                hull = hull - sp[i, j, ..., 2]
                sp[i, j, ..., 2] += hull

    return sp


def smartFillLVBP(mask, min_green=0.2, min_blue=0.2):
    """
    moving through the image at the pixel level, first row-by-row then column by column we
    look for regions that fall between two green peices, and fill them blue
    note, this is quite an aggressive correction process but can help clean up bad LV blood pool segmentations
    """

    assert len(mask.shape) == 5  # mask shape should be (t,s,h,w,3)

    # first do row-by-row:
    mask = _doFillBetweenGreen(mask, min_green, min_blue)

    # next column-by-column (by passing the transposed image)''
    mask = np.moveaxis(mask, 2, 3)
    mask = _doFillBetweenGreen(mask, min_green, min_blue)
    mask = np.moveaxis(mask, 2, 3)

    return mask


def _doFillBetweenGreen(mask, min_green=0.2, min_blue=0.2):
    """
        scans through the image at the pixel level, row-by-row
        fills all pixels between the first and second green regions encountered blue
    """

    for t in range(mask.shape[0]):  # for each time-frame

        for s in range(mask.shape[1]):  # for each slice

            for j in range(mask.shape[2]):

                state, start, end = 0, None, None
                for k in range(mask.shape[3]):

                    if mask[
                        t, s, j, k, 1] >= min_green:  # count a pixel as green if it's green channel is at least 0.2

                        if state == 0:
                            state = 1
                        elif state == 2:
                            state = 3
                            end = k
                    else:
                        if state == 1:
                            state = 2
                            start = k

                if state == 3:
                    has_blue = mask[t, s, j, start:end, 2] >= min_blue
                    mask[t, s, j, start:end, 0] -= mask[t, s, j, start:end, 0] * has_blue
                    mask[t, s, j, start:end, 1] -= mask[t, s, j, start:end, 1] * has_blue
                    mask[t, s, j, start:end, 2] = mask[t, s, j, start:end, 2] * (
                                1 - has_blue) + has_blue

    return mask


def cleanSegmentationData(sp, pad=1):
    """
    This function cleans up the segmentation masks in various ways:
        0. potential to do some things here
        1. firstly make sure the segmentation data is "one-hot"
        2. fill holes in the segmentation masks
        3. smooth frames over time
            - specifically if we consider three conscutive frames t1, t2, t3, then anything
              not present in t1 and t3 should also not be present in t2
              (this is approximatley true, we actually allow a margin of error (in pixels) (the pad variable)
        4. keep only the largest connected component (when considering all classes together)
        5. tries to fill in any missing LB blood pool by leveraging the fact that the LV should be a convex shape
    """

    assert len(sp.shape) == 5 and sp.shape[-1] == 3

    # step 0:
    # at this stage segmentation masks are still "soft" (i.e. probabilies, rather than binary masks),
    # so potentially some useful corrections can be done here using that softness,
    # e.g. smartFillLVBP sets pixels that fall inside the LV as LVBP if they are >= 0.2 probability LVBP:
    # sp = smartFillLVBP(sp)

    # step 1:
    sp = ensureOneHot(sp)

    # step 2:
    sp = fillHoles(sp)

    # step 3:
    sp_prev = np.roll(sp, shift=-1, axis=0)
    sp_next = np.roll(sp, shift=1, axis=0)
    sp_union = np.clip(np.sum(sp_prev, -1) + np.sum(sp_next, -1), 0, 1)

    for i in range(sp_union.shape[0]):
        for j in range(sp_union.shape[1]):
            sp_union[i, j] = dilate(sp_union[i, j], iterations=pad)  # allow a certain buffer

    sp = sp * sp_union[..., None]

    # step 4:
    for i in range(sp.shape[0]):
        for j in range(sp.shape[1]):
            sp[i, j] = largestConnectedComponent(sp[i, j])

    # step 5:
    sp = convexHull(sp)

    return sp

def pointToPlaneDistance(normal, origin, point):
    '''
	computes the distances between a list of points and a plane (defined by a normal vector anad a point on the plane)

	normal: the plane's normal vector, should be a unit vector with shape (3,)
	origin: a point on the plane, should be a vector with shape (3,)
	points: a list of points, should be shape (n,3)

	returns a list of distances of shape (n,)
    '''
    a, b, c = normal
    d = -torch.sum(normal*origin)
    x1, y1, z1 = point[:,0], point[:,1], point[:,2]
    d = torch.abs(a * x1 + b * y1 + c * z1 + d)
    return d
