__all__ = ["ProcessedExamObject",]

from typing import List

import pyvista as pv
import pickle
import numpy as np
import gzip

class ProcessedExamObject:
    """A simple representation of a processed CMRI Exam

    self.series is a list of dictionaries (1 for each series in the io)
    self.lge_series is a list of dictionaries (1 for each lge series in the exam)

    each dictionary, s, has
        s['img'] => a one channel array containing the image data for the series,
            shape = (timesteps, slices, H, W)
        s['seg'] => a three channel segmentation mask for the image data,
                shape = (timesteps, slices, 3, H, W)
        s['coord'] => a three channel (i.e. 3D) array containing the world space
                coordinates of every pixel, shape = (slices, 3, H, W)
    """
    series: List[dict]
    affine: List[int]
    affine_resampled: [int]

    def __init__(self):
        self.series = []
        self.lge_series = []
        self.affine = []
        self.affine_resampled = []
        self.stack_orientation_fromSAX = None
        self.lge_time_frame=None #time frame from the sax cine series corresponding to the trigger time of the lge image

    def addSeries(self, seg, coord, img, view, affineMatrix, affineMatrixRes):
        '''
        adds a series by appending a dictionary to the list of dictionaries (self.series)
        '''
        self.series.append({
            'seg': seg,
            'coord': coord,
            'img': img,
            'view': view,
            'affine': affineMatrix,
            'affineRes': affineMatrixRes,
        })

    def addLGESeries(self, seg, coord, img, view, affineMatrix, affineMatrixRes,lge_time_frame,seg_LV):
        '''
        adds a lge series by appending a dictionary to the list of dictionaries (self.series)
        '''
        self.lge_series.append({
            'seg': seg, #channels: std2to5, healthy, >std5
            'coord': coord,
            'img': img,
            'view': view,
            'affine': affineMatrix,
            'affineRes': affineMatrixRes,
            'seg_LV': seg_LV,
        })
        self.lge_time_frame=lge_time_frame

    def addSegementationToLGESeries(self, seg):
        '''
        adds a lge segmentation to an existing lge series dictionary
        '''
        for lges in self.lge_series:
            if lges['view']=='SAX':
                lges['seg']=seg
            else:
                print('attention: no sax lge series')

    def compress(self, coord=True, seg=True, img=True):
        """compresses the coord array (makes it small for saving)
        also converts the segmenttion masks and the CMR images from float values in [0,1] to
        uints in [0,255] this is slightly lossy, but should make the save file much smaller,
        and shouldn't be lossy in a way that matters you can also toggle the compression for seg,
        img and coord individually
        :param coord:
        :param seg:
        :param img:
        """

        for i in range(len(self.series)):

            if coord:
                xyz_start = self.series[i]['coord'][0, :, 0, 0]
                xyz_diag = self.series[i]['coord'][1, :, 1, 1] - xyz_start
                self.series[i]['compressed_coord'] = {'start': xyz_start, 'diag': xyz_diag}
                del self.series[i]['coord']

            if seg:
                self.series[i]['seg'] = (self.series[i]['seg'] * 255).astype('uint8')

            if img:
                self.series[i]['img'] = (self.series[i]['img'] * 255).astype('uint8')

        return self

    def decompress(self, coord=True, seg=True, img=True):
        """decompresses the coord array (makes it back to an array, which is neccessary
        before using the peo) also convert the segmentation masks values and/or MR image
        values back to [0,1] (not neccessarily exactly the same as it was, but every value should
        be within ~0.004)
        """

        for i in range(len(self.series)):
            t, slc, h, w = self.series[i]['img'].shape

            if coord:
                new_coord = (np.zeros((slc, 3, h, w), dtype='float32') +
                             self.series[i]['compressed_coord']['start'][None, :, None, None])
                ds, dh, dw = self.series[i]['compressed_coord']['diag']
                for s in range(slc):
                    for hi in range(h):
                        for wi in range(w):
                            new_coord[s, :, hi, wi] += np.array([hi * ds, wi * dh, s * dw])
                self.series[i]['coord'] = new_coord

            if seg:
                self.series[i]['seg'] = self.series[i]['seg'].astype('float') / 255.

            if img:
                self.series[i]['img'] = self.series[i]['img'].astype('float') / 255.

        return self

    def show(self, t_step=0, just_central_slices=False, what_to_show='images', round_masks=False):
        """visulaise the slice data"""

        assert what_to_show in ['images', 'masks']

        plotter = pv.Plotter()

        for s in self.series:

            data = s['img'][t_step]
            masks = s['seg'][t_step]
            coords = s['coord']

            if just_central_slices:
                slices_to_show = [coords.shape[0] // 2]
            else:
                slices_to_show = range(coords.shape[0])

            for i in slices_to_show:
                x, y, z = coords[i][0], coords[i][1], coords[i][2]
                grid = pv.StructuredGrid(x, y, z)

                if what_to_show == 'images':
                    plotter.add_mesh(grid, scalars=data[i].T, opacity=0.5, cmap='gray')

                elif what_to_show == 'masks':
                    msk = masks[i]
                    # if there is a channels dimentions, times each channel by a different
                    # value and sum
                    if len(msk.shape) == 3:
                        msk = np.sum(msk * np.arange(1, msk.shape[0] + 1)[:, None, None], 0)
                    if round_masks:
                        msk = np.round(msk)
                    plotter.add_mesh(grid, scalars=msk.T, opacity=0.5, cmap='jet')

        plotter.show()

    @classmethod
    def from_file(cls, filename: str, compressed: bool = True) -> 'ProcessedExamObject':
        if compressed:
            if not filename.endswith('.gz'):
                filename += '.gz'
                print('loading', filename)
            exam_object: ProcessedExamObject = pickle.load(gzip.open(filename, 'r'))
        else:
            with open(filename, 'rb') as inp:
                exam_object: ProcessedExamObject = pickle.load(inp)

        return exam_object

    def save_pickle(self, filename: str, compressed: bool = True):
        if compressed:
            pickle.dump(self, gzip.open(filename + '.gz', 'wb'))
        else:
            with open(filename, 'wb') as outp:  # Overwrites any existing file.
                pickle.dump(self, outp, pickle.HIGHEST_PROTOCOL)

    def export_vti(self, target_folder: str):
        """
        exports vti of the segmentation wioth multiple channels in world coordinates
        :param target_folder: str
        :return:
        """
        import os, vtk
        from tqdm import tqdm
        from vtk.util.numpy_support import numpy_to_vtk

        for counter,series in enumerate(self.series):
            vti_out = target_folder + '/' + series['view'] + '_multiclass_VTI'
            os.makedirs(vti_out, exist_ok=True)

            data = np.transpose(series['seg'], (2,3,1,0))
            vti_image = vtk.vtkImageData()
            vti_image.SetDimensions(data.shape[:3])
            vti_image.SetSpacing((1.0, 1.0, 1.0))

            orig_img3D = series['affineRes'].dot(np.array([0,0,0,1]).reshape(-1,1))
            vti_image.SetOrigin(orig_img3D[:3])

            # This automatically allows to visualize the correct orientation in space,
            # but paraview has issues with the slicing.
            Img2World_vtk = vtk.vtkMatrix3x3()
            for i in range(3):
                for j in range(3):
                    Img2World_vtk.SetElement(i, j, series['affineRes'][i, j])
            vti_image.SetDirectionMatrix(Img2World_vtk)
            np.save(vti_out + '/Img2World_matrix4x4.npy', self.affine)

            for phase_sel in tqdm(range(data.shape[-1]), desc="Writing vti files"):
                # Add mask labels and store vti

                linear_array = data[...,phase_sel].reshape(-1, 1, order="F")
                vtk_array = numpy_to_vtk(linear_array)
                vtk_array.SetName('labels')
                vti_image.GetPointData().AddArray(vtk_array)
                vti_image.Modified()

                writer = vtk.vtkXMLImageDataWriter()
                writer.SetFileName(vti_out + '/frame_%02d.vti' % (phase_sel))
                writer.SetInputData(vti_image)
                writer.Write()
