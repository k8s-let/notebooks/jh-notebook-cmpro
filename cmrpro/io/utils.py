__all__ = ["long_substr", "is_substr", "get_sorted_filenames",
           "data_array_from_dicom_folder", "sorted_nicely","planeToGrid","planeToXYZ","to3Ch"]

import re
import os
import numpy as np
import pyvista as pv
from typing import Tuple, List
import pydicom

def long_substr(data):
    """finds the longest common substring from a list of strings"""
    substr = ''
    if len(data) > 1 and len(data[0]) > 0:
        for i in range(len(data[0])):
            for j in range(len(data[0]) - i + 1):
                if j > len(substr) and is_substr(data[0][i:i + j], data):
                    substr = data[0][i:i + j]
    return substr


def is_substr(find, data):
    """helper function for long_substr()"""
    if len(data) < 1 and len(find) < 1:
        return False
    for i in range(len(data)):
        if find not in data[i]:
            return False
    return True


def check_name(file_name):
    return ((file_name[0] != '.') and ('.gif' not in file_name))


def get_sorted_filenames(dicom_path):
    lstFilesDCM = []
    for dirName, subdirList, fileList in os.walk(dicom_path):
        for filename in fileList:
            if check_name(filename):
                lstFilesDCM.append(os.path.join(dirName, filename))
    lstFilesDCM = sorted(lstFilesDCM)

    return lstFilesDCM


def data_array_from_dicom_folder(PathDicom):

    lstFilesDCM = get_sorted_filenames(PathDicom)

    # Get ref file (find one that has pixel spacing)
    for i, f in enumerate(lstFilesDCM):
        if 'XX' not in f:
            try:
                ref_dicom = pydicom.read_file(lstFilesDCM[i], force=True)
                assert len(ref_dicom[0x028,0x030].value)==2
                break
            except:
                continue

    dicom_dir_details = {
        'SliceLocation': ref_dicom.get('SliceLocation', '?'),
        'InstanceNumber': ref_dicom.get('InstanceNumber', '?'),
        # 'ImageSize':RefDs.pixel_array.shape,
        'ImagePosition': ref_dicom.get('ImagePositionPatient', '?'),
        'ImageOrientation': ref_dicom.get('ImageOrientationPatient', '?'),
        'PatientPosition': ref_dicom.get('PatientPosition', '?'),
        'PixelSpacing': ref_dicom.get('PixelSpacing', '?'),
    }

    # Load spacing values (in mm)
    constant_pixel_spacing = (float(ref_dicom[0x018,0x050].value),
                              float(ref_dicom.PixelSpacing[0]),
                              float(ref_dicom.PixelSpacing[1]))

    # loop through all the DICOM files and build lists of spatial and temporal positions:
    slice_locations, trigger_times, slice_positions = [], [], []


    original_lstFilesDCM = lstFilesDCM.copy()
    lstFilesDCM = []
    for filenameDCM in original_lstFilesDCM:
        if 'XX' not in filenameDCM:
            dicom_file = pydicom.read_file(filenameDCM, force=True)
            location   = dicom_file.get('SliceLocation', None)
            t_time     = dicom_file.get('TriggerTime', None)
            t_pos      = dicom_file.get('ImagePositionPatient', None)
            if location is not None and t_time is not None:
                slice_locations.append(location), trigger_times.append(t_time), slice_positions.append(t_pos[2])
                lstFilesDCM.append(filenameDCM)

    slice_locations = list(set(slice_locations))
    slice_locations.sort()
    trigger_times = list(set(trigger_times))
    trigger_times.sort()
    slice_positions = list(set(slice_positions))
    slice_positions.sort()

    # work out data size from unique spatial and temporal positions:
    data = np.zeros((len(trigger_times), len(slice_locations), int(ref_dicom.Rows),
                     int(ref_dicom.Columns)), dtype=ref_dicom.pixel_array.dtype)
    placment = np.zeros((len(trigger_times), len(slice_locations)))
    image_ids = np.zeros((len(trigger_times), len(slice_locations)))
    trigger_time_specific = np.zeros((len(trigger_times), len(slice_locations)))

    # load images into 4D data array:
    image_positions = [None] * len(slice_locations)

    for i, filenameDCM in enumerate(lstFilesDCM):
        if 'XX' not in filenameDCM:
            dicom_file = pydicom.read_file(filenameDCM, force=True)
            #location = dicom_file.get('SliceLocation', False)
            location = dicom_file.get('ImagePositionPatient', False)[2]
            t_time = dicom_file.get('TriggerTime', False)
            if location != '?' and t_time != '?':
                z = slice_positions.index(location)
                t = trigger_times.index(t_time)
                if dicom_file.pixel_array.shape == data[t, z].shape:
                    data[t, z] = dicom_file.pixel_array
                placment[t, z] = 1
                trigger_time_specific[t, z] = dicom_file.get('TriggerTime', False)
                image_ids[t, z] = i
                image_positions[z] = dicom_file.get('ImagePositionPatient', '?')

    # squash to solve small variations in trigger_time:
    i = 0
    while i < data.shape[0] - 1:
        if np.max((placment[i] > 0) * 1 + (placment[i + 1] > 0) * 1) <= 1:
            data = np.concatenate([data[:i], data[i + 1:i + 2] + data[i:i + 1], data[i + 2:]],
                                  axis=0)
            placment = np.concatenate([placment[:i], placment[i + 1:i + 2] + placment[i:i + 1],
                                       placment[i + 2:]], axis=0)
            image_ids = np.concatenate([image_ids[:i], image_ids[i + 1:i + 2] + image_ids[i:i + 1],
                                        image_ids[i + 2:]], axis=0)
            trigger_time_specific = np.concatenate([trigger_time_specific[:i], trigger_time_specific[i + 1:i + 2] + trigger_time_specific[i:i + 1],
                                        trigger_time_specific[i + 2:]], axis=0)
        else:
            i += 1
    # todo: check that this is actually false:
    is3D = False
    multifile = True

    return (data, constant_pixel_spacing, image_ids, dicom_dir_details, slice_locations,
            trigger_time_specific, image_positions, is3D, multifile)


def sorted_nicely(l):
    """ Sort the given iterable in the way that humans expect."""
    convert = lambda text: int(text) if text.isdigit() else text
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
    return sorted(l, key=alphanum_key)


def planeToGrid(img_size, slice_index, affine_matrix):
    xv, yv = np.meshgrid(np.linspace(0, img_size[0], img_size[0]),
                         np.linspace(0, img_size[1], img_size[1]), indexing='ij')
    one_vector = np.ones_like(xv)

    PixCoords = np.concatenate((xv.reshape(-1, 1, order='F'), yv.reshape(-1, 1, order='F'),
                                slice_index * one_vector.reshape(-1, 1, order='F'),
                                one_vector.reshape(-1, 1, order='F')), 1)
    WorldCoords = np.dot(affine_matrix, PixCoords.T).T

    X = WorldCoords[:, 0].reshape(xv.shape, order='F')
    Y = WorldCoords[:, 1].reshape(xv.shape, order='F')
    Z = WorldCoords[:, 2].reshape(xv.shape, order='F')

    grid = pv.StructuredGrid(X, Y, Z)

    return grid



def planeToXYZ(img_size: Tuple[int, int], z_position: float, affine_matrix: np.ndarray):
    xv, yv = np.meshgrid(np.linspace(0, img_size[0], img_size[0]),
                         np.linspace(0, img_size[1], img_size[1]), indexing='ij')
    one_vector = np.ones_like(xv)
    try:
        PixCoords = np.concatenate((xv.reshape(-1, 1, order='F'), yv.reshape(-1, 1, order='F'),
                                    z_position[2] * one_vector.reshape(-1, 1, order='F'),
                                    one_vector.reshape(-1, 1, order='F')), 1)
    except:
        PixCoords = np.concatenate((xv.reshape(-1, 1, order='F'), yv.reshape(-1, 1, order='F'),
                                    z_position * one_vector.reshape(-1, 1, order='F'),
                                    one_vector.reshape(-1, 1, order='F')), 1)
    WorldCoords = np.dot(affine_matrix, PixCoords.T).T

    X = WorldCoords[:, 0].reshape(img_size, order='F')
    Y = WorldCoords[:, 1].reshape(img_size, order='F')
    Z = WorldCoords[:, 2].reshape(img_size, order='F')

    return X, Y, Z


def to3Ch(img: np.ndarray) -> np.ndarray:
    """the input img should be array of shape (H,W) or (H,W,3) returns an array of shape
    (H,W,3) with values in the range [0-1] in the special case where the input is an array with
    shape (H,W) containing values in [0,1,2,3] then it is converted to a three channel image
    by doing:
    0 -> black, 1 -> red, 2 -> green, 3 -> blue
    this is useful for getting an array ready to be saved as an RGB image
    :param img:
    """

    if len(img.shape) == 2:
        img_vals = np.unique(img)
        if set(img_vals) <= set([0, 1, 2, 3]):
            cimg = np.zeros(img.shape + (3,))
            for i in [1, 2, 3]:
                cimg[img == i, i - 1] = 1
            return cimg
        else:
            img = img / img.max()
            return np.tile(img[..., None], (1, 1, 3))

    elif len(img.shape) == 3:
        img = img / img.max()
        return img

    else:
        print(
            'error, input to to3Ch() should have shape (H,W) or (H,W,3), but recieved input with shape:',
            img.shape)
