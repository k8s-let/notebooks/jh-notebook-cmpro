__all__ = ["CMRExam", ]

import os
from typing import List
import pickle
import numpy as np
from scipy.ndimage import zoom
from cmrpro.io._series import CMRSeries
import cmrpro.io.utils

class CMRExam:
    """An object which contains a number of CMRSeries objects"""
    time_frames: int
    id_string: str
    base_dir: str
    series: List['CMRSeries']
    frames: int
    lge_series: List['CMRSeries']

    def __init__(self, base_dir: str, id_string=None):

        print(f'\ncreating CMRExam from: {base_dir}')
        self.time_frames = None
        if id_string is None:
            id_string = os.path.basename(base_dir).split('.')[0]

        self.id_string = id_string
        self.base_dir = base_dir
        self.series = []
        self.lge_series = []

        if not os.path.isdir(base_dir):
            serie = CMRSeries(base_dir, self.id_string)
            # assigns the new constructed series to the cine or lge series list of the exam object depending on the
            # isLGESeries flag the flag serie.isLGESeries was set in the constructor of the series objeject in the
            # function guess_isLGE_from_folder_name depending on the file name
            if serie.isLGESeries:
                self.lge_series.append(serie)
            else:
                self.series.append(serie)
        else:
            ordered_series = cmrpro.io.utils.sorted_nicely(os.listdir(base_dir))
            # discard filed which should not be used but might be in the patient folder
            ordered_series = [x for x in ordered_series if x[0] != '.']
            ordered_series = [x for x in ordered_series if x[:2] != 'XX']
            ordered_series = [x for x in ordered_series if x[-3:] != 'rec']
            ordered_series = [x for x in ordered_series if x[-3:] != 'png']
            # ordered_series = [x for x in ordered_series if x != 'Output']
            ordered_series = [x for x in ordered_series if x[-4:] != '_VTI']

            for series_dir in ordered_series:
                full_path = os.path.join(base_dir, series_dir)
                print('loading series from /%s' % (series_dir,))
                ds = CMRSeries.from_file(full_path)
                if np.prod(ds.data.shape) > 1:
                    if ds.isLGESeries:
                        self.lge_series.append(ds)
                    else:
                        self.series.append(ds)

    @property
    def num_series(self):
        return len(self.series)

    def __getitem__(self, i):
        return self.series[i]

    def __len__(self):
        return len(self.series)

    def addSeries(self, dicom_series: CMRSeries):
        self.series.append(dicom_series)

    def summary(self):
        """print a short summary of the CMRExam"""
        summary_string = f'CMRExam summary: \tsource file/directory: {self.base_dir}' \
                         f'\n\tnumber of series:{self.num_series}\n\tdetails:\n'
        for s in self.series:
            summary_string += f"\t{str(s)}\n"
        if len(self.lge_series) > 0:
            summary_string += f"\tnumber of lge series:{str(len(self.lge_series))}\n"
            for s in self.lge_series:
                summary_string += f"\t{str(s.name)}\t {str(s.data.shape)}\n"
        print(summary_string)

    def __str__(self):
        return "\n".join([f'{series.name} {series.data.shape} ({series.prepped_data.shape})'
                          for series in self.series])

    def standardise_time_frames(self, resample_to='fewest'):
        """make sure all cine series have the same number of time frames, and resample them if they
        don't """

        print('standardising number of time frames across series by resampling..')

        if self.num_series == 1:
            print('only one series, so no resampling required')
            self.time_frames = self.series[0].frames
            return

        time_frames_seen = []
        for s in self.series:
            time_frames_seen.append(s.frames)
        time_frames_seen = np.unique(time_frames_seen)

        if len(time_frames_seen) == 1:
            self.time_frames = time_frames_seen[0]
            print('all series already have %d time frame(s), so no resampling required' % (
                time_frames_seen[0]))
            return

        if 1 in time_frames_seen:
            print('some series only have a single time frame, these will be ignored')
            time_frames_seen = [t for t in time_frames_seen if t != 1]

        if resample_to == 'fewest':
            # downsample to smallest time resolution:
            target_slices = np.min(time_frames_seen)
        else:
            # otherwise upsample to highest temporal resolution:
            target_slices = np.max(time_frames_seen)

        print('resampling all series to %d time frames' % (target_slices,))
        for s in self.series:

            # skip series with only 1 time frame (upsampling them doesn't really make any sense..)
            if s.frames == 1:
                continue

            if s.frames != target_slices:
                s.data = zoom(s.data, (target_slices / s.data.shape[0], 1, 1, 1), order=3)
                s.frames = target_slices

        self.frames = target_slices

    def save(self, target_folder: str = None):
        if target_folder is None:
            target_folder = self.folder['base']
        os.makedirs(target_folder, exist_ok=True)
        fname = os.path.join(target_folder, 'CMRExam.pickle')
        with open(fname, "wb") as file_to_save:
            pickle.dump(self, file_to_save)

    def visualiseSlicesIn3D(self, mesh: 'pyvista.UnstructuredGrid' = None,
                            time_frame: int = 0, folder: bool = None):
        '''shows the central slice of each series in an (interactive; if interactive==True; else exports png of the 3D view) 3D plot'''
        import pyvista as pv
        pv.set_plot_theme("document")

        plotter = pv.Plotter(off_screen=True)

        for s in np.concatenate((self.series, self.lge_series)):
            intensities = [s.data, s.seg][0]

            grid = cmrpro.io.utils.planeToGrid(intensities.shape[2:], s.slices // 2,
                                               s.affine_original)
            plot_scalars = intensities[time_frame, s.slices // 2].flatten(order="F")
            plotter.add_mesh(grid, scalars=plot_scalars, opacity=0.5, cmap='gray')

        if mesh is not None:
            plotter.add_mesh(mesh, show_scalar_bar=False, color='grey')

        plotter.show(screenshot=f"{folder}/3DSeries_intersections.png")
        plotter.close()
        return 0

    def combine_series(self, ):
        '''
        Method to combine SAX slices from the same stack that are saved in different exams
        '''

        to_remove = []
        groups = []
        for i in range(self.num_series):
            i_grouped = False
            for j, g in enumerate(groups):
                if np.array_equal(self.series[i].orientation, self.series[g[0]].orientation):
                    if self.series[i].slices == 1:  # only if there is 1 slice slone
                        groups[j].append(i)
                        i_grouped = True
                        break
            if not i_grouped:
                groups.append([i])

        groups = [np.array(g) for g in groups]

        # now work out the order to combine them:
        for g in groups:
            if len(g) > 1:
                # make sure they all match (keep the largest subset that share a data shape):
                all_shapes = []
                for j in g:
                    all_shapes.append(str(self.series[j].data.shape))
                unique, counts = np.unique(all_shapes, return_counts=True)
                target_shape = unique[np.argmax(counts)]
                new_g = []
                for j in g:
                    if target_shape == str(self.series[j].data.shape):
                        new_g.append(j)
                g = new_g

                # Slices need to be ordered based on their position, otherwise they could be in random order
                # using the position in space projected onto the normal of the reference one would provide the ordering
                rel_positions = [0]
                ref_direction = self.series[g[0]].stack_orientation / np.linalg.norm(
                    self.series[g[0]].stack_orientation)

                for i in g[1:]:
                    rel_pos = np.array(self.series[i].image_positions[0]) - np.array(
                        self.series[g[0]].image_positions[0])
                    rel_positions.append(np.dot(rel_pos, ref_direction))

                ordering_slices = np.argsort(np.array(rel_positions))
                gnew = []
                for k in ordering_slices:
                    gnew.append(g[k])
                g = gnew
                # make the first series the new combined series:
                i = g[0]
                to_remove = to_remove + g[1:]

                new_name = cmrpro.io.utils.long_substr([self.series[j].name for j in g]) + 'combined'

                new_data = np.concatenate([self.series[j].data for j in g], 1)
                # change:
                self.series[i].data = new_data
                self.series[i].slices = len(g)
                self.series[i].prepped_data = self.series[i].data
                self.series[i].name = new_name
                self.series[i].full_path = new_name
                self.series[i].series_folder_name = new_name

                self.series[i].view = 'SAX'
                # keep the same:
                self.series[i].pixel_spacing

                # Redefine affine matrix
                normal = [(self.series[g[-1]].image_positions[0][0] - self.series[g[0]].image_positions[0][0]) / (
                            self.series[i].slices - 1),
                          (self.series[g[-1]].image_positions[0][1] - self.series[g[0]].image_positions[0][1]) / (
                                      self.series[i].slices - 1),
                          (self.series[g[-1]].image_positions[0][2] - self.series[g[0]].image_positions[0][2]) / (
                                      self.series[i].slices - 1), ]

                affine_trafo = np.array([[self.series[g[0]].orientation[3] * self.series[g[0]].pixel_spacing[1],
                                          self.series[g[0]].orientation[0] * self.series[g[0]].pixel_spacing[2],
                                          normal[0], self.series[g[0]].image_positions[0][0]],
                                         [self.series[g[0]].orientation[4] * self.series[g[0]].pixel_spacing[1],
                                          self.series[g[0]].orientation[1] * self.series[g[0]].pixel_spacing[2],
                                          normal[1], self.series[g[0]].image_positions[0][1]],
                                         [self.series[g[0]].orientation[5] * self.series[g[0]].pixel_spacing[1],
                                          self.series[g[0]].orientation[2] * self.series[g[0]].pixel_spacing[2],
                                          normal[2], self.series[g[0]].image_positions[0][2]],
                                         [0, 0, 0, 1]])

                self.series[i].stack_orientation = self.series[g[0]].stack_orientation

                self.series[i].affine = affine_trafo
                self.series[i].affine_original = affine_trafo
                # combine:
                self.series[i].image_positions = [self.series[j].image_positions[0] for j in g]

        to_keep = [i for i in range(self.num_series) if i not in to_remove]

        # now we need to update the exam object:
        self.series = [self.series[i] for i in to_keep]
