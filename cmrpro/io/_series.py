__all__ = ["CMRSeries"]

import os
from typing import Tuple, List

import numpy as np
import pyvista as pv

import nibabel as nib
from nibabel.affines import apply_affine

import vtk
from vtk.util.numpy_support import numpy_to_vtk
from tqdm import tqdm

import cmrpro.io.utils

import scipy

class CMRSeries:
    """A class for representing a single cardiac magnetic resonance image series.
    A series is an image array with shape: (num_frames, num_slices, H, W).
    Thus, the series can be either 2D or multi-slice (~3D), and can be a single frame or multiple
    frames over time (cine).

    """
    #: Name of the series
    name: str
    #: Original image array : shape= (num_frames, num_slices, H, W)
    data: np.ndarray
    #: Size of pixels in each dimension : (rd, rh, rw)
    pixel_spacing: Tuple[float, float, float]
    #: 3D coordinates of all pixels : shape= (num_slices, H, W, 3)
    pix_coords: np.ndarray
    #: A string out of ['SAX', 'LAX', 'LAX_2CH', 'LAX_3CH', 'LAX_4CH', 'unknown']
    view: str
    #: Array of shape (6, ) containing two unit vectors defining the image plane
    orientation: np.ndarray
    #: (3, ) unit vector of the slice-normal
    image_positions: List
    #: list of slice positions in the stack
    stack_orientation: np.ndarray
    #: 4x4 matrix to affine transform image to 3D patient coordinates
    affine: np.ndarray
    #: Flag if segmentation was already performed
    seg: bool
    #: Corresponds to self.data.shape[0]
    frames: List
    #: Corresponds to self.data.shape[1]
    slices: List
    #flag if is a lgeSeries
    isLGESeries: bool
    #slice specific trigger times shape= (num_frames, num_slices)
    trigger_time: np.ndarray

    def __init__(self, name: str, data: np.ndarray, orientation: np.ndarray, image_positions:List,
                 stack_orientation: np.ndarray, affine_trafo: np.ndarray,
                 pixel_spacing: Tuple[float, float, float], view: str, isLGESeries: bool, trigger_time: np.ndarray):
        """
        :param name:
        :param data: (#frames, #slices, X, Y)
        :param orientation:
        :param affine_trafo: 4x4 affine transformation matrix
        :param pixel_spacing: (float, float) tuple of pixel dimension in mm
        :param view:
        """

        self.name = name
        self.data = data
        self.orientation = orientation
        self.image_positions = image_positions
        self.stack_orientation = stack_orientation
        self.affine = affine_trafo
        self.affine_original = affine_trafo
        self.pixel_spacing = pixel_spacing
        self.view = view
        self.seg = False
        self.frames, self.slices = self.data.shape[0:2]
        self.isLGESeries=isLGESeries
        self.trigger_time=trigger_time

    def __str__(self):
        details_str = (f"{self.name} ({self.view}), data shape: {self.data.shape}, "
                       + f"pixel spacing: {self.pixel_spacing}")
        # if self.prepped_data.shape != self.data.shape:
        #     details_str += f"(resampled to {self.prepped_data.shape}"
        if self.seg:
            details_str += ' (has been segmented)'
        return details_str

    def export_vti(self, target_folder: str):
        """
        exports image stack in world coordinates as vti and npy file with affine matrix (image to world coordinates)
        :param target_folder: str
        """

        vti_out = target_folder + '/' + self.name + '_VTI'
        os.makedirs(vti_out, exist_ok=True)

        data = np.transpose(self.data, (2,3,1,0))
        vti_image = vtk.vtkImageData()
        vti_image.SetDimensions(data.shape[:3])
        vti_image.SetSpacing((1.0, 1.0, 1.0))
        vti_image.SetOrigin(tuple(self.affine[:3, 3]))

        # This automatically allows to visualize the correct orientation in space,
        # but paraview has issues with the slicing.
        Img2World_vtk = vtk.vtkMatrix3x3()
        for i in range(3):
            for j in range(3):
                Img2World_vtk.SetElement(i, j, self.affine[i, j])
        vti_image.SetDirectionMatrix(Img2World_vtk)
        np.save(vti_out + '/Img2World_matrix4x4.npy', self.affine)

        for phase_sel in tqdm(range(data.shape[-1]), desc="Writing vti files"):
            # Add mask labels and store vti

            linear_array = data[...,phase_sel].reshape(-1, 1, order="F")
            vtk_array = numpy_to_vtk(linear_array)
            vtk_array.SetName('intensity')
            vti_image.GetPointData().AddArray(vtk_array)
            vti_image.Modified()

            writer = vtk.vtkXMLImageDataWriter()
            writer.SetFileName(vti_out + '/frame_%02d.vti' % (phase_sel))
            writer.SetInputData(vti_image)
            writer.Write()

    def get_preprocessed_data(self, res=2., sz=256, clip_percentile=99.5, normalize=True,
                              return_pixels_added=False, center=None,
                              check_plots=False):
        """preprocess the original image data from the series, returning the results at the desired
        size and resolution"""

        if sz is None:
            sz = self.data.shape[-1]
            print('No cropping dimensions specified, keeping original dimensions')

        half_sz = sz // 2

        # rescale to required resolution:
        resampled = scipy.ndimage.zoom(self.data,
                                       (1, 1, self.pixel_spacing[-2] / res,
                                        self.pixel_spacing[-1] / res),
                                       order=1)

        print('Series resampled to ', resampled.shape)
        # I need to modify the affine matrix with the new scaling function
        res2orig = np.array([[self.pixel_spacing[-2] / res, 0, 0, 0],
                             [0, self.pixel_spacing[-1] / res, 0, 0],
                             [0, 0, 1, 0],
                             [0, 0, 0, 1]])

        # crop/pad to desired size:
        resampled = np.pad(resampled, ((0, 0), (0, 0), (half_sz, half_sz), (half_sz, half_sz)))
        h, w = resampled.shape[2:]
        if center is None:
            ch, cw = h // 2, w // 2
        else:
            ch, cw = center
            ch = int(ch * (self.pixel_spacing[-2] / res) + half_sz)
            cw = int(cw * (self.pixel_spacing[-1] / res) + half_sz)

        resampled = resampled[:, :, ch - half_sz:ch + half_sz, cw - half_sz:cw + half_sz]
        print('Series resampled to ', resampled.shape)
        # clip large values:
        resampled = np.clip(resampled, 0, np.percentile(resampled, clip_percentile))

        if normalize:
            resampled = (resampled / (1.0 * resampled.max())).astype('float32')

        # calculate how many pixels have been added/removed:
        pixels_added = [[half_sz - (ch - half_sz), half_sz - (h - (ch + half_sz))],
                        [half_sz - (cw - half_sz), half_sz - (w - (cw + half_sz))]]

        # # Modify affine matrix to account for new position of original points (in m)
        res2orig[0, -1] += pixels_added[0][0] * res
        res2orig[1, -1] += pixels_added[1][0] * res

        affine = np.dot(self.affine_original, np.linalg.inv(res2orig))

        # Plot original image and resampled/cropped image in space to check if orientation and
        # position are preserved
        if check_plots:
            pv.set_plot_theme("document")
            plotter = pv.Plotter(off_screen=True)

            grid = cmrpro.io.utils.planeToGrid(self.data.shape[2:], self.slices // 2, self.affine_original)
            plotter.add_mesh(grid, scalars=self.data[0, self.slices // 2].T.flatten(order="F"),
                             opacity=0.5, cmap='jet')
            grid = cmrpro.io.utils.planeToGrid(resampled.shape[2:], self.slices // 2, self.affine)
            plotter.add_mesh(grid, scalars=resampled[0, self.slices // 2].T.flatten(order="F"),
                             opacity=1.0, cmap='gray')
            if self.slices > 2:
                grid = cmrpro.io.utils.planeToGrid(self.data.shape[2:], self.slices // 2 + 1, self.affine_original)
                plotter.add_mesh(grid,
                                 scalars=self.data[0, self.slices // 2 + 1].T.flatten(order="F"),
                                 opacity=0.5, cmap='jet')

                grid = cmrpro.io.utils.planeToGrid(self.data.shape[2:], self.slices // 2 - 1, self.affine_original)
                plotter.add_mesh(grid,
                                 scalars=self.data[0, self.slices // 2 - 1].T.flatten(order="F"),
                                 opacity=0.5, cmap='jet')

                grid = cmrpro.io.utils.planeToGrid(resampled.shape[2:], self.slices // 2 + 1, self.affine)
                plotter.add_mesh(grid,
                                 scalars=resampled[0, self.slices // 2 + 1].T.flatten(order="F"),
                                 opacity=0.5, cmap='gray')
                #
                grid = cmrpro.io.utils.planeToGrid(resampled.shape[2:], self.slices // 2 - 1, self.affine)
                plotter.add_mesh(grid,
                                 scalars=resampled[0, self.slices // 2 - 1].T.flatten(order="F"),
                                 opacity=0.5, cmap='gray')

            plotter.show(screenshot=os.getcwd()+'/Resizing_check.png')

        # plotter = pv.Plotter()

        coordinatesXYZ_resampled = np.zeros((self.slices, resampled.shape[2], resampled.shape[3], 3))
        print('This io has %d slices' % self.slices)
        for n in range(self.slices):
            coordinatesXYZ_resampled[n, ..., 0], coordinatesXYZ_resampled[n, ..., 1], coordinatesXYZ_resampled[n, ..., 2], = \
                cmrpro.io.utils.planeToXYZ([resampled.shape[2], resampled.shape[3]], n, self.affine)

        # convert to a single stack of 1 channel images (i.e. ready to use as input for a network):
        resampled_images = resampled.reshape((-1, 1, sz, sz))
        if return_pixels_added:
            return resampled_images, coordinatesXYZ_resampled, pixels_added, affine
        else:
            return resampled_images, coordinatesXYZ_resampled, affine

    @staticmethod
    def guess_view_from_folder_name(folder_name: str, data_shape: Tuple[int, int, int]):
        """sets the series' view property to one of: [SAX, 2CH, 3CH, 4CH, LAX, unknown] by
        checking if folder_name contains certain keywords
        """

        # try to guess the view from series name:
        if 'sax' in folder_name.lower() or 'sa' in folder_name[4:].lower():
            view = 'SAX'
        elif '2ch' in folder_name.lower():
            view = 'LAX_2CH'
        elif '3ch' in folder_name.lower():
            view = 'LAX_3CH'
        elif '4ch' in folder_name.lower():
            view = 'LAX_4CH'
        elif 'sa' in folder_name.lower():
            view = 'SAX'
        elif any(key in folder_name.lower() for key in ["_la", "la_", "lax", "la"]):
            view = "LAX"
        else:
            view = 'unknown'
        return view

    @staticmethod
    def guess_isLGE_from_folder_name(folder_name: str):
        '''
        descides if the series is a LGE series (true if 'Viab'/'viab' or 'lge')
        needs to be extended if naming convention changes
        '''
        if 'Viab' in folder_name or 'viab' in folder_name or 'lge' in folder_name:
            isLGE=True
        else:
            isLGE=False
        return isLGE

    @classmethod
    def from_file(cls, full_path: str, details: dict = None) -> 'CMRSeries':
        '''
        constructs a series object based on data calling a separate function depending on the input format (multiple input data formats possible: .nii; par/rec; DICOM)
        '''
        files_inside = cmrpro.io.utils.sorted_nicely(os.listdir(full_path))

        if files_inside[0].endswith('nii.gz') or files_inside[0].endswith('.nii'):
            obj = CMRSeries.from_nifty(full_path+'/'+files_inside[0])
        elif files_inside[0].endswith('.par') or files_inside[0].endswith('.rec'):
            print('loading series from par/rec files')
            obj = CMRSeries.from_parrec(os.path.join(full_path, files_inside[0]))
        else:
            try:
                lstFilesDCM = cmrpro.io.utils.get_sorted_filenames(full_path)
                if len(lstFilesDCM)>1:
                    obj = CMRSeries.from_multi_dicom(full_path)
                else:
                    obj = CMRSeries.from_single_dicom(lstFilesDCM[0])
                print('loading series from dicom file/folder')
            except:
                raise ValueError("Invalid file path")
        return obj

        # if files_inside[0].endswith('nii.gz') or files_inside[0].endswith('.nii'):
        #     obj = CMRSeries.from_nifty(full_path+'/'+files_inside[0])
        # elif files_inside[0].endswith('.npy'):
        #     if details is None:
        #         details = {}
        #     raise NotImplemented("npy file reader not implemented yet")
        # elif files_inside[0].endswith('.vti'):
        #     raise NotImplemented("vti file reader not implemented yet")
        # elif files_inside[0].endswith('.par') or files_inside[0].endswith('.rec'):
        #     print('loading series from par/rec files')
        #     obj = CMRSeries.from_parrec(os.path.join(full_path, files_inside[0]))
        # else:
        #     print('loading series from dicom file/folder')
        #     try:
        #         lstFilesDCM = cmrpro.io.utils.get_sorted_filenames(full_path)
        #         if len(lstFilesDCM)>1:
        #             obj = CMRSeries.from_multi_dicom(full_path)
        #         else:
        #             obj = CMRSeries.from_single_dicom(lstFilesDCM[0])
        #     except:
        #         raise ValueError("Invalid file path")
        # return obj

    @classmethod
    def from_multi_dicom(cls, full_path: str) -> 'CMRSeries':
        """Scans the provided folder for subdirectories containing DICOM files and creates a
        CMRSeries object from all of them

        :param full_path: str
        :return:
        """

        # lstFilesDCM = cmrpro.io.utils.get_sorted_filenames(full_path)
        print('Reading from multi-dicom')
        details = cmrpro.io.utils.data_array_from_dicom_folder(full_path)
        details = {k: v for k, v in zip(["data", "pixel_spacing", "image_ids", "dicom_details",
                                         "slice_location", "trigger_time_specific", "image_positions",
                                         "is3D", "multifile"], details)}

        image_positions = details["image_positions"]
        orientation = np.array(list(details["dicom_details"]['ImageOrientation']))
        data = details['data']
        image_pos_patient, image_pos_patient_n = image_positions[0], image_positions[-1]

        details["slices"] = details["data"].shape[1]

        image_ori_patient = np.array([[float(orientation[3]), float(orientation[0])],
                                      [float(orientation[4]), float(orientation[1])],
                                      [float(orientation[5]), float(orientation[2])]])
        pixel_spacing = [float(details["pixel_spacing"][1]), float(details["pixel_spacing"][2])]
        slice_thickness = float(details["pixel_spacing"][0])

        if details["slices"] > 1:
            normal = [(image_pos_patient_n[0] - image_pos_patient[0]) / (details["slices"] - 1),
                      (image_pos_patient_n[1] - image_pos_patient[1]) / (details["slices"] - 1),
                      (image_pos_patient_n[2] - image_pos_patient[2]) / (details["slices"] - 1), ]
        else:
            normal = np.cross(np.array(image_ori_patient[:, 0]), np.array(image_ori_patient[:, 1]))
            normal *= slice_thickness
            if normal[2]<0: # this checks if the normal is in the feet-head direction pointing towards head
                normal[0] *= -1
                normal[1] *= -1
                normal[2] *= -1

        p0 = image_ori_patient[:, 0] * pixel_spacing[0]
        p1 = image_ori_patient[:, 1] * pixel_spacing[1]
        affine_trasform = np.array([[p0[0], p1[0], normal[0], image_pos_patient[0]],
                                 [p0[1], p1[1], normal[1], image_pos_patient[1]],
                                 [p0[2], p1[2], normal[2], image_pos_patient[2]],
                                 [0, 0, 0, 1]])

        stack_orientation = normal / np.linalg.norm(normal)

        view = CMRSeries.guess_view_from_folder_name(full_path, details["data"].shape)
        isLGESeries = CMRSeries.guess_isLGE_from_folder_name(full_path)
        name = os.path.basename(full_path).lower().split(".")[0]
        pixel_spacing = (slice_thickness, pixel_spacing[0], pixel_spacing[1])
        return CMRSeries(name=name, data=data, orientation=orientation,
                         stack_orientation=stack_orientation, image_positions=image_positions,
                         affine_trafo=affine_trasform, pixel_spacing=pixel_spacing, view=view,
                         isLGESeries=isLGESeries,trigger_time=details["trigger_time_specific"])

    @classmethod
    def from_single_dicom(cls, full_path: str) -> 'CMRSeries':
        """loads a series from DICOM file/folder"""
        from pydicom import dcmread
        from pydicom.tag import Tag
        import re

        def _extractNumbers(string):
            return re.findall(r"[-+]?[.]?[\d]+(?:,\d\d\d)*[\.]?\d*(?:[eE][-+]?\d+)?", string)

        def _search_dataset(ds, tag, info_found):
            # Thanks to Martin Buhrer GyroTools
            for elem in ds:
                if elem.VR == "SQ":
                    for item in elem:
                        info_found = _search_dataset(item, tag, info_found)
                if elem.tag == tag:
                    info_found.append(str(elem))
            return info_found

        def _search_number_of_slices(ds):
            for elem in ds:
                if elem.VR == "SL" and 'Number of Slices MR' in str(elem):
                    return int(_extractNumbers(str(elem))[-1])

        ds = dcmread(full_path)
        try:
            data = ds.pixel_array
        except:
            print("Data might be compressed, decompressing it")
            ds.decompress()
            data = ds.pixel_array

        rescale_slope = float(_extractNumbers(_search_dataset(ds, Tag('RescaleSlope'), [])[0])[-1])
        rescale_intercept = float(
            _extractNumbers(_search_dataset(ds, Tag('RescaleIntercept'), [])[0])[-1])
        data = rescale_intercept + rescale_slope * data

        n_slices = _search_number_of_slices(ds)
        cardiacPhases = data.shape[0] // n_slices

        print(f'Found {cardiacPhases} cardiac phases and {n_slices} slices\n'
              f'Data format is {data.shape}')

        if n_slices == 1:  # assumes it is a single slice
            data = np.expand_dims(data, axis=1)
        elif cardiacPhases == 1:  # assumes it is a single frame
            data = np.expand_dims(data, axis=0)
        else:
            data = data.reshape(cardiacPhases, n_slices, data.shape[-2], data.shape[-1], order='F')

        # Parse pixel-spacing
        if n_slices > 1:
            slice_thickness = float(
                _extractNumbers(_search_dataset(ds, Tag(0x18, 0x50), [])[0])[-1])
        else:
            slice_thickness = float(
                _extractNumbers(_search_dataset(ds, Tag(0x18, 0x88), [])[0])[-1])
        pixel_spacings_ = _extractNumbers(_search_dataset(ds, Tag('PixelSpacing'), [])[0])[-2:]
        pixel_spacing = [float(pixel_spacings_[0]), float(pixel_spacings_[1])]
        pixel_spacing = (slice_thickness, pixel_spacing[0], pixel_spacing[1])

        image_pos_patient_ = _extractNumbers(_search_dataset(ds, Tag(0x20, 0x32), [])[0])[-3:]
        image_pos_patient = [float(image_pos_patient_[0]), float(image_pos_patient_[1]),
                             float(image_pos_patient_[2])]

        image_pos_patient_n_ = _extractNumbers(_search_dataset(ds, Tag(0x20, 0x32), [])[-1])[-3:]
        image_pos_patient_n = [float(image_pos_patient_n_[0]), float(image_pos_patient_n_[1]),
                               float(image_pos_patient_n_[2])]

        image_ori_patient_ = _extractNumbers(_search_dataset(ds, Tag(0x20, 0x37), [])[0])[2:]
        image_ori_patient = np.array([[float(image_ori_patient_[3]), float(image_ori_patient_[0])],
                                      [float(image_ori_patient_[4]), float(image_ori_patient_[1])],
                                      [float(image_ori_patient_[5]), float(image_ori_patient_[2])]])

        if n_slices > 1:
            normal = [(image_pos_patient_n[0] - image_pos_patient[0]) / (n_slices - 1),
                      (image_pos_patient_n[1] - image_pos_patient[1]) / (n_slices - 1),
                      (image_pos_patient_n[2] - image_pos_patient[2]) / (n_slices - 1), ]
        else:
            normal = np.cross(np.array(image_ori_patient[:, 0]), np.array(image_ori_patient[:, 1]))
            if normal[2]<0: # this checks if the normal is in the feet-head direction pointing towards head
                normal *= -1
            normal *= slice_thickness

        p0 = image_ori_patient[:, 0] * pixel_spacing[1]
        p1 = image_ori_patient[:, 1] * pixel_spacing[2]

        affine_trafo = np.array([[p0[0], p1[0], normal[0], image_pos_patient[0]],
                                 [p0[1], p1[1], normal[1], image_pos_patient[1]],
                                 [p0[2], p1[2], normal[2], image_pos_patient[2]],
                                 [0, 0, 0, 1]])

        ax1 = apply_affine(affine_trafo, [0, 1, 0]) - apply_affine(affine_trafo, [0, 0, 0])
        ax1 /= np.linalg.norm(ax1)
        ax2 = apply_affine(affine_trafo, [1, 0, 0]) - apply_affine(affine_trafo, [0, 0, 0])
        ax2 /= np.linalg.norm(ax2)
        image_orientation = np.concatenate([ax1, ax2])

        trigger_time_ = _search_dataset(ds, Tag('TriggerTime'), [])
        trigger_time=[]
        for i in range(len(trigger_time_)):
            trigger_time.append(float(_extractNumbers(trigger_time_[i])[-1]))
        trigger_time=np.transpose(np.reshape(trigger_time,(data.shape[1],data.shape[0])))

        view = CMRSeries.guess_view_from_folder_name(full_path, data.shape)
        isLGESeries = CMRSeries.guess_isLGE_from_folder_name(full_path)
        name = os.path.split(os.path.dirname(full_path))[1] #os.path.basename(full_path).lower().split(".")[0]
        stack_orientation = normal
        return CMRSeries(name=name, data=data, orientation=image_orientation,
                         image_positions=[image_pos_patient_],
                         stack_orientation=stack_orientation, affine_trafo=affine_trafo,
                         pixel_spacing=pixel_spacing, view=view, isLGESeries=isLGESeries, trigger_time=trigger_time)
    @classmethod
    def from_parrec(cls, full_path: str)-> 'CMRSeries':
        '''
        builds a series object from par/rec data
        '''
        loadedImg = nib.load(full_path)
        imgInfo = loadedImg.header
        trigger_time=imgInfo.get_def('trigger_time')

        data = loadedImg.get_fdata()
        if len(data.shape) == 4:
            data = np.array(np.transpose(data, (3, 2, 0, 1)), dtype='float64')
            trigger_time=np.transpose(np.reshape(trigger_time,(data.shape[1],data.shape[0])))
        else:
            # Takes care of data without temporal dimension
            data = np.expand_dims(np.transpose(data,(2,0,1)),0)

        pixel_spacing_original = imgInfo.get_zooms()[0:3]
        pixel_spacing = (pixel_spacing_original[2], pixel_spacing_original[0],
                         pixel_spacing_original[1])
        affine = imgInfo.get_affine()

        image_positions = [apply_affine(affine, [0, 0, k]) for k in range(data.shape[1])]

        ax1 = apply_affine(affine, [0, 1, 0]) - apply_affine(affine, [0, 0, 0])
        ax1 = ax1 / np.sum(ax1 ** 2) ** 0.5
        ax2 = apply_affine(affine, [1, 0, 0]) - apply_affine(affine, [0, 0, 0])
        ax2 = ax2 / np.sum(ax2 ** 2) ** 0.5
        orientation = np.concatenate([ax1, ax2])
        stack_orientation = image_positions[-1] - image_positions[0]
        if np.linalg.norm(stack_orientation)>0:
            stack_orientation /= np.linalg.norm(stack_orientation)
        else:
            stack_orientation = np.cross(ax1,ax2)

        view = CMRSeries.guess_view_from_folder_name(full_path, data.shape)

        isLGESeries = CMRSeries.guess_isLGE_from_folder_name(full_path)
        name = os.path.split(os.path.dirname(full_path))[1]#os.path.basename(full_path).lower().split(".")[0]
        return CMRSeries(name=name, data=data, orientation=orientation,
                         image_positions=image_positions,
                         stack_orientation=stack_orientation, affine_trafo=affine,
                         pixel_spacing=pixel_spacing, view=view,isLGESeries=isLGESeries,trigger_time=trigger_time)

    @classmethod
    def from_nifty(cls, full_path: str)-> 'CMRSeries':
        """loads a series from a nifti file"""
        img = nib.load(full_path)
        hdr = img.header

        data = np.transpose(img.get_fdata(), (3, 2, 0, 1))
        pixel_spacing = (hdr.get('pixdim')[3], hdr.get('pixdim')[1], hdr.get('pixdim')[2])
        slice_locations = [0 for k in range(data.shape[1])]
        image_positions = [apply_affine(img.affine, [0, 0, k]) for k in range(data.shape[1])]
        stack_orientation = image_positions[-1] - image_positions[0]
        stack_orientation /= np.linalg.norm(stack_orientation)

        affine = img.affine  # This affine is referred to the original orientation
        ax1 = apply_affine(affine, [0, 1, 0]) - apply_affine(affine, [0, 0, 0])
        ax1 = ax1 / np.sum(ax1 ** 2) ** 0.5
        ax2 = apply_affine(affine, [1, 0, 0]) - apply_affine(affine, [0, 0, 0])
        ax2 = ax2 / np.sum(ax2 ** 2) ** 0.5
        orientation = np.concatenate([ax1, ax2])
        view = CMRSeries.guess_view_from_folder_name(full_path, data.shape)
        isLGESeries = CMRSeries.guess_isLGE_from_folder_name(full_path)
        name = os.path.split(os.path.dirname(full_path))[1]#os.path.basename(full_path).lower().split(".")[0] + 'sax_stack'

        return CMRSeries(name=name, data=data, orientation=orientation,
                         image_positions=image_positions,
                         stack_orientation=stack_orientation, affine_trafo=affine,
                         pixel_spacing=pixel_spacing, view=view,isLGESeries=isLGESeries,trigger_time=None)

    @classmethod
    def from_npy(cls, full_path: str, details: {}, affine: np.ndarray)-> 'CMRSeries':
        """loads a series from a numpy array"""
        # assume 10mm spacing, if not given
        slice_spacing = details.get('slice_spacing', default=10.)
        # assume 1mm x 1mm image resolution, if not given
        pixel_size = details.get('pixel_size', default=1.)

        data = np.load(full_path)  # [...,0]
        pixel_spacing = (slice_spacing, pixel_size, pixel_size)
        slice_locations = [0 for k in range(data.shape[1])]
        image_positions = [[0, 0, k * pixel_spacing[0]] for k in range(data.shape[1])]
        stack_orientation = image_positions[-1] - image_positions[0]
        stack_orientation /= np.linalg.norm(stack_orientation)

        orientation = np.array([0, 1, 0, 1, 0, 0])  # set arbitrary orientation
        view = CMRSeries.guess_view_from_folder_name(full_path, data.shape)

        isLGESeries = CMRSeries.guess_isLGE_from_folder_name(full_path)
        name = os.path.split(os.path.dirname(full_path))[1]#os.path.basename(full_path).lower().split(".")[0] + 'sax_stack'
        return CMRSeries(name=name, data=data, orientation=orientation,
                         image_positions=image_positions,
                         stack_orientation=stack_orientation, affine_trafo=affine,
                         pixel_spacing=pixel_spacing, view=view,isLGESeries=isLGESeries,trigger_time=None)

    def combine_series(self, ):
        '''
        Method to combine SAX slices from the same stack that are saved in different exams
        '''

        to_remove = []
        groups    = []
        for i in range(self.num_series):
            i_grouped = False
            for j, g in enumerate(groups):
                if np.array_equal(self.series[i].orientation, self.series[g[0]].orientation):
                    if self.series[i].slices == 1:  # if there is more than 1 slice I assume there is no need to group them
                        groups[j].append(i)
                        i_grouped = True
                        break
            if not i_grouped:
                groups.append([i])

        groups = [np.array(g) for g in groups]

        # now work out the order to combine them:
        for g in groups:
            if len(g) > 1:
                # make sure they all match (keep the largest subset that share a data shape):
                all_shapes = []
                for j in g:
                    all_shapes.append(str(self.series[j].data.shape))
                unique, counts = np.unique(all_shapes, return_counts=True)
                target_shape = unique[np.argmax(counts)]
                new_g = []
                for j in g:
                    if target_shape == str(self.series[j].data.shape):
                        new_g.append(j)
                g = new_g

                # Slices need to be ordered based on their position, otherwise they could be in random order
                # using the position in space projected onto the normal of the reference one would provide the ordering
                # TODO: how do we prescribe the right stack orientation? I cant be sure it is the correct one
                rel_positions = [0]
                ref_direction = self.series[g[0]].stack_orientation/np.linalg.norm(self.series[g[0]].stack_orientation)

                for i in g[1:]:
                    rel_pos = np.array(self.series[i].image_positions[0])-np.array(self.series[g[0]].image_positions[0])
                    rel_positions.append(np.dot(rel_pos,ref_direction))

                ordering_slices = np.argsort(np.array(rel_positions))
                gnew = []
                for k in ordering_slices:
                    gnew.append(g[k])
                g = gnew
                # make the first series the new combined series:
                i = g[0]
                to_remove = to_remove + g[1:]

                new_name = cmrpro.io.utils.long_substr([self.series[j].name for j in g]) + 'combined'

                new_data = np.concatenate([self.series[j].data for j in g], 1)
#                new_stack_orientation = self.series[g[0]].stack_orientation #np.array(self.series[g[-1]].image_positions[0])-np.array(self.series[g[0]].image_positions[0])
                # change:
                self.series[i].data = new_data
                self.series[i].slices = len(g)
                self.series[i].prepped_data = self.series[i].data
                self.series[i].name      = new_name
                self.series[i].full_path = new_name
                self.series[i].series_folder_name = new_name

#                self.series[i].stack_orientation = None #new_stack_orientation
                self.series[i].view = 'SAX'
                # keep the same:
                self.series[i].pixel_spacing

                #Redefine affine matrix
                normal = [(self.series[g[-1]].image_positions[0][0] - self.series[g[0]].image_positions[0][0]) / (self.series[i].slices  - 1),
                          (self.series[g[-1]].image_positions[0][1] - self.series[g[0]].image_positions[0][1]) / (self.series[i].slices  - 1),
                          (self.series[g[-1]].image_positions[0][2] - self.series[g[0]].image_positions[0][2]) / (self.series[i].slices  - 1), ]
                #if normal[2]<0:
                #    normal *= -1

                affine_trafo = np.array([[self.series[g[0]].orientation[3]*self.series[g[0]].pixel_spacing[1], self.series[g[0]].orientation[0]*self.series[g[0]].pixel_spacing[2], normal[0], self.series[g[0]].image_positions[0][0]],
                                         [self.series[g[0]].orientation[4]*self.series[g[0]].pixel_spacing[1], self.series[g[0]].orientation[1]*self.series[g[0]].pixel_spacing[2], normal[1], self.series[g[0]].image_positions[0][1]],
                                         [self.series[g[0]].orientation[5]*self.series[g[0]].pixel_spacing[1], self.series[g[0]].orientation[2]*self.series[g[0]].pixel_spacing[2], normal[2], self.series[g[0]].image_positions[0][2]],
                                         [0, 0, 0, 1]])
#                self.series[i].affine_original[0,2] = new_stack_orientation[0]#normal[0]
#                self.series[i].affine_original[1,2] = new_stack_orientation[1]#normal[1]
#                self.series[i].affine_original[2,2] = new_stack_orientation[2]#normal[2]
                self.series[i].stack_orientation = self.series[g[0]].stack_orientation

                self.series[i].affine = affine_trafo
                self.series[i].affine_original = affine_trafo
                # combine:
                self.series[i].image_positions = [self.series[j].image_positions[0] for j in g]

        to_keep = [i for i in range(self.num_series) if i not in to_remove]

        # now we need to update the exam object:
        self.series = [self.series[i] for i in to_keep]
