import os
import numpy as np
import pyvista as pv
import cmrpro.io

import matplotlib.pyplot as plt

def mask_images(patient_folder,out_folder=None,img_folders=None,phases_ids=None):
    """ Masks given image folders with dynamic meshes from mesh_folder
    Images are transformed to WorldCoordinates (the affine matrix should be available)
    Args
        patient_folder: folder with patient data
        out_folder: output folder for storage of masked images
        img_folders: list of folders with images to be masked, if None all folders ending with _VTI will be considered
        phases_ids: selection of phases to consider, all used if None
    """
    mesh_folder = patient_folder + '/meshes_WorldCoordinates/'
    if img_folders is None:
        cine_avail_ = np.sort(next(os.walk(patient_folder))[1])
        img_folders = []
        for sel_cine in cine_avail_:
            if sel_cine[-4:] == '_VTI':
                img_folders.append(patient_folder+'/'+sel_cine)
    else:
        if not hasattr(img_folders, "__len__"):
            img_folders = [img_folders]
    print('Using folders', img_folders)

    if out_folder is None:
        out_folder = patient_folder
    all_phases = np.sort(next(os.walk(mesh_folder))[2])

    if phases_ids is None:
        phases = all_phases
    else:
        phases = all_phases[phases_ids]

    for sel_cine in img_folders:

        print('Masking ',sel_cine)
        affine_matrix = np.load(sel_cine + '/Img2World_matrix4x4.npy')

        for kphase, phase_file in enumerate(phases):
            mesh_file = mesh_folder + '/' + phase_file

            # Read segmentation mesh and convert to surface file
            mesh = pv.read(mesh_file)
            surf_mesh = mesh.extract_surface(nonlinear_subdivision=5)

            # Read image data and dimensions
            img = pv.read(sel_cine + '/' + phase_file.rstrip('k')+'i')
            img_dims = img.dimensions
            masked_img = np.zeros(img_dims)

            for sel_plane in range(img_dims[2]):
                plane = cmrpro.io.utils.planeToGrid(img_dims[:2], sel_plane, affine_matrix)
                masked_img[..., sel_plane] = plane.select_enclosed_points(surf_mesh, check_surface=True, )[
                    'SelectedPoints'].reshape(img_dims[:2], order='F')
            img_phase = img.get_array(0).reshape(img_dims, order='F')

            if kphase == 0:
                Img4D = np.expand_dims(img_phase, 0)
                Mask4D = np.expand_dims(masked_img, 0)
            else:
                Img4D = np.concatenate((Img4D, np.expand_dims(img_phase, 0)), 0)
                Mask4D = np.concatenate((Mask4D, np.expand_dims(masked_img, 0)), 0)

            try:
                if kphase == 0:
                    xLV, yLV, _ = np.where(masked_img == 1)
                    center_img = [int(np.mean(xLV)), int(np.mean(yLV))]
                    box_size = np.min([np.max([xLV.max() - xLV.min(), yLV.max() - yLV.min()]), img_dims[0]]) + 10

                if box_size != img_dims[0]:
                    cropped_mask = masked_img[center_img[0] - box_size // 2:center_img[0] + box_size // 2,
                                  center_img[1] - box_size // 2:center_img[1] + box_size // 2, :]

                    cropped_img = img.get_array(0).reshape(img_dims, order='F')[
                                        center_img[0] - box_size // 2:center_img[0] + box_size // 2,
                                        center_img[1] - box_size // 2:center_img[1] + box_size // 2, :]
                else:
                    cropped_mask = masked_img
                    cropped_img = img.get_array(0).reshape(img_dims, order='F')

                for sel_plane in range(img_dims[2]):
                    if sel_plane == 0:
                        img_int = cropped_img[...,sel_plane]
                        img_msk = cropped_mask[...,sel_plane]
                    else:
                        img_int = np.concatenate((img_int, cropped_img[...,sel_plane]), 0)
                        img_msk = np.concatenate((img_msk, cropped_mask[...,sel_plane]), 0)

                if kphase == 0:
                    Img4plot  = img_int
                    Mask4plot = img_msk
                else:
                    Img4plot  = np.concatenate((Img4plot, img_int), 1)
                    Mask4plot = np.concatenate((Mask4plot, img_msk),1)

            except:
                dmmy = 0

        np.save(out_folder + '/Img4D_%s.npy' %(os.path.basename(os.path.normpath(sel_cine))),Img4D )
        np.save(out_folder + '/LVMask4D_%s.npy' %(os.path.basename(os.path.normpath(sel_cine))),Mask4D )

        try:
            Mask4plot = np.ma.masked_where(Mask4plot < 1, Mask4plot)
            plt.imshow(Img4plot, cmap=plt.cm.gray)
            plt.imshow(Mask4plot, vmin=0, vmax=1, alpha=0.3)
            plt.axis('off')
            plt.savefig(out_folder + '/Masked_%s.png' %(os.path.basename(os.path.normpath(sel_cine))), bbox_inches='tight', dpi=800)
            plt.close()
        except:
            dmmy=0

    return 0

def getSAXStrains(patient_folder):

    from scipy.interpolate import interp1d

    files_available = np.sort(next(os.walk(patient_folder))[2])
    SAXmasks_available = []

    for file_sel in files_available:
        if 'LVMask4D' in file_sel and file_sel.endswith('.npy') and 'sax' in file_sel:
            SAXmasks_available.append(file_sel)
    print()
    if len(SAXmasks_available) == 1:
        mask4D = np.load(patient_folder+'/'+SAXmasks_available[0])
        radial, circ = computeSAXStrainsFromMask(mask4D)
    else:
        # interpolate along phase and then average
        radial_ = []
        circ_   = []
        n_phases_max = 0
        for sel_mask in SAXmasks_available:
            mask4D = np.load(patient_folder+'/'+sel_mask)
            radial, circ = computeSAXStrainsFromMask(mask4D)
            n_phases_max = int(np.max([n_phases_max,radial[:,0].max(),circ[:,0].max()]))
            radial_.append(radial)
            circ_.append(circ)
        radial = np.zeros((n_phases_max,2))
        circ   = np.zeros((n_phases_max, 2))

        for kk in range(len(radial_)):
            f = interp1d(radial_[kk][:,0], radial_[kk][:,1])
            radial[:,1] = f(np.arange(n_phases_max))
            f = interp1d(circ_[kk][:, 0], circ_[kk][:, 1])
            circ[:, 1] = f(np.arange(n_phases_max))
        radial[:,0] = np.arange(n_phases_max)
        radial[:,1] /= kk
        circ[:,0] = np.arange(n_phases_max)
        circ[:,1] /= kk

    ff = open(patient_folder + '/Radial_maskStrains.txt', 'w')
    for phase_sel in range(radial.shape[0]):
        ff.write('%d %1.4f\n' % (radial[phase_sel, 0],radial[phase_sel, 1]))
    ff.close()

    ff = open(patient_folder + '/Circ_maskStrains.txt', 'w')
    for phase_sel in range(radial.shape[0]):
        ff.write('%d %1.4f\n' % (circ[phase_sel, 0],circ[phase_sel, 1]))
    ff.close()

    plt.plot(radial[:,0],radial[:,1], 'b', label=' mean radial (max=%s)' % (radial[2:-3,1].max(),))
    plt.plot(circ[:,0],circ[:,1], 'r', label='mean circ (max=%s)' % (circ[2:-3,1].min(),))
    plt.legend()
    plt.xlabel('time frame')
    plt.ylabel('strain')

    plt.savefig(patient_folder + '/MaskStrains.png', dpi=100)
    plt.close()

    return 0

def computeSAXStrainsFromMask(mask4D,target_points=25):

    from scipy.ndimage import binary_fill_holes
    import cv2
    from scipy.ndimage import center_of_mass as com
    from scipy.interpolate import interp1d

    # phase index is 0
    # slice index is 3
    img_dims = mask4D.shape

    thickness = np.zeros((img_dims[0],img_dims[-1],target_points))
    radius    = np.zeros((img_dims[0],img_dims[-1],target_points))
    ref_theta = np.linspace(-np.pi * 0.8, np.pi * 0.8, target_points)

    for phase_sel in range(img_dims[0]):

        current_mask = mask4D[phase_sel,...]

        for slice_sel in range(img_dims[-1]):

            myo = current_mask[...,slice_sel]
            if np.sum(myo) == 0:
                continue

            filled = 1 * binary_fill_holes(myo)

            # Check if there is a closed blood pool or not. If LV mask is not closed, the algorithm returns the input
            if np.sum(filled-myo) == 0 or slice_sel < 2 or slice_sel > img_dims[0]-2:
                thickness[phase_sel,slice_sel] = -10
                radius[phase_sel,slice_sel]    = -10
                continue

            bpool = np.multiply(1 - myo, filled)

            if np.sum(bpool)==0:
                thickness[phase_sel,slice_sel] = -10
                radius[phase_sel,slice_sel]    = -10
                continue

            cy, cx = com(myo)

            epiContour, _ = cv2.findContours(filled.astype(np.uint8), 2, 1)
            epiContour    = np.array(epiContour[0]).reshape(-1, 2)

            anglesEpi = np.zeros((epiContour.shape[0], 1))
            radiusEpi = np.zeros((epiContour.shape[0], 1))

            for i in range(epiContour.shape[0]):
                relx = float(epiContour[i, 0] - cx)
                rely = float(epiContour[i, 1] - cy)
                anglesEpi[i, 0] = np.arctan2(rely, relx)
                radiusEpi[i, 0] = np.sqrt(relx ** 2 + rely ** 2)
            anglesEpiPeriodic = np.concatenate((anglesEpi - 2 * np.pi, anglesEpi, anglesEpi + 2 * np.pi))
            radiusEpiPeriodic = np.concatenate((radiusEpi, radiusEpi, radiusEpi))
            f = interp1d(anglesEpiPeriodic[:, 0], radiusEpiPeriodic[:, 0])
            radiusEpiresampled = f(ref_theta)
            epiPoints = np.zeros((target_points, 2))
            for i in range(target_points):
                epiPoints[i, :] = [radiusEpiresampled[i] * np.cos(ref_theta[i]) + cx,
                                   radiusEpiresampled[i] * np.sin(ref_theta[i]) + cy]

            endoContour, _ = cv2.findContours(bpool.astype(np.uint8), 2, 1)
            endoContour    = np.array(endoContour[0]).reshape(-1, 2)
            anglesEndo = np.zeros((endoContour.shape[0], 1))
            radiusEndo = np.zeros((endoContour.shape[0], 1))
            for i in range(endoContour.shape[0]):
                relx = float(endoContour[i, 0] - cx)
                rely = float(endoContour[i, 1] - cy)
                anglesEndo[i, 0] = np.arctan2(rely, relx)
                radiusEndo[i, 0] = np.sqrt(relx ** 2 + rely ** 2)
            anglesEndoPeriodic = np.concatenate((anglesEndo - 2 * np.pi, anglesEndo, anglesEndo + 2 * np.pi))
            radiusEndoPeriodic = np.concatenate((radiusEndo, radiusEndo, radiusEndo))
            f = interp1d(anglesEndoPeriodic[:, 0], radiusEndoPeriodic[:, 0])
            radiusEndoresampled = f(ref_theta)
            endoPoints = np.zeros((target_points, 2))
            for i in range(target_points):
                endoPoints[i, :] = [radiusEndoresampled[i] * np.cos(ref_theta[i]) + cx,
                                    radiusEndoresampled[i] * np.sin(ref_theta[i]) + cy]
            #
            thickness[phase_sel,slice_sel,:] = np.linalg.norm(epiPoints - endoPoints,1)
            radius[phase_sel,slice_sel,:]    = np.linalg.norm(endoPoints-np.array([cx,cy]),1)

    radial_strain = -10*np.ones((img_dims[0],img_dims[-1],target_points))
    circ_strain   = -10*np.ones((img_dims[0],img_dims[-1],target_points))

    for phase_sel in range(img_dims[0]):
        for slice_sel in range(img_dims[-1]):
            for point_sel in range(target_points):
                if thickness[0,slice_sel,point_sel] == 0 or thickness[0,slice_sel,point_sel] == -10:
                    radial_strain[:,slice_sel,point_sel] = -10
                    continue
                if thickness[phase_sel,slice_sel,point_sel] == -10:
                    radial_strain[phase_sel, slice_sel, point_sel] = -10
                    continue
                radial_strain[phase_sel,slice_sel,point_sel] = thickness[phase_sel,slice_sel,point_sel]/thickness[0,slice_sel,point_sel]-1

    for phase_sel in range(img_dims[0]):
        for slice_sel in range(img_dims[-1]):
            for point_sel in range(target_points):
                if radius[0, slice_sel, point_sel] == 0 or radius[0, slice_sel, point_sel] == -10:
                    circ_strain[:, slice_sel, point_sel] = -10
                    continue
                if radius[phase_sel, slice_sel, point_sel] == -10:
                    circ_strain[phase_sel, slice_sel, point_sel] = -10
                    continue
                circ_strain[phase_sel, slice_sel, point_sel] = radius[phase_sel, slice_sel, point_sel] / radius[
                    0, slice_sel, point_sel] - 1

    avg_radial_strain = []
    avg_circ_strain = []
    # n_slices = img_dims[-1]
    # for ii in range(img_dims[0]):
    #     sel_range = radial_strain[ii,n_slices//2-2:n_slices//2+2]
    #     id_use = np.where(sel_range!= -10)
    #     avg_radial_strain.append([ii,np.mean(sel_range[id_use])])
    #     sel_range = circ_strain[ii,n_slices//2-2:n_slices//2+2]
    #     id_use = np.where(sel_range!= -10)
    #     avg_circ_strain.append([ii,np.mean(sel_range[id_use])])

    for ii in range(img_dims[0]):
        slice__,point__ = np.where(radial_strain[ii,:,:] != -10)
        avg_radial_strain.append([ii,np.mean(radial_strain[ii,slice__,point__])])
        slice__,point__ = np.where(circ_strain[ii,:,:] != -10)
        avg_circ_strain.append([ii,np.mean(circ_strain[ii,slice__,point__])])

    return np.array(avg_radial_strain), np.array(avg_circ_strain)

# def computeSAXThicknessAndRadius(patient_folder):
#     ''' Compute average radius and thickness of each myocardium slice in sax views
#     Args
#         mesh_path: folder with cardiac meshes in world coordinates
#         sax_image_folder: folder with sax images
#
#     Output
#         thickness_over_time[phase,slice]: vector with the average myocardium thickness for a given phase and slice.
#                                         If no LV is there, the default value is -1
#         radius_over_time[phase,slice]: vector with the average mid ventricular radius for a given phase and slice
#                                         If no LV is there, the default value is -1
#     '''
#
#     import vtk
#     import pyvista as pv
#     mesh_path = patient_folder + '/meshes_WorldCoordinates/'
#
#     folders_available = np.sort(next(os.walk(patient_folder))[1])
#     sax_folder = None
#     for folder_sel in folders_available:
#         if folder_sel[-4:] == '_VTI' and 'sax' in folder_sel.lower():
#             sax_folder = folder_sel
#             sax_image_folder = patient_folder + '/' + sax_folder
#             break
#     if sax_folder == None:
#         print('Could not find the SAX images in VTI format')
#
#     # Read vti files in the folder of interest
#     files = np.sort(next(os.walk(mesh_path))[2])
#     cardiac_phases = []
#     for file_sel in files:
#         if file_sel[0] != '.' and file_sel[-3:] == 'vtk':
#             cardiac_phases.append(file_sel)
#
#     n_phases = len(cardiac_phases)
#     # Read reference image to get the position in space and dimensions
#     refImg = np.sort(next(os.walk(sax_image_folder))[2])
#     ref_vtk = vtk.vtkXMLImageDataReader()
#     for img_name in refImg:
#         if img_name[-3:] == 'vti':
#             ref_vtk.SetFileName(sax_image_folder + '/' + img_name)
#             break
#         else:
#             continue
#
#     ref_vtk.Update()
#     img_vtk_to_use = ref_vtk.GetOutput()
#     dims = img_vtk_to_use.GetDimensions()
#
#     Img2WorldMatrix = np.load(sax_image_folder + '/Img2World_matrix4x4.npy')
#
#     radius_over_time = np.zeros((n_phases, dims[2]))
#     thickness_over_time = np.zeros((n_phases, dims[2]))
#
#     for phase_sel in range(n_phases):
#         phase_mesh = pv.read(mesh_path + '/' + cardiac_phases[phase_sel])
#         surf_mesh = phase_mesh.extract_surface(nonlinear_subdivision=5)
#
#         mask = np.zeros(dims)
#
#         for sel_plane in range(dims[2]):
#             plane = cmrpro.io.utils.planeToGrid(dims[:2], sel_plane, Img2WorldMatrix)
#             mask[..., sel_plane] = plane.select_enclosed_points(surf_mesh, check_surface=True, )['SelectedPoints'].reshape(
#                 dims[:2], order='F')
#
#             thickness_over_time[phase_sel, sel_plane], radius_over_time[phase_sel, sel_plane] = extractEndoEpiContourPoints(mask[..., sel_plane])
#
#     return thickness_over_time, radius_over_time
#
# def computeSAXStrains(thickness_over_time, radius_over_time):
#     ''' Compute average radial and circumferential strains in sax views
#     Args
#         thickness_over_time[phase,slice]: vector with the average myocardium thickness for a given phase and slice
#         radius_over_time[phase,slice]: vector with the average mid ventricular radius for a given phase and slice
#         slice_ids: index of selected slices for the calculation, if None all slices containing LV myocardium are used
#
#     Returns
#         thickness_over_time[phase,slice]: vector with the average myocardium thickness for a given phase and slice
#         tradius_over_time[phase,slice]: vector with the average mid ventricular radius for a given phase and slice
#     '''
#
#     slice_ids = np.arange(thickness_over_time.shape[1])
#     n_slices = len(slice_ids)
#
#     phases   = np.arange(thickness_over_time.shape[0])
#     n_phases = len(phases)
#     anat_radial = np.zeros((n_phases, n_slices))
#     anat_circ = np.zeros((n_phases, n_slices))
#
#     slices_exclude = []
#     for ii in range(thickness_over_time.shape[1]):
#         if any(thickness_over_time[:,ii] == -1) or any(radius_over_time[:,ii] == -1):
#             slices_exclude.append(ii)
#
#     slices_exclude = np.array(slices_exclude)
#     for phase_id in range(n_phases):
#         for dim_index in range(n_slices):
#             if dim_index not in slices_exclude:
#                 anat_radial[phase_id,dim_index] =  (thickness_over_time[phase_id, dim_index] / thickness_over_time[0, dim_index] - 1)
#                 anat_circ[phase_id,dim_index]   =  (radius_over_time[phase_id, dim_index] / radius_over_time[0, dim_index] - 1)
#
#             #             anat_radial[phase_id,dim_index] = 0.5 * ((thickness_over_time[phase_id, dim_index] / thickness_over_time[0, dim_index])** 2 - 1)
#             #             anat_circ[phase_id,dim_index]   = 0.5 * ((radius_over_time[phase_id, dim_index] / radius_over_time[0, dim_index])** 2 - 1)
#             else:
#                 anat_radial[phase_id,dim_index] = - 5.0
#                 anat_circ[phase_id, dim_index]  = - 5.0
#     return anat_radial, anat_circ
#
# def reportStrains(anat_radial,anat_circ,radius_time,thick_time,out_folder):
#     '''
#     saves radial and circumferential strains as txt file
#     saves plots of mean radial and circumferential strains
#     Args:
#         anat_radial:
#         anat_circ:
#         out_folder:
#
#     Returns:
#     '''
#
#     ff = open(out_folder + '/Radial_maskStrains.txt', 'w')
#     for phase_sel in range(anat_radial.shape[0]):
#         ff.write('%d ' % (phase_sel, ))
#         for slice_sel in range(anat_radial.shape[1]):
#             ff.write('%1.4f ' % (anat_radial[phase_sel, slice_sel]))
#         ff.write('\n')
#     ff.close()
#
#     ff = open(out_folder + '/Circ_maskStrains.txt', 'w')
#     for phase_sel in range(anat_circ.shape[0]):
#         ff.write('%d ' % (phase_sel, ))
#         for slice_sel in range(anat_circ.shape[1]):
#             ff.write('%1.4f ' % (anat_circ[phase_sel, slice_sel]))
#         ff.write('\n')
#     ff.close()
#
#     ff = open(out_folder + '/ThicknessFromMask.txt', 'w')
#     for phase_sel in range(thick_time.shape[0]):
#         ff.write('%d ' % (phase_sel, ))
#         for slice_sel in range(thick_time.shape[1]):
#             ff.write('%1.4f ' % (thick_time[phase_sel, slice_sel]))
#         ff.write('\n')
#     ff.close()
#
#     ff = open(out_folder + '/RadiusFromMask.txt', 'w')
#     for phase_sel in range(radius_time.shape[0]):
#         ff.write('%d ' % (phase_sel,))
#         for slice_sel in range(radius_time.shape[1]):
#             ff.write('%1.4f ' % (radius_time[phase_sel, slice_sel]))
#         ff.write('\n')
#     ff.close()
#
#     slices_exclude = []
#     for ii in range(anat_radial.shape[1]):
#         if any(anat_radial[ii,:] == -5):
#             new_exclude = np.where(anat_radial[ii,:]==-5.0)[0]
#             for kk in new_exclude:
#                 if kk not in slices_exclude:
#                     slices_exclude.append(kk)
#         if any(anat_circ[ii,:] == -5):
#             new_exclude = np.where(anat_circ[ii,:]==-5.0)[0]
#             for kk in new_exclude:
#                 if kk not in slices_exclude:
#                     slices_exclude.append(kk)
#
#
#
#     slices_exclude = np.unique(np.array(slices_exclude))
#     slices_ok = []
#     for ii in range(anat_radial.shape[1]):
#         if ii not in slices_exclude:
#             slices_ok.append(ii)
#
#     anat_avg_radial = np.mean(anat_radial[:,slices_ok],1).reshape(-1,1)[:,0]
#
#     anat_avg_circ = np.mean(anat_circ[:,slices_ok], 1).reshape(-1,1)[:,0]
#
#     phases = np.arange(len(anat_avg_radial)).reshape(-1,1)[:,0]
#
#     plt.plot(phases,anat_avg_radial, 'b', label=' mean radial (max=%s)' % (anat_avg_radial[2:-3].max(),))
#     plt.plot(phases,anat_avg_circ, 'r', label='mean circ (max=%s)' % (anat_avg_circ[2:-3].min(),))
#     plt.legend()
#     plt.xlabel('time frame')
#     plt.ylabel('strain')
#
#     plt.savefig(out_folder + '/MaskStrains.png', dpi=100)
#     plt.close()
#
#     return 0
#
#
# def extractEndoEpiContourPoints(myo, target_points=60):
#
#     from scipy.ndimage import binary_fill_holes
#     import cv2
#     from scipy.ndimage import center_of_mass as com
#     from scipy.interpolate import interp1d
#
#     filled = 1 * binary_fill_holes(myo)
#     bpool = np.multiply(1 - myo, filled)
#     ref_theta = np.linspace(-np.pi*0.8,np.pi*0.8, target_points)
#     if np.sum(filled) > 0 and np.sum(bpool) > 0:  # ensures that the slice has both endo and epi
#
#         try:
#             epiContour, _ = cv2.findContours(filled.astype(np.uint8), 2, 1)
#             epiContour = np.array(epiContour[0]).reshape(-1, 2)
#             endoContour, _ = cv2.findContours(bpool.astype(np.uint8), 2, 1)
#             endoContour = np.array(endoContour[0]).reshape(-1, 2)
#             cy, cx = com(myo)
#
#             anglesEpi = np.zeros((epiContour.shape[0], 1))
#             radiusEpi = np.zeros((epiContour.shape[0], 1))
#
#             for i in range(epiContour.shape[0]):
#                 relx = float(epiContour[i, 0] - cx)
#                 rely = float(epiContour[i, 1] - cy)
#                 anglesEpi[i, 0] = np.arctan2(rely, relx)
#                 radiusEpi[i, 0] = np.sqrt(relx ** 2 + rely ** 2)
#
#             #anglesEpiPeriodic = np.concatenate((anglesEpi - 2 * np.pi, anglesEpi, anglesEpi + 2 * np.pi))
#             #radiusEpiPeriodic = np.concatenate((radiusEpi, radiusEpi, radiusEpi))
#             f = interp1d(anglesEpi[:, 0], radiusEpi[:, 0])
#             radiusEpiresampled = f(ref_theta)
#             epiPoints = np.zeros((target_points, 2))
#             for i in range(target_points):
#                 epiPoints[i, :] = [radiusEpiresampled[i] * np.cos(ref_theta[i]) + cx,
#                                    radiusEpiresampled[i] * np.sin(ref_theta[i]) + cy]
#
#             anglesEndo = np.zeros((endoContour.shape[0], 1))
#             radiusEndo = np.zeros((endoContour.shape[0], 1))
#             for i in range(endoContour.shape[0]):
#                 relx = float(endoContour[i, 0] - cx)
#                 rely = float(endoContour[i, 1] - cy)
#                 anglesEndo[i, 0] = np.arctan2(rely, relx)
#                 radiusEndo[i, 0] = np.sqrt(relx ** 2 + rely ** 2)
#
#             #anglesEndoPeriodic = np.concatenate((anglesEndo - 2 * np.pi, anglesEndo, anglesEndo + 2 * np.pi))
#             #radiusEndoPeriodic = np.concatenate((radiusEndo, radiusEndo, radiusEndo))
#             f = interp1d(anglesEndo[:, 0], radiusEndo[:, 0])
#             #ref_theta = np.linspace(anglesEndo[:, 0].min(), anglesEndo[:, 0].max(), target_points)
#             radiusEndoresampled = f(ref_theta)
#             endoPoints = np.zeros((target_points, 2))
#             for i in range(target_points):
#                 endoPoints[i, :] = [radiusEndoresampled[i] * np.cos(ref_theta[i]) + cx,
#                                     radiusEndoresampled[i] * np.sin(ref_theta[i]) + cy]
#
#             avg_thickness = 0
#             avg_radius    = 0
#             actual_points = 0
#             for i in range(target_points):
#                 if np.linalg.norm(endoPoints[i, :] - epiPoints[i, :]) >1:
#                     actual_points +=1
#                     avg_thickness += np.linalg.norm(endoPoints[i, :] - epiPoints[i, :])
# #                    avg_radius += np.linalg.norm(
# #                        0.5 * epiPoints[i, :] + 0.5 * endoPoints[i, :] - np.array([cx, cy]))
#                     # From Echo it looks they just look at the endocardium (Blood pool size)
#                     avg_radius += np.linalg.norm(endoPoints[i, :] - np.array([cx, cy]))
#             if avg_thickness>0 and avg_radius>0:
#                 return avg_thickness/actual_points, avg_radius/actual_points
#             else:
#                 return -1,1
#         except:
#             return -1, -1
#     else:
#         return -1, -1
