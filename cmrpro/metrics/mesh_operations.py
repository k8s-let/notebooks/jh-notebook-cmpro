import os
import numpy as np
import pyvista as pv
import vtk
from vtk.util.numpy_support import numpy_to_vtk, vtk_to_numpy
from skimage.morphology import skeletonize
from skimage.measure import label as skiLabel
import matplotlib.pyplot as plt

def readVTK(file_path,label_names):
	from vtk import vtkUnstructuredGridReader, vtkXMLUnstructuredGridReader
	from vtk.util.numpy_support import vtk_to_numpy, numpy_to_vtk
	import sys
	import numpy as np

	if file_path[-3:] == 'vtk':
		source_mesh_vtk = vtkUnstructuredGridReader()
		source_mesh_vtk.SetFileName(file_path)
		if len(label_names) > 0:
			source_mesh_vtk.ReadAllScalarsOn()
			source_mesh_vtk.ReadAllVectorsOn()
	elif file_path[-3:] == 'vtu':
		source_mesh_vtk = vtkXMLUnstructuredGridReader()
		source_mesh_vtk.SetFileName(file_path)
	else:
		print('Not a vtk unstructured grid format')
		sys.exit()

	source_mesh_vtk.Update()
	source_mesh  = source_mesh_vtk.GetOutput()

	n_points  = int(source_mesh.GetNumberOfPoints())
	n_cells   = source_mesh.GetNumberOfCells()
	Coords    = vtk_to_numpy(source_mesh.GetPoints().GetData()).copy()

	Els = []
	for i in range(n_cells):
		n_nodes_el   = source_mesh.GetCell(i).GetPointIds().GetNumberOfIds()
		connectivity = [0]*n_nodes_el
		for n_sel in range(n_nodes_el):
			connectivity[n_sel] = int(source_mesh.GetCell(i).GetPointId(n_sel))
		Els.append(connectivity)
	Els = np.array(Els)

	if len(label_names) > 0:
		out_variables = []
		for label_name in label_names:
			try:
				new_array = vtk_to_numpy(source_mesh.GetPointData().GetArray(label_name))
				out_variables.append(new_array)
			except:
				print(label_name,' not found in ',file_path)
	else:
		out_variables = []
	return n_points, n_cells, Coords, Els, out_variables

def getEDShapeParametrized(patient_folder,out_folder=None):

    if out_folder is None:
        out_folder = patient_folder

    mesh_file = patient_folder + '/meshes_WorldCoordinates/frame_%02d.vtk' % (0)

    mesh = pv.read(mesh_file)

    e_l = mesh.compute_derivative(scalars="x_l")['gradient']
    e_c = mesh.compute_derivative(scalars="x_c")['gradient']
    e_t = mesh.compute_derivative(scalars="x_t")['gradient']

    for i in range(e_l.shape[0]):
        if np.linalg.norm(e_l[i,:])>0:
            e_l[i,:] /= np.linalg.norm(e_l[i,:])
        if np.linalg.norm(e_c[i,:])>0:
            e_c[i,:] /= np.linalg.norm(e_c[i,:])
        if np.linalg.norm(e_t[i,:])>0:
            e_t[i,:] /= np.linalg.norm(e_t[i,:])

    reader = vtk.vtkUnstructuredGridReader()
    reader.SetFileName(mesh_file)
    reader.ReadAllScalarsOn()
    reader.ReadAllVectorsOn()
    reader.Update()
    mesh_vtk = reader.GetOutput()

    linear_array = np.array(e_l).reshape(-1,1)
    linear_array_vtk = numpy_to_vtk(linear_array)
    linear_array_vtk.SetName('e_l')
    linear_array_vtk.SetNumberOfComponents(3)
    mesh_vtk.GetPointData().AddArray(linear_array_vtk)
    mesh_vtk.Modified()

    linear_array = np.array(e_c).reshape(-1,1)
    linear_array_vtk = numpy_to_vtk(linear_array)
    linear_array_vtk.SetName('e_c')
    linear_array_vtk.SetNumberOfComponents(3)
    mesh_vtk.GetPointData().AddArray(linear_array_vtk)
    mesh_vtk.Modified()

    linear_array = np.array(e_t).reshape(-1,1)
    linear_array_vtk = numpy_to_vtk(linear_array)
    linear_array_vtk.SetName('e_t')
    linear_array_vtk.SetNumberOfComponents(3)
    mesh_vtk.GetPointData().AddArray(linear_array_vtk)
    mesh_vtk.Modified()

    writer = vtk.vtkUnstructuredGridWriter()
    writer.SetFileName(patient_folder+'/ED_parametric_reference_mesh.vtk')
    writer.SetInputData(mesh_vtk)
    writer.Write()

    return 0

def computeVolumesFromMeshes(patient_folder, store_on_file = True, out_folder=None):

    if out_folder is None:
        out_folder = patient_folder

    mesh_folder = patient_folder + '/meshes_WorldCoordinates/'

    files = np.sort(next(os.walk(mesh_folder))[2])
    cardiac_phases = []
    for file_sel in files:
        if file_sel[0] != '.' and file_sel[-3:] == 'vtk':
            cardiac_phases.append(file_sel)

    n_phases = len(cardiac_phases)

    MV_endo_label = 3

    n_points, n_cells, Coords, Els, out_vectors = readVTK(mesh_folder + '/' + cardiac_phases[0],
                                                                      ['labels', 'x_c', 'x_l'])
    labels = out_vectors[0]

    faces_connectivity = np.array([[0, 2, 1], [0, 1, 3], [1, 2, 3], [2, 0, 3]])
    Faces_Endo = []
    start_faces = True
    for kk in range(n_cells):
        el_points = Els[kk, :]
        for jj in range(4):
            if all(labels[int(v)] == 2 for v in el_points[faces_connectivity[jj]]):
                if start_faces:
                    Faces_Endo = np.array(el_points[faces_connectivity[jj]], dtype=int).reshape(1, -1)
                    start_faces = False
                else:
                    Faces_Endo = np.concatenate(
                        (Faces_Endo, np.array(el_points[faces_connectivity[jj]], dtype=int).reshape(1, -1)), 0)

    # Find MV points and sort based on the circumferential coordinate
    MV_points = np.where(labels == MV_endo_label)[0]
    BP_volume = np.zeros((n_phases, 1))
    Myo_volume = np.zeros((n_phases, 1))

    for phase_sel in range(n_phases):
        _, _, Coords, _, _ = readVTK(mesh_folder + '/' + cardiac_phases[phase_sel],
                                                 ['labels', 'x_c', 'x_l'])
        Coords /= 1e3

        id_phase = int(cardiac_phases[phase_sel].rstrip('.vtk').lstrip('frame_'))

        MV_center = np.mean(Coords[MV_points, :], 0)

        # Volume weighted projection
        for j in range(Els.shape[0]):
            Myo_volume[id_phase, 0] += 1.0 / 6.0 * (Coords[Els[j, 3], :] - Coords[Els[j, 0], :]).dot(
                np.cross(Coords[Els[j, 1], :] - Coords[Els[j, 0], :],
                         Coords[Els[j, 2], :] - Coords[Els[j, 0], :])) * 1e6

        for j in range(len(Faces_Endo)):
            sel_el = Faces_Endo[j]
            oa = np.array(Coords[sel_el[0], :] - MV_center)
            ob = np.array(Coords[sel_el[1], :] - MV_center)
            oc = np.array(Coords[sel_el[2], :] - MV_center)

            BP_volume[id_phase, 0] += 1.0 / 6.0 * abs(np.dot(np.cross(oa, ob), oc)) * 1e6

    if store_on_file:
        ff = open(out_folder + '/MeshVolumes.txt', 'w')
        for phase_sel in range(n_phases):
            ff.write('%d %1.4f %1.4f \n' % (phase_sel, BP_volume[phase_sel, 0], Myo_volume[phase_sel, 0]))
        ff.close()

    EF = (BP_volume[0] - BP_volume.min()) / BP_volume[0]
    MYocompr = Myo_volume.max() / Myo_volume.min() - 1

    if store_on_file:
        plt.plot(BP_volume, 'r', label='BP')
        plt.plot(Myo_volume, 'b', label='MYO')
        plt.legend()
        plt.xlabel('phase [-]')
        plt.ylabel('volume [ml]')
        plt.title('EF %.2f, Myo vol variation %.2f' % (EF, MYocompr))
        plt.savefig(out_folder + '/MeshVolumes.png')
        plt.close()

        np.save(out_folder + '/EF.npy', EF)
    return BP_volume, Myo_volume

def computeGS(patient_folder, out_folder=None):

    '''
    Computes global strains of the anatomy based on meshes. It is not using a Lagrangian Formulation (0.5*(C-I)),
    but uses the estimation of thickness and length from mesh points, similarly to what you would do with masks
    The strain is that computed as Lf/L0 -1 (as done in clinics)
    Args:
        patient_folder:
        out_folder:
    '''

    if out_folder is None:
        out_folder = patient_folder

    mesh_folder = patient_folder + '/meshes_WorldCoordinates/'
    mesh_folder_for_LAX =  patient_folder + '/meshes_SMCoordinates/'

    files = np.sort(next(os.walk(mesh_folder))[2])
    cardiac_phases = []
    for file_sel in files:
        if file_sel[0] != '.' and file_sel[-3:] == 'vtk':
            cardiac_phases.append(file_sel)

    n_phases = len(cardiac_phases)

    reader = vtk.vtkUnstructuredGridReader()
    reader.SetFileName(mesh_folder_for_LAX + '/' + cardiac_phases[0])
    reader.Update()
    vtk_mesh = reader.GetOutput()

    Coords = vtk_to_numpy(vtk_mesh.GetPoints().GetData())

    ref_img = vtk.vtkImageData() # generate 256x256x256 image with 1mm isotropic resolution
    ref_img_size = 200
    ref_img_res  = 0.01
    origin  = np.mean(Coords,0) - ref_img_res*ref_img_size//2
    ref_img.SetDimensions(ref_img_size,ref_img_size,ref_img_size)
    ref_img.SetOrigin(origin[0],origin[1],origin[2])
    ref_img.SetSpacing(ref_img_res,ref_img_res,ref_img_res)

    EpiPoints  = np.arange(1201) # This is just because of the shape model used
    LocalThick   = np.zeros((n_phases, 1201))
    LocalDiam    = np.zeros((n_phases, 1201))
    LocalRadial   = np.zeros((n_phases, 1201))
    LocalCirc     = np.zeros((n_phases, 1201))

    AvgLength     = np.zeros((n_phases, 1))


    for phase_sel in range(n_phases):
        _, _, Coords, _, _ = readVTK(mesh_folder + '/' + cardiac_phases[phase_sel],['labels'])
        # Computes average thickness of the anatomy for GRS
        LocalThick[phase_sel,:] = np.linalg.norm(Coords[EpiPoints+1201*3,:] - Coords[EpiPoints,:],axis=1)

        # Computes average circumference of epi and endo for GCS
        # Only consider projection onto 2D plane
        cdm = np.mean(Coords[1201*3:,:2],axis=0)
        LocalDiam[phase_sel,:] = np.linalg.norm(Coords[1201*3:,:2]-cdm,axis=1)

        # Compute longitudinal strains
        reader.SetFileName(mesh_folder_for_LAX + '/' + cardiac_phases[phase_sel])
        reader.Update()
        vtk_mesh = reader.GetOutput()
        AvgLength[phase_sel]= computeSkeletonLength(ref_img,vtk_mesh)

    for phase_sel in range(n_phases):
        LocalRadial[phase_sel,:] = LocalThick[phase_sel,:]/LocalThick[0,:] - 1
        LocalCirc[phase_sel,  :] = LocalDiam[phase_sel,:]/LocalDiam[0, :] -  1

    GRS = np.mean(LocalRadial,axis=1)
    GCS = np.mean(LocalCirc,axis=1)
    GLS = AvgLength/AvgLength[0]-1

    ff = open(out_folder + '/MeshGLS.txt', 'w')
    for phase_sel in range(n_phases):
        ff.write('%d %1.4f \n' % (phase_sel, GLS[phase_sel]))
    ff.close()

    ff = open(out_folder + '/MeshGRS.txt', 'w')
    for phase_sel in range(n_phases):
        ff.write('%d %1.4f \n' % (phase_sel, GRS[phase_sel]))
    ff.close()

    ff = open(out_folder + '/MeshGCS.txt', 'w')
    for phase_sel in range(n_phases):
        ff.write('%d %1.4f \n' % (phase_sel, GCS[phase_sel]))
    ff.close()

    plt.plot(GLS, 'r', label='GLS')
    plt.plot(GRS, 'b', label='GRS')
    plt.plot(GCS, 'g', label='GCS endo')

    plt.legend()
    plt.xlabel('phase [-]')
    plt.ylabel('strains [-]')
    plt.savefig(out_folder + '/MeshGlobalStrains.png')
    plt.close()

    return 0


def computeSkeletonLength(ref_img, vtk_mesh):

    probeSkel = vtk.vtkProbeFilter()
    probeSkel.SetInputData(ref_img)
    probeSkel.SetSourceData(vtk_mesh)
    probeSkel.PassPointArraysOn()
    probeSkel.Update()

    maskSkel = vtk_to_numpy(probeSkel.GetOutput().GetPointData().GetArray('vtkValidPointMask')).reshape(ref_img.GetDimensions(), order='F').copy(order='C')
    # keep only the largest connected component (to try and be somewhat robust to
    # outliers / small segmentation errors)
    maskSkel, count = skiLabel(maskSkel, return_num=True)

    if count > 1:
        component_sizes = []
        for i in range(1, count + 1):
            component_sizes.append(np.sum(maskSkel == i))
        largest_region_val = np.argmax(component_sizes) + 1
        maskSkel = maskSkel  == largest_region_val

    lvx, lvy, lvz = np.where(maskSkel == 1)

    cdmx = int(np.mean(lvx))
    cdmy = int(np.mean(lvy))

    lax1 = maskSkel[cdmx, ...].copy(order='C')
    lax2 = maskSkel[:, cdmy, :].copy(order='C')

    skel1 = skeletonize(lax1) * 1
    skel2 = skeletonize(lax2) * 1


    mask_sel = skel1
    xx, zz = np.where(mask_sel == 1)
    apex_id = int(xx.argmin())

    apex_pos = [xx[apex_id], zz[apex_id]]

    branchLeft  = mask_sel[:, :apex_pos[1] + 1]
    branchRight = mask_sel[:, apex_pos[1]:]

    xbranch, zbranch = np.where(branchLeft == 1)
    sorting_list = np.argsort(xbranch)
    xbranch = xbranch[sorting_list]
    zbranch = zbranch[sorting_list]

    branch_length1 = 0
    for i in range(1, len(xbranch)):
        segment = np.sqrt((xbranch[i] - xbranch[i - 1]) ** 2 + (zbranch[i] - zbranch[i - 1]) ** 2)
        if segment < np.sqrt(2)*1.1:
            branch_length1 += segment

    xbranch, zbranch = np.where(branchRight == 1)
    sorting_list = np.argsort(xbranch)

    xbranch = xbranch[sorting_list]
    zbranch = zbranch[sorting_list]

    for i in range(1, len(xbranch)):
        segment = np.sqrt((xbranch[i] - xbranch[i - 1]) ** 2 + (zbranch[i] - zbranch[i - 1]) ** 2)
        if segment < np.sqrt(2)*1.1:
            branch_length1 += segment

    mask_sel = skel2
    xx, zz = np.where(mask_sel == 1)
    apex_id = int(xx.argmin())

    apex_pos = [xx[apex_id], zz[apex_id]]

    branchLeft  = mask_sel[:, :apex_pos[1] + 1]
    branchRight = mask_sel[:, apex_pos[1]:]

    xbranch, zbranch = np.where(branchLeft == 1)
    sorting_list = np.argsort(xbranch)
    xbranch = xbranch[sorting_list]
    zbranch = zbranch[sorting_list]

    branch_length2 = 0
    for i in range(1, len(xbranch)):
        segment = np.sqrt((xbranch[i] - xbranch[i - 1]) ** 2 + (zbranch[i] - zbranch[i - 1]) ** 2)
        if segment < np.sqrt(2)*1.1:
            branch_length2 += segment

    xbranch, zbranch = np.where(branchRight == 1)
    sorting_list = np.argsort(xbranch)
    xbranch = xbranch[sorting_list]
    zbranch = zbranch[sorting_list]

    for i in range(1, len(xbranch)):
        segment = np.sqrt((xbranch[i] - xbranch[i - 1]) ** 2 + (zbranch[i] - zbranch[i - 1]) ** 2)
        if segment < np.sqrt(2)*1.1:
            branch_length2 += segment

    return (branch_length1 + branch_length2) * 0.5

import os
import numpy as np
import pyvista as pv
import vtk
from vtk.util.numpy_support import numpy_to_vtk, vtk_to_numpy
from skimage.morphology import skeletonize
from skimage.measure import label as skiLabel
import matplotlib.pyplot as plt

def readVTK(file_path,label_names):
	from vtk import vtkUnstructuredGridReader, vtkXMLUnstructuredGridReader
	from vtk.util.numpy_support import vtk_to_numpy, numpy_to_vtk
	import sys
	import numpy as np

	if file_path[-3:] == 'vtk':
		source_mesh_vtk = vtkUnstructuredGridReader()
		source_mesh_vtk.SetFileName(file_path)
		if len(label_names) > 0:
			source_mesh_vtk.ReadAllScalarsOn()
			source_mesh_vtk.ReadAllVectorsOn()
	elif file_path[-3:] == 'vtu':
		source_mesh_vtk = vtkXMLUnstructuredGridReader()
		source_mesh_vtk.SetFileName(file_path)
	else:
		print('Not a vtk unstructured grid format')
		sys.exit()

	source_mesh_vtk.Update()
	source_mesh  = source_mesh_vtk.GetOutput()

	n_points  = int(source_mesh.GetNumberOfPoints())
	n_cells   = source_mesh.GetNumberOfCells()
	Coords    = vtk_to_numpy(source_mesh.GetPoints().GetData()).copy()

	Els = []
	for i in range(n_cells):
		n_nodes_el   = source_mesh.GetCell(i).GetPointIds().GetNumberOfIds()
		connectivity = [0]*n_nodes_el
		for n_sel in range(n_nodes_el):
			connectivity[n_sel] = int(source_mesh.GetCell(i).GetPointId(n_sel))
		Els.append(connectivity)
	Els = np.array(Els)

	if len(label_names) > 0:
		out_variables = []
		for label_name in label_names:
			try:
				new_array = vtk_to_numpy(source_mesh.GetPointData().GetArray(label_name))
				out_variables.append(new_array)
			except:
				print(label_name,' not found in ',file_path)
	else:
		out_variables = []
	return n_points, n_cells, Coords, Els, out_variables

def getEDShapeParametrized(patient_folder,out_folder=None):

    if out_folder is None:
        out_folder = patient_folder

    mesh_file = patient_folder + '/meshes_WorldCoordinates/frame_%02d.vtk' % (0)

    mesh = pv.read(mesh_file)

    e_l = mesh.compute_derivative(scalars="x_l")['gradient']
    e_c = mesh.compute_derivative(scalars="x_c")['gradient']
    e_t = mesh.compute_derivative(scalars="x_t")['gradient']

    for i in range(e_l.shape[0]):
        if np.linalg.norm(e_l[i,:])>0:
            e_l[i,:] /= np.linalg.norm(e_l[i,:])
        if np.linalg.norm(e_c[i,:])>0:
            e_c[i,:] /= np.linalg.norm(e_c[i,:])
        if np.linalg.norm(e_t[i,:])>0:
            e_t[i,:] /= np.linalg.norm(e_t[i,:])

    reader = vtk.vtkUnstructuredGridReader()
    reader.SetFileName(mesh_file)
    reader.ReadAllScalarsOn()
    reader.ReadAllVectorsOn()
    reader.Update()
    mesh_vtk = reader.GetOutput()

    linear_array = np.array(e_l).reshape(-1,1)
    linear_array_vtk = numpy_to_vtk(linear_array)
    linear_array_vtk.SetName('e_l')
    linear_array_vtk.SetNumberOfComponents(3)
    mesh_vtk.GetPointData().AddArray(linear_array_vtk)
    mesh_vtk.Modified()

    linear_array = np.array(e_c).reshape(-1,1)
    linear_array_vtk = numpy_to_vtk(linear_array)
    linear_array_vtk.SetName('e_c')
    linear_array_vtk.SetNumberOfComponents(3)
    mesh_vtk.GetPointData().AddArray(linear_array_vtk)
    mesh_vtk.Modified()

    linear_array = np.array(e_t).reshape(-1,1)
    linear_array_vtk = numpy_to_vtk(linear_array)
    linear_array_vtk.SetName('e_t')
    linear_array_vtk.SetNumberOfComponents(3)
    mesh_vtk.GetPointData().AddArray(linear_array_vtk)
    mesh_vtk.Modified()

    writer = vtk.vtkUnstructuredGridWriter()
    writer.SetFileName(patient_folder+'/ED_parametric_reference_mesh.vtk')
    writer.SetInputData(mesh_vtk)
    writer.Write()

    return 0

def computeVolumesFromMeshes(patient_folder, store_on_file = True, out_folder=None):

    if out_folder is None:
        out_folder = patient_folder

    mesh_folder = patient_folder + '/meshes_WorldCoordinates/'

    files = np.sort(next(os.walk(mesh_folder))[2])
    cardiac_phases = []
    for file_sel in files:
        if file_sel[0] != '.' and file_sel[-3:] == 'vtk':
            cardiac_phases.append(file_sel)

    n_phases = len(cardiac_phases)

    MV_endo_label = 3

    n_points, n_cells, Coords, Els, out_vectors = readVTK(mesh_folder + '/' + cardiac_phases[0],
                                                                      ['labels', 'x_c', 'x_l'])
    labels = out_vectors[0]

    faces_connectivity = np.array([[0, 2, 1], [0, 1, 3], [1, 2, 3], [2, 0, 3]])
    Faces_Endo = []
    start_faces = True
    for kk in range(n_cells):
        el_points = Els[kk, :]
        for jj in range(4):
            if all(labels[int(v)] == 2 for v in el_points[faces_connectivity[jj]]):
                if start_faces:
                    Faces_Endo = np.array(el_points[faces_connectivity[jj]], dtype=int).reshape(1, -1)
                    start_faces = False
                else:
                    Faces_Endo = np.concatenate(
                        (Faces_Endo, np.array(el_points[faces_connectivity[jj]], dtype=int).reshape(1, -1)), 0)

    # Find MV points and sort based on the circumferential coordinate
    MV_points = np.where(labels == MV_endo_label)[0]
    BP_volume = np.zeros((n_phases, 1))
    Myo_volume = np.zeros((n_phases, 1))

    for phase_sel in range(n_phases):
        _, _, Coords, _, _ = readVTK(mesh_folder + '/' + cardiac_phases[phase_sel],
                                                 ['labels', 'x_c', 'x_l'])
        Coords /= 1e3

        id_phase = int(cardiac_phases[phase_sel].rstrip('.vtk').lstrip('frame_'))

        MV_center = np.mean(Coords[MV_points, :], 0)

        # Volume weighted projection
        for j in range(Els.shape[0]):
            Myo_volume[id_phase, 0] += 1.0 / 6.0 * (Coords[Els[j, 3], :] - Coords[Els[j, 0], :]).dot(
                np.cross(Coords[Els[j, 1], :] - Coords[Els[j, 0], :],
                         Coords[Els[j, 2], :] - Coords[Els[j, 0], :])) * 1e6

        for j in range(len(Faces_Endo)):
            sel_el = Faces_Endo[j]
            oa = np.array(Coords[sel_el[0], :] - MV_center)
            ob = np.array(Coords[sel_el[1], :] - MV_center)
            oc = np.array(Coords[sel_el[2], :] - MV_center)

            BP_volume[id_phase, 0] += 1.0 / 6.0 * abs(np.dot(np.cross(oa, ob), oc)) * 1e6

    if store_on_file:
        ff = open(out_folder + '/MeshVolumes.txt', 'w')
        for phase_sel in range(n_phases):
            ff.write('%d %1.4f %1.4f \n' % (phase_sel, BP_volume[phase_sel, 0], Myo_volume[phase_sel, 0]))
        ff.close()

    EF = (BP_volume[0] - BP_volume.min()) / BP_volume[0]
    MYocompr = Myo_volume.max() / Myo_volume.min() - 1

    if store_on_file:
        plt.plot(BP_volume, 'r', label='BP')
        plt.plot(Myo_volume, 'b', label='MYO')
        plt.legend()
        plt.xlabel('phase [-]')
        plt.ylabel('volume [ml]')
        plt.title('EF %.2f, Myo vol variation %.2f' % (EF, MYocompr))
        plt.savefig(out_folder + '/MeshVolumes.png')
        plt.close()

        np.save(out_folder + '/EF.npy', EF)
    return BP_volume, Myo_volume

def computeMVDisplacement(patient_folder,out_folder=None):

    '''
    Computes mitral valve center displacement as estimated with MV Locator

    Args:
        patient_folder:
        out_folder:
    '''

    if out_folder is None:
        out_folder = patient_folder

    valve_positions = np.loadtxt(patient_folder+'/MV_position.txt')

    valve_displ = [0]
    for ii in range(1,valve_positions.shape[0]):
        valve_displ.append(np.linalg.norm(valve_positions[ii,1:]-valve_positions[0,1:]))

    ff = open(patient_folder+'/MV_motion.txt','w')
    for ii in range(len(valve_displ)):
        ff.write('%1.4f\n' %(valve_displ[ii]))
    ff.close()

    plt.plot(valve_displ)
    plt.xlabel('phase [-]')
    plt.ylabel('valve_displacement [mm]')
    plt.savefig(out_folder + '/MV_motion.png')
    plt.close()

    return 0
