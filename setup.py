import setuptools
import os
import shutil
import sys

project_name = "cmrpro"
author_list = "Dr. Stefano Buoso"
author_email_list = "buoso@biomed.ee.ethz.ch"
project_urls = {
    "Source": "https://gitlab.ethz.ch/ibt-cmr/cmrpro"
}


# Get version tag
if '--version' in sys.argv:
    tag_index = sys.argv.index('--version') + 1
    current_version = sys.argv[tag_index]
    sys.argv.pop(tag_index-1)
    sys.argv.pop(tag_index-1)
else:
    raise ValueError('No version as keyword "--version" was specified')

with open(f'{project_name}/__init__.py', 'r+') as module_header_file:
    content = module_header_file.read()
    module_header_file.seek(0)
    module_header_file.write(f"__version__ = '{current_version}'\n" + content)

setuptools.setup(
    name=project_name,
    project_urls=project_urls,
    version=current_version,
    author=author_list,
    author_email=author_email_list,
    packages=setuptools.find_packages(exclude=["examples"]),
    package_data={"cmrpro": ["processor/networks/**", "meshfitter/Shape_model_ML_v3/**", "font/**"]},
    include_package_data=True,
    install_requires=["torch", "segmentation_models_pytorch", "numpy", "tqdm", "scipy"],
    python_requires=">=3.8"
)
