#FROM jupyter/tensorflow-notebook:hub-3.0.0
FROM ghcr.io/pyvista/pyvista:latest
#FROM quay.io/jupyter/pytorch-notebook
#FROM jupyter/base-notebook:python-3.10

USER 0

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get upgrade -y && \
  #apt-get install -y software-properties-common \
  #&& \
  #add-apt-repository ppa:fenics-packages/fenics \
  #&& \
  #apt-get update \
  #&& \
  apt-get install -y \
    build-essential \
    libstdc++6 \
    libx11-6 \
    libgl1-mesa-dev \
    libxrender-dev \
    xvfb \
    ffmpeg \
    libsm6 \
    libxext6 \
    wget \
  && \
  apt-get clean

RUN pip3 install torch
RUN pip3 install torchvision
RUN pip3 install nibabel pydicom scikit-image

RUN PIP_PROXY=http://proxy.ethz.ch:3128 pip3 install --proxy=http://proxy.ethz.ch:3128 --default-timeout=100 \
  efficientnet-pytorch \
  ffmpeg \
  gtagora-connector \
  imageio \
  matplotlib \
  meshio \
  numpy \
  opencv-python \
  panda \
  panel \
  parrec-reader-py \
  plotly \
  python-gdcm \
  pretrainedmodels \
  scipy \
  segmentation_models_pytorch \
  skan \
  timm \
  tqdm \
  cmocean \
  colorcet \
  imageio-ffmpeg \
  #ipywidgets<9.0.0 \
  #jupyter-server-proxy \
  #jupyterlab<5.0.0 \
  #trame>=2.5.2 \
  #trame-vtk>=2.5.8 \
  #trame-vuetify>=2.3.1 \

#RUN pip uninstall vtk
#RUN pip install --no-cache-dir --extra-index-url https://wheels.vtk.org vtk-osmesa

RUN export DISPLAY=:99.0 && Xvfb :99 -screen 0 1024x768x24 > /dev/null 2>&1 &
RUN sleep 5;

ENV JUPYTER_ENABLE_LAB=yes
ENV PYVISTA_TRAME_SERVER_PROXY_PREFIX='/proxy/'

USER 1000