## Overview

CMRPro is a package for loading and processing cardiac MRI exams. 
It is be able to load all exams (consisting of multiple series) of a patient from dicom, nifti and par/rec.

The package is split in:
- io : functions for loading and preparing the series for analysis
- processor : functions for segmenting the data (cine and LGE)
- meshfitter : functions for fitting a left-ventricular shape model to given segmentation masks
- metrics: functions for computing clinical metrics of interest (volumes, EF, strains)
- lgepostprocessor: functions for extracting metrics from LGE data

The method is presented in 

Joyce T, Buoso S, Stoeck C and Kozerke S (2022), Rapid inference of personalised left-ventricular meshes by deformation-based differentiable mesh voxelization, Medical Image Analysis,Volume 79, 102445, https://doi.org/10.1016/j.media.2022.102445.

## Environment installation

The code is based on the pytorch and pyvista libraries. We recommend running the method in docker containers.

You can generate the docker image from the docker files inside the folde r_ /cmrpro/docker/_ with the command

`docker build -t cmrexam -f <path_to_project_folder>/cmrpro/docker/Dockerfile_base . `

Alternatively, you can pull the pre-built image from the registry of eth. First log in as 

`docker login registry.ethz.ch ` with your ETH username and password, or alternatively by creating a token.

Then, pull the existing image with 

`docker pull registry.ethz.ch/ibt-cmr/modeling/cmrexam:base `
You can also tag the image with a shorter name for conveninece

`docker tag  registry.ethz.ch/ibt-cmr/modeling/cmrexam:base cmrexam`

Then, mount the image as:

`docker run -it -v  <path_to_project_folder>:/code -v <path_to_data_folder>:/data --init -p 8888:8888 cmrexam`

where <path_to_project_folder> is the path to the local folder containing the repository and <path_to_data_folder> is the folder containing the datasets to process. The folders will be mounted in /code and /data respectively within the docker container.

Note, you should use a rootless installation of docker. If you are running Ubuntu, before mounting the image you should run 

`docker context use rootless`

You can also link the docker image in PyCharm. In the project settings, select Python Interpreter and create a new one based on Docker. Select to pull the image and use cmrexam as name tag (after you pulled the image or built it yourself). In the Interpreter path use 

`/opt/conda/envs/pytorch3d/bin/python` 

so that PyCharm will use the anaconda environment provided in the environment. To mount the local folders, select Run and Edit Configurations and add the mounting instructions (-v options above) in the Docker container settings.

You can also download the docker image `registry.ethz.ch/ibt-cmr/modeling/cmrexam:latest ` that contains the latest compiled version of cmrpro. In this case there is no need of downloading the repository, but simply, in your script, include 

`import cmrpro`

## Code installation
You can pull this repository and add the repository path to your script.

Alternatively, you can install the pre-built [pip package](https://gitlab.ethz.ch/ibt-cmr/modeling/cmrexam/-/packages) available in the repository. You need to first create your [personal access token]¨(https://gitlab.ethz.ch/help/user/profile/personal_access_tokens) and then install the package in the docker container with 

`pip install cmrpro --index-url https://__token__:<your_personal_token>@gitlab.ethz.ch/api/v4/projects/33986/packages/pypi/simple`

## Usage
Examples of package use can be found in the examples folder.
The framework requires (and has been tested for) sets of images that should be placed in folders with the following structure

    ├── Patient folder                 
    │   ├── sax          # folder containing cine sax images (folder name should contain the string 'sax')
    │   └── 4ch/lax      # folder containing cine lax images  (folder name should contain the string '4ch or 'lax')
	│   └── viab sax     # folder containing sax LGE images  (folder name should contain the string 'viab')

A single CMR serie can be loaded as an series object:

`import cmrpro.exam `

`cmre = cmrpro.exam.CMRSeries.from_file('path_to_folder')` 

If the folder contains multiple series, they can be all loaded as a single exam object:

`import cmrpro.exam `

`cmre = cmrpro.exam.CMRExam('path_to_folder')` 

You can then print a summary of the exam:

`cmre.summary()`

When multiple series are available, you can visualise the intersection of the middle slices to verify the definition of the affine matrices from the data. The command below saves an image in the selected output folder.

`cmre.visualiseSlicesIn3D(time_frame=0, interactive=False, return_image=False, folder=out_folder)`

Finally, all the images in the exam can be segmented like this:

`peo = cmre.processExam()`

processExam goes through each series in the exam and performs the following steps: 

1. locates the region of interest in the series (i.e. tries to find the heart)
2. resized to 1mm x 1mm and crops out a 192x192 image centered on the ROI
3. segments the images into thre classes: lm-myo, lv-bp and rv-bp

It then returns the segmentations, the coordinates, and the images (all 192x192 pixels) for each of the series combined together in a `ProcessedExamObject`.

This `ProcessedExamObject` is a very simple class designed for representing segmented CMRExams. It is essentially a list of dictionaries, with one dictionary corresponding to each series in the exam. Each dictionary has three entries: 'seg', which contains the resulting segmentations, 'coord' which contains the coordinate images, and 'img' which contains the corresponding region of interest from the original image.

Putting it all together, the full code loading and segmenting an exam would look like this:

You can then save the resulting peo object (which contains the segmentation results) like this:

`peo.save_pickle(out_folder + '/ProcessesExamObject.peo',compressed=False)`

This saved file can then be used directly as an input for mesh fitting, 


`import cmrpro.meshfitter`

`from cmrpro.exam._processed_exam import ProcessedExamObject`

`peo = ProcessedExamObject().from_file(working_dir+'/ProcessesExamObject.peo', compressed=False)`

`mf = cmrpro.meshfitter.MeshModel(peo, out_folder=working_dir)`

`mf.fit()`


##major changes to integrate lge segmentations

In the cmrpro.io code:

	CMRSeries object can be either a cine or lge series (is saved in the flag “isLGESeries”)
		Flag “isLGESeries” is set via the function guess_isLGE_from_folder_name()  a sieres is only interpreted as lge series if name contains ‘lge’ or ‘viab’
	CMRExam object 
		has both a “series” (list of ‘CMRSeries’ from cine data) and “lge_series” (list of ‘CMRSeries’ from lge data) 
	ProcessedExamObject 
		also has both: “series” & “lge_series” [both are lists of dictionaries with one dictionary for each CMRSeries]
		Variable lge_time_frame: time frame from the sax cine series corresponding to the trigger time of the lge image
		Function addLGESeries: appends a dictionary to the dictionary list “lge_series” 
		Function addSegementationToLGESeries: can be used to add a lge segmentation that was segmented externally to the data (e.g. when using a tensorflow netwok or manual segmentation)

In the cmrpro.processor code:

	cmrpro.processor.segment.process_cine_exam(cmre,external_lge_segmentation=False,useTTA=False)
		Segments all contained series (cine and lege) and stores them in a new ProcessedExam object
	get_matching_time_frame
		get the time frame from the cine data that matches the lge data the best
	preprocess_lge_series: 
		Reprocesses the lge data (cropping based on cine LV center)+if external_lge_segmentation==False: applys LGE segmentation network (if flag is True data is only preprocessed but not segmented: see 2 example skrips: run_combinedanalysis_cine_lge_withExternalLGESegmentation_step[1&2]preprocessing)

Added cmrpro.lgepostprocessor.LGEModel

	For post-processing, scar evaluation, and visualization

